package com.lia.yello.linedineresturant.Utils

import android.Manifest
//import com.amazonaws.regions.Regions


object Constants {

    object ApiKeys {

        const val AUTHORIZATION = "authorization"
        const val LANG = "lang"
        const val STATUS = "status"
        const val ROLE = "role"
        const val PATIENT = "patient"

        const val RECIVERID = "receiverID"

        const val EMAIL = "email"
        const val COUNTRYCODE = "countrycode"
        const val MOBILENUMBER = "mobileNumber"
        const val PHONE = "phone"

        const val PASSWORD = "password"
        const val OLDPASSWORD = "oldpassword"
        const val NAME = "name"
        const val FIRSTNAME = "firstname"
        const val LASTNAME = "lastname"


        const val OTP = "otp"

        const val SOCIALTOKEN = "socialToken"
        const val TYPE = "type"

        const val OS = "os"
        const val FCMTOKEN = "fcmToken"
        const val ANDROID = "android"
        const val TOKEN = "token"

        const val IMAGEURL = "imageUrl"
        const val PROFILEPIC = "profilePic"

        const val GENDER = "gender"
        const val DOB = "dob"
        const val BLOOD_GROUP = "bloodGroup"
        const val HEIGHT = "height"
        const val WEIGHT = "weight"

        const val LATITUDE = "latitude"
        const val LONGITUDE = "longitude"

        const val BOOKING_LATITUDE = "bookingLatitude"
        const val BOOKING_LONGITUDE = "bookingLongitude"
        const val ISVIRTUALBOOKING = "isVirtualBooking"

        const val SPECIALITYID = "specialityId"
        const val PAGE = "page"


        const val PROVIDERID = "providerId"
        const val BOOKINGDATE = "bookingDate"
        const val BOOKINGTIME = "bookingTime"
        const val BOOKINGPERIOD = "bookingPeriod"
        const val LOCATION = "location"

        const val PATIENTID = "patientId"
        const val PATIENTTYPE = "patientType"
        const val PATIENTNAME = "patientName"
        const val PATIENTAGE = "patientAge"
        const val PATIENTGENDER = "patientGender"
        const val PREVIOUSISSUE = "previousIssue"

        const val BOOKINGID = "bookingId"
        const val REVIEW = "review"
        const val RATING = "rating"
        const val METHOD = "method"

        const val PROVIDERTYPE = "providerType"

        const val DATE = "date"


        const val CHANNELNAME = "channelName"
        const val FROM = "from"
        const val TO = "to"
        const val CALLTYPE = "callType"
        const val ID = "id"

        const val AMOUNT = "amount"
        const val TRANSACTIONID = "transactionId"
        const val STREAM = "stream"
        const val CHAT = "chat"


    }

    object IntentKeys {

        const val HOMETYPE = "homeType"
        const val LOCATION = "location"
        const val LATITUDE = "latitude"
        const val LONGITUDE = "longitude"

        const val SPECIALITYLIST = "speacilityList"
        const val SPECIALITYID = "specialityId"
        const val EMAIL = "email"
        const val NAME = "name"
        const val PROFILE_PICTURE = "profilepic"
        const val SOCIALTOKEN = "sociualToken"
        const val LOGINTYPE = "loginType"

        const val DOCTOR_DATA = "dcotord_Data"
        const val REVIEW_LIST = "review_list"
        const val BOOKINGFLOW = "bookingFlow"
        const val BOOKINGTYPE = "bookingType"
        const val AMOUNT = "amount"

        const val CONSULTATIONTYPE = "consultationType"
        const val HEADER = "header"

        const val PROVIDERID = "providerId"
        const val BOOKINGID = "bookingId"
        const val ARTICLEID = "articleId"
        const val ARTICLELIST = "articleList"

        const val PAYMENTDETAIL = "paymentDetail"
        const val BACKPRESSALLOWED = "backpressAllowed"

        const val FINISHACT = "finishActivity"

        const val DOCTORSID = "doctorsId"
        const val DOCTORNAME = "doctorsName"
        const val DOCTORIMAGE = "doctorsImage"

    }

    object SocketKey {
        const val LATUTUDE = "latitude"
        const val LONGITUDE = "longitude"

        const val SENDERID = "senderID"
        const val RECIVERID = "receiverID"
        const val CONTENT = "content"
        const val CONTENTTYPE = "contentType"
        const val TIME = "time"
        const val SENDERTYPE = "senderType"
        const val ID = "id"
        const val TYPE = "type"
        const val BOOKINGID = "bookingId"

    }

    object SocialLogin {
        const val GOOGLE = "google"
        const val FACEBOOK = "facebook"
    }

    object Permission {

        const val CAMERA_PERMISSIONS = 201
        const val READ_STORAGE_PERMISSIONS = 202
        const val CAMERA_STORAGE_PERMISSIONS = 203
        const val LOCATION_PERMISSION = 204
        const val COURSE_LOCATION_PERSISSION = 205

        const val VIDEO_CALL_PERMISSION = 206
        const val AUDIO_CALL_PERMISSION = 207


        val CAMERA_PERM_LIST = arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        val READ_STORAGE_PERM_LIST = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
        val COURSE_LOCATION_PERM_LIST = arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION)
        val LOCATION_PERMISSION_PERMISSON_LIST = arrayOf(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
        val AUDIO_CALL_PERMISSION_LIST = arrayOf(Manifest.permission.RECORD_AUDIO)
        val VIDEO_CALL_PERMISSION_LIST =
            arrayOf(Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA)
    }

    object RequestCode {

        const val RequestCode = "RequestCode"
        const val PLACEREQUESTCODE = 100
        const val CAMERA_INTENT = 101
        const val GALLERY_INTENT = 102

        const val ILLNESSREQUESTCODE = 104
        const val INSURANCEREQUESTCODE = 105

        const val GPS_REQUEST = 106
        const val LOCATION_REQUEST = 107
        const val PAYPAL_REQUEST_CODE = 108

    }

   /* //image Upload from amazon s3
    object AWS {
        const val POOL_ID: String = BuildConfig.POOL_ID
        const val BASE_S3_URL: String = BuildConfig.BASE_S3_URL
        const val ENDPOINT: String = BuildConfig.ENDPOINT
        const val BUCKET_NAME: String = BuildConfig.BUCKET_NAME
        val REGION = Regions.US_EAST_1
    }

*/
    object IntentFragment {
        const val LOGIN = "Login"
        const val REGISTER = "register"
        const val FORGOTPASSWORD = "forgotPassword"
        const val RESETPASSWORD = "resetPassword"
        const val SOCIALLOGINDETAILS = "socialLoginDetails"
    }

    object BookingType {
        const val DOCTOR = "doctor"
        const val AMBULANCE = "ambulance"
    }


    object NotificationIds {

        const val CALL_NOTIFICATION_ID = 1
        const val INCOMING_CALL_CHANNEL_ID = "IncomingCall"
        const val INCOMING_CALL_CHANNEL_NAME = "IncomingCall"

        const val HANGUP_CALL_CHANNEL_ID = "HangupCall"
        const val HANGUP_CALL_CHANNEL_NAME = "HangupCall"

    }

    object NotificationActions {

        const val ACCEPT_CALL = "Accept"
        const val REJECT_CALL = "Reject"
        const val HANGUP_CALL = "Hang Up"
        const val END_CALL = "End Call"

        const val CALLDIVERTION = "CallDivertion"

    }

    object NotificationIntentValues {

        const val CHANNEL_ID = "channelid"
        const val FROM_ID = "fromid"
        const val TO_ID = "toid"
        const val NAME = "docName"
        const val IMAGE = "docImage"
        const val CALL_TYPE = "voiceVideo"
        const val CALL_FROM = "incomingOutgoing"
        const val ID = "id"
        const val CALLDIVERTION = "CallDivertion"
        const val CHANNELNAME = "channelName"
        const val BOOKINGDATETIME = "bookingdateAndTime"
        const val BOOKINGID= "bookingId"
    }

    object ChatTypes {

        const val VIDEO_CALL = "video"
        const val VOICE_CALL = "voice"
        const val INCOMING_CALL = "incoming"
        const val OUTGOING_CALL = "outgoing"
        const val ENDCALL = "endcall"
    }

    object EventBusKeys {

        const val ACCEPT_CALL = "acceptcall"
        const val DOCTOR_ACCEPTED_CALL = "doctoracceptcall"
        const val DOCTOR_REJECTED_CALL = "doctorrejectcall"
        const val REJECT_CALL = "rejectCall"
        const val HANGUP_CALL = "hangupcall"
        const val END_CALL = "endcall"


    }
}