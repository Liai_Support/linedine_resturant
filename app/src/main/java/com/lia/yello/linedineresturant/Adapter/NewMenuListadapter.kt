package com.lia.yello.linedineresturant.Adapter

import android.content.Context
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedineresturant.Fragment.ItemDetailFragment
import com.lia.yello.linedineresturant.Fragment.MenuOrderFragment
import com.lia.yello.linedineresturant.Model.MenuDetailJson2
import com.lia.yello.linedineresturant.Model.MenuDetailss
import com.lia.yello.linedineresturant.R
import com.lia.yello.linedineresturant.Utils.UiUtils
import java.util.*
import kotlin.collections.ArrayList

class NewMenuListadapter(
    var menuOrderFragment: MenuOrderFragment,
   // var addNewMenuListActivity: AddNewMenuListActivity,
    var context: Context,
    var list: ArrayList<MenuDetailss>?,
    var iscolsed: Boolean,
    var menuquatity: MutableList<Int>?,
    var menuids: MutableList<Int>

) :
    RecyclerView.Adapter<NewMenuListadapter.HomeHeaderViewHolder>() {
    private lateinit var fragmentTransaction: FragmentTransaction
    private var itemDetailFragment: ItemDetailFragment? = null
    public var cost:Double? = 0.0
    var count:Int=0
    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var title: TextView
        var price: TextView
        var txtquantity: TextView
        var cirlce: ImageView
        var cirlce2: ImageView
        var plus: ImageView
        var minus: ImageView


        init {
            title = view.findViewById(R.id.menutitle)
            price = view.findViewById(R.id.menuprice)
            txtquantity = view.findViewById(R.id.menuquantity)
            cirlce = view.findViewById(R.id.cirlce)
            cirlce2 = view.findViewById(R.id.cirlce2)
            plus = view.findViewById(R.id.imgplus)
            minus = view.findViewById(R.id.imgminus)



        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.newmenulist,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {

        Log.d("asdf",""+menuOrderFragment.menulist!!.size)
     //   Log.d("list!![position].menu_id",""+list!![position].menu_id)
       // Log.d("list!![position].menu_id",""+list!![position].item)
      //  count=Integer.parseInt(holder.txtquantity.text.toString())
       // holder.delete.visibility=View.GONE

          count =0
        for (i in 0 until menuOrderFragment .menulist!!.size){
                Log.d("rrffrrfrfr","nmmnnmnmnm")

            if(menuOrderFragment.menulist!![i].menuid == list!![position].menu_id){

                Log.d("bvdsbvdsbsd","yesitstrue")
                Log.d("hdchbdchd",""+  menuOrderFragment.menulist!![i].quantity.toString()  )
                holder.cirlce.backgroundTintList = ContextCompat.getColorStateList(context,R.color.colorPrimary)

                holder.txtquantity.text = menuOrderFragment.menulist!![i].quantity.toString()

                    }

        }



        holder.title.text = list!![position].item

        holder.price.text =  String.format(Locale("en", "US"),"%.2f",list!![position].price)+ " SAR"



        menuOrderFragment.menulist2!!.add(
            MenuDetailJson2(
                list!![position].menu_id!!,
                Integer.parseInt(holder.txtquantity.text.toString())    ,
                        list!![position].price!!.toDouble()
            )
        )




        menuOrderFragment.jsonArrayy=
            menuOrderFragment.mapper.writeValueAsString(menuOrderFragment.menulist2)
        Log.d("lppppp", "" + menuOrderFragment.jsonArrayy)







        holder.plus.setOnClickListener(View.OnClickListener {

            for (i in 0 until menuOrderFragment.menulist2!!.size) {
                if (list!![position].menu_id == menuOrderFragment.menulist2!![i].menuid) {
                    count=Integer.parseInt(holder.txtquantity.text.toString())
                   // count=menuOrderFragment.menulist!![i].quantity
                    count++
                    holder.txtquantity.setText("" + count)
                    holder.cirlce.backgroundTintList = ContextCompat.getColorStateList(context,R.color.colorPrimary)

                    Log.d("count", "" + count)

                    menuOrderFragment.menulist2!!.remove(
                        MenuDetailJson2(
                            menuOrderFragment.menulist2!![i].menuid,
                            menuOrderFragment.menulist2!![i].quantity,
                            menuOrderFragment.menulist2!![i].cost
                        )
                    )
                    menuOrderFragment.menulist2!!.add(
                        MenuDetailJson2(
                            list!![position].menu_id!!,
                            count,
                            list!![position].price!!
                        )
                    )

                    menuOrderFragment.jsonArrayy =
                        menuOrderFragment.mapper.writeValueAsString(menuOrderFragment.menulist2)
                    Log.d("ghb ", "" + menuOrderFragment.jsonArrayy)

                    break

                }else{

                }
            }



        })

        holder.minus.setOnClickListener(View.OnClickListener {
            count=Integer.parseInt(holder.txtquantity.text.toString())

            if (count == 0 ) {
                holder.txtquantity.setText("" + count)
             //   UiUtils.showSnack(holder.minus, context.getString(R.string.itemcannotbelessthan1))
                holder.cirlce.backgroundTintList = ContextCompat.getColorStateList(context,R.color.grey1)



            } else {
                for (i in 0 until menuOrderFragment.menulist2!!.size) {
                    if (list!![position].menu_id == menuOrderFragment.menulist2!![i].menuid) {
                        count=Integer.parseInt(holder.txtquantity.text.toString())
                        // count=menuOrderFragment.menulist!![i].quantity
                        count--

                        if (count == 0 ) {
                            holder.txtquantity.setText("" + count)
                            //   UiUtils.showSnack(holder.minus, context.getString(R.string.itemcannotbelessthan1))
                            holder.cirlce.backgroundTintList = ContextCompat.getColorStateList(context,R.color.grey1)



                        }else{
                            holder.txtquantity.setText("" + count)
                            //   UiUtils.showSnack(holder.minus, context.getString(R.string.itemcannotbelessthan1))
                            holder.cirlce.backgroundTintList = ContextCompat.getColorStateList(context,R.color.colorPrimary)

                        }
                       // holder.txtquantity.setText("" + count)
                        Log.d("count", "" + count)

                        menuOrderFragment.menulist2!!.remove(
                            MenuDetailJson2(
                                menuOrderFragment.menulist2!![i].menuid,
                                menuOrderFragment.menulist2!![i].quantity,
                                menuOrderFragment.menulist2!![i].cost
                            )
                        )
                        menuOrderFragment.menulist2!!.add(
                            MenuDetailJson2(
                                list!![position].menu_id!!,
                                count,
                                list!![position].price!!
                            )
                        )

                        menuOrderFragment.jsonArrayy =
                            menuOrderFragment.mapper.writeValueAsString(menuOrderFragment.menulist2)
                        Log.d("ghb ", "" + menuOrderFragment.jsonArrayy)

                        break

                    }
                }





            }
        })



    }




}