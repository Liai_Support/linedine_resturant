package com.lia.yello.linedineresturant.Utils

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.snackbar.Snackbar
import com.lia.yello.linedineresturant.R

object UiUtils {
    fun showSnack(view: View, content: String) {
        Snackbar.make(view, content, Snackbar.LENGTH_SHORT).show()
    }

    fun showLog(TAG: String, content: String) {
        Log.d(TAG, content)
    }

    fun loadImage(imageView: ImageView?, imageUrl: String?) {
        if (imageUrl == null || imageView == null) {
            return
        }

        Glide.with(imageView.context)
            .load(imageUrl)
            .apply(
                RequestOptions()
                   // .placeholder(R.drawable.logo)
                    .error(
                        R
                        .drawable.logo1)
            )
            .into(imageView)

    }

    fun loadImage(imageView: ImageView?, imageUrl: String?, placeHolder: Drawable) {
        if (imageUrl == null || imageView == null) {
            return
        }

        Glide.with(imageView.context)
            .load(imageUrl)
            .apply(
                RequestOptions()
                    .placeholder(placeHolder)
                    .error(placeHolder)
            )
            .into(imageView)

    }

    fun convertDpToPixel(dp: Int, context: Context): Float {
        return dp * (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    }

}
