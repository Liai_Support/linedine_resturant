package com.lia.yello.linedineresturant.Adapter

import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedineresturant.Fragment.MenuListFragment
import com.lia.yello.linedineresturant.Model.BookingListData
import com.lia.yello.linedineresturant.Model.MenuDetailJson
import com.lia.yello.linedineresturant.Model.MenuDetails
import com.lia.yello.linedineresturant.R
import com.lia.yello.linedineresturant.Utils.UiUtils
import com.lia.yello.linedineresturant.activity.BookingList
import com.lia.yello.linedineresturant.activity.MenuListActivity
import kotlinx.android.synthetic.main.fragment_menu_list.*
import kotlinx.android.synthetic.main.headerhome.*
import java.util.*
import kotlin.collections.ArrayList

class MenuListAdapter (
    var menuListFragment: MenuListFragment,
    var context: Context,
    var list: ArrayList<MenuDetails>?

) :
    RecyclerView.Adapter<MenuListAdapter.HomeHeaderViewHolder>() {
    var count:Int=0
    var qnt:Int=0

    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {


        var title: TextView
        var price: TextView
        var txtquantity: TextView
        var cirlce: ImageView
        var cirlce2: ImageView
        var plus: ImageView
        var minus: ImageView
        var delete: ImageView
        var linrmenu: LinearLayout


        init {
            title = view.findViewById(R.id.menutitle)
            price = view.findViewById(R.id.menuprice)
            txtquantity = view.findViewById(R.id.menuquantity)
            cirlce = view.findViewById(R.id.cirlce)
            cirlce2 = view.findViewById(R.id.cirlce2)
            plus = view.findViewById(R.id.imgplus)
            minus = view.findViewById(R.id.imgminus)
            linrmenu = view.findViewById(R.id.linrmenu)
            delete = view.findViewById(R.id.delete)


        }




    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.menuadapnew,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {
        holder.cirlce.visibility=View.GONE
        holder.cirlce2.visibility=View.VISIBLE

        if (menuListFragment.sharedHelper!!.language == "ar"){
            holder.delete.rotation= 180F

        }
        if (menuListFragment.booked.equals("false")){
            holder.plus.isEnabled=false
            holder.minus.isEnabled=false

        }
        if (menuListFragment.statuspaid.equals("paid")){
            holder.linrmenu.isEnabled=false
            holder.plus.isEnabled=false
            holder.minus.isEnabled=false
        }


        holder.title.text = list!![position].menuname
        holder.txtquantity.text = list!![position].quantity.toString()
        holder.price.text=  String.format(Locale("en", "US"),"%.2f",list!![position].menucost)+ " SAR"


        holder.plus.setOnClickListener(View.OnClickListener {
            menuListFragment.subtotal=0.0
            menuListFragment.vat=0.0
            menuListFragment.totalamt=0.0

               // count=qnt
                count= (Integer.parseInt(holder.txtquantity.text as String))
                count += 1

            Log.d("asdfghygfdxsdcfvgbh;", "" + count)
            holder.txtquantity.text = count.toString()
            //2
            for (i in 0 until menuListFragment.menulist!!.size) {
                // 3 size
                Log.d("aswedrftyuiujhgv", "" + i)

                if (menuListFragment.menulist!![i].menuid == list!![position].menuid) {

                     qnt=menuListFragment.menulist!![i].quantity

                    menuListFragment.menulist!!.set(i, MenuDetailJson(list!![position].menuid!!,count, list!![i].menucost!!))
                    Log.d("menuplus", "" + menuListFragment.menulist)
                    // break


                }
                 //subtotal = 0

                menuListFragment.subtotal +=  menuListFragment.menulist!![i].cost* menuListFragment.menulist!![i].quantity

                Log.d("zxcvbnm",""+menuListFragment.subtotal)
                menuListFragment.vat = (menuListFragment.subtotal / 100.0f) * menuListFragment.sharedHelper!!.vat
                Log.d("vatttttttttttttt",""+menuListFragment.subtotal)

                menuListFragment.totalamt = menuListFragment.subtotal + menuListFragment.vat - 50


                menuListFragment.ordersubtotal.text = "" + String.format(Locale("en", "US"),"%.2f", menuListFragment.subtotal) + " SAR"
                menuListFragment.totalamtt.text = "" + String.format(Locale("en", "US"),"%.2f", menuListFragment.totalamt) + " SAR"
                menuListFragment.vat_txt.text = "" + String.format(Locale("en", "US"),"%.2f", menuListFragment.vat) + " SAR"
            }
        })


        holder.minus.setOnClickListener(View.OnClickListener {

            // count= list!![position].quantity!!
            menuListFragment.subtotal=0.0
            menuListFragment.vat=0.0
            menuListFragment.totalamt=0.0
            count= (Integer.parseInt(holder.txtquantity.text as String))
            holder.txtquantity.setText("" + count)
            if (count <= 1) {

                UiUtils.showSnack(holder.minus, context.getString(R.string.itemcannotbelessthan1))
            } else {
                // count= list!![position].quantity!! - 1
                count -= 1
                holder.txtquantity.text = count.toString()

                Log.d("asdfghjkl", "" + count)
                for (i in 0 until menuListFragment.menulist!!.size){

                    var costprice = count * list!![position].menucost!!
                    Log.d("costpriceee,", "" + costprice)

                    if (menuListFragment.menulist!![i].menuid == list!![position].menuid) {
                        menuListFragment.menulist!!.set(i, MenuDetailJson(list!![position].menuid!!, count, list!![i].menucost!!))
                        Log.d("minusmenu", "" + menuListFragment.menulist)

                    }
                    menuListFragment.subtotal +=  menuListFragment.menulist!![i].cost* menuListFragment.menulist!![i].quantity

                    Log.d("zxcvbnm",""+menuListFragment.subtotal)
                    menuListFragment.vat = (menuListFragment.subtotal / 100.0f) * menuListFragment.sharedHelper!!.vat
                    Log.d("vatttttttttttttt",""+menuListFragment.subtotal)

                    menuListFragment.totalamt = menuListFragment.subtotal + menuListFragment.vat - 50
                    menuListFragment.ordersubtotal.setText("" + String.format(Locale("en", "US"),"%.2f", menuListFragment.subtotal) + " SAR")
                    menuListFragment.totalamtt.setText("" + String.format(Locale("en", "US"),"%.2f", menuListFragment.totalamt) + " SAR")
                    menuListFragment.vat_txt.setText("" + String.format(Locale("en", "US"),"%.2f", menuListFragment.vat) + " SAR")

                }


            }
        })


    }
}