package com.lia.yello.linedineresturant.Model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CommonResponseUpdateToken: Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: UpdateToken? = null

}
class UpdateToken : Serializable {


    @SerializedName("status")
    var status: String? = null

    }