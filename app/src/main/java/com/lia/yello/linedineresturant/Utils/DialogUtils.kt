package com.lia.yello.linedineresturant.Utils

import android.app.Dialog
import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.LocationManager
import android.view.LayoutInflater
import android.view.WindowManager
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.lia.yello.linedineresturant.Interface.DialogCallBack
import com.lia.yello.linedineresturant.Interface.SingleTapListener
import com.lia.yello.linedineresturant.R
import kotlin.math.roundToInt
import kotlinx.android.synthetic.main.dialog_loader.*


object DialogUtils {

    var loaderDialog: Dialog? = null
    var noInternetDialog: Dialog? = null
    var paymentDialog: Dialog? = null
    private var isShowingDialog = false


    fun showAlert(
        context: Context,
        singleTapListener: SingleTapListener,
        content: String
    ) {
        var dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_alert_single)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var contentView = dialog.findViewById<TextView>(R.id.content)
        var ok = dialog.findViewById<TextView>(R.id.ok)

        contentView.text = content



        ok.setOnClickListener {

            dialog.dismiss()
            singleTapListener.singleTap()
        }

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.7).roundToInt()

        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)
        dialog.setOnCancelListener {
            singleTapListener.singleTap()
        }

        dialog.show()

    }

    fun showAlertWithHeader(
        context: Context,
        singleTapListener: SingleTapListener,
        content: String,
        headerVal: String
    ) {
        var dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_alert_single)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var contentView = dialog.findViewById<TextView>(R.id.content)
        var header = dialog.findViewById<TextView>(R.id.header)
        var ok = dialog.findViewById<TextView>(R.id.ok)

        contentView.text = content
        header.text = headerVal


        ok.setOnClickListener {

            dialog.dismiss()
            singleTapListener.singleTap()
        }

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.7).roundToInt()

        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)

        dialog.show()

    }

    fun showAlertDialog(
        context: Context,
        content: String,
        title: String,
        positiveText: String,
        negativeText: String,
        callBack: DialogCallBack
    ) {
        var dialog = Dialog(context)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.dialog_alert)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.8).roundToInt()
        var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()
        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)


        var headerTextView = dialog.findViewById<TextView>(R.id.header)
        var contentTextView = dialog.findViewById<TextView>(R.id.content)

        var cancel = dialog.findViewById<TextView>(R.id.cancel)
        var ok = dialog.findViewById<TextView>(R.id.ok)

        headerTextView.text = title
        contentTextView.text = content

        cancel.text = negativeText
        ok.text = positiveText

        cancel.setOnClickListener {
            callBack.onNegativeClick()
            dialog.dismiss()
        }

        ok.setOnClickListener {
            callBack.onPositiveClick()
            dialog.dismiss()
        }



        dialog.show()

    }

    fun showAlertDialogPayment(
        context: Context,
        content: String,
        title: String,
        positiveText: String,
        negativeText: String,
        callBack: DialogCallBack
    ) {

        paymentDialog?.let {
            if (it.isShowing) {
                return
            }
        }

        paymentDialog = Dialog(context)
        paymentDialog?.setCancelable(false)
        paymentDialog?.setCanceledOnTouchOutside(false)
        paymentDialog?.setContentView(R.layout.dialog_alert)
        paymentDialog?.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        var width: Int = (context.resources.displayMetrics.widthPixels * 0.8).roundToInt()
        var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()

        paymentDialog?.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)


        var headerTextView = paymentDialog?.findViewById<TextView>(R.id.header)
        var contentTextView = paymentDialog?.findViewById<TextView>(R.id.content)

        var cancel = paymentDialog?.findViewById<TextView>(R.id.cancel)
        var ok = paymentDialog?.findViewById<TextView>(R.id.ok)

        headerTextView?.text = title
        contentTextView?.text = content

        cancel?.text = negativeText
        ok?.text = positiveText

        cancel?.setOnClickListener {
            callBack.onNegativeClick()
            paymentDialog?.dismiss()
        }

        ok?.setOnClickListener {
            callBack.onPositiveClick()
            paymentDialog?.dismiss()
        }



        paymentDialog?.show()

    }


//    fun showDialog(
//        context: Context,
//        header: String,
//        content: String,
//        positiveText: String,
//        negativeText: String,
//        dialogCallback: DialogCallback
//    ) {
//
//        var alertDialog = AlertDialog.Builder(context)
//        alertDialog.setTitle(header)
//        alertDialog.setMessage(content)
//        alertDialog.setCancelable(false)
//        alertDialog.setPositiveButton(positiveText) { _, _ ->
//            dialogCallback.positiveClickListner()
//        }
//        alertDialog.setNegativeButton(negativeText) { _, _ ->
//            dialogCallback.negativeClickListner()
//        }
//        alertDialog.show()
//    }

    fun showLoader(context: Context) {
        if (loaderDialog != null) {
            if (loaderDialog!!.isShowing) {
                loaderDialog!!.dismiss()
            }
        }
        loaderDialog = Dialog(context)
        loaderDialog?.setCancelable(false)
        loaderDialog?.setCanceledOnTouchOutside(false)
        loaderDialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        loaderDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val inflater = LayoutInflater.from(context)
        val view = inflater?.inflate(R.layout.dialog_loader, null)
        if (view != null) {
            loaderDialog!!.setContentView(view)
        }

     //   var animation = loaderDialog?.findViewById<com.airbnb.lottie.LottieAnimationView>(R.id.anim_view)

        loaderDialog!!.anim_view.playAnimation()
        if (!loaderDialog?.isShowing!!) {
            loaderDialog?.show()
        }
    }
    fun dismissLoader() {
        if (loaderDialog != null) {
            if (loaderDialog!!.isShowing) {
                loaderDialog!!.dismiss()
            }
        }
    }

    //    fun noInternetDialog(context: Context, singleTapListener: SingleTapListener) {
//
//        if (noInternetDialog != null) {
//            if (isShowingDialog) {
//
//            }
//        }
//
//        if (!isShowingDialog) {
//            noInternetDialog = Dialog(context, android.R.style.Theme_Light_NoTitleBar_Fullscreen)
//            noInternetDialog?.setCancelable(false)
//            noInternetDialog?.setCanceledOnTouchOutside(false)
//            noInternetDialog?.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT,
//                    WindowManager.LayoutParams.MATCH_PARENT)
//
//            val inflater = LayoutInflater.from(context)
//            val view = inflater?.inflate(R.layout.dialog_no_internet, null)
//            if (view != null) {
//                noInternetDialog!!.setContentView(view)
//            }
    object DialogUtils {

        var loaderDialog: Dialog? = null
        var noInternetDialog: Dialog? = null
        var paymentDialog: Dialog? = null
        private var isShowingDialog = false


        fun showAlert(
            context: Context,
            singleTapListener: SingleTapListener,
            content: String
        ) {
            var dialog = Dialog(context)
            dialog.setCancelable(true)
            dialog.setCanceledOnTouchOutside(true)
            dialog.setContentView(R.layout.dialog_alert_single)
            dialog.window?.setBackgroundDrawable(
                ColorDrawable(
                    ContextCompat.getColor(
                        context,
                        R.color.transparent
                    )
                )
            )

            var contentView = dialog.findViewById<TextView>(R.id.content)
            var ok = dialog.findViewById<TextView>(R.id.ok)

            contentView.text = content



            ok.setOnClickListener {

                dialog.dismiss()
                singleTapListener.singleTap()
            }

            var width: Int = (context.resources.displayMetrics.widthPixels * 0.7).roundToInt()

            dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)
            dialog.setOnCancelListener {
                singleTapListener.singleTap()
            }

            dialog.show()

        }

        fun showAlertWithHeader(
            context: Context,
            singleTapListener: SingleTapListener,
            content: String,
            headerVal: String
        ) {
            var dialog = Dialog(context)
            dialog.setCancelable(true)
            dialog.setCanceledOnTouchOutside(true)
            dialog.setContentView(R.layout.dialog_alert_single)
            dialog.window?.setBackgroundDrawable(
                ColorDrawable(
                    ContextCompat.getColor(
                        context,
                        R.color.transparent
                    )
                )
            )

            var contentView = dialog.findViewById<TextView>(R.id.content)
            var header = dialog.findViewById<TextView>(R.id.header)
            var ok = dialog.findViewById<TextView>(R.id.ok)

            contentView.text = content
            header.text = headerVal


            ok.setOnClickListener {

                dialog.dismiss()
                singleTapListener.singleTap()
            }

            var width: Int = (context.resources.displayMetrics.widthPixels * 0.7).roundToInt()

            dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)

            dialog.show()

        }

        fun showAlertDialog(
            context: Context,
            content: String,
            title: String,
            positiveText: String,
            negativeText: String,
            callBack: DialogCallBack
        ) {
            var dialog = Dialog(context)
            dialog.setCancelable(false)
            dialog.setCanceledOnTouchOutside(false)
            dialog.setContentView(R.layout.dialog_alert)
            dialog.window?.setBackgroundDrawable(
                ColorDrawable(
                    ContextCompat.getColor(
                        context,
                        R.color.transparent
                    )
                )
            )

            var width: Int = (context.resources.displayMetrics.widthPixels * 0.8).roundToInt()
            var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()

            dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)


            var headerTextView = dialog.findViewById<TextView>(R.id.header)
            var contentTextView = dialog.findViewById<TextView>(R.id.content)

            var cancel = dialog.findViewById<TextView>(R.id.cancel)
            var ok = dialog.findViewById<TextView>(R.id.ok)

            headerTextView.text = title
            contentTextView.text = content

            cancel.text = negativeText
            ok.text = positiveText

            cancel.setOnClickListener {
                callBack.onNegativeClick()
                dialog.dismiss()
            }

            ok.setOnClickListener {
                callBack.onPositiveClick()
                dialog.dismiss()
            }



            dialog.show()

        }

        fun showAlertDialogPayment(
            context: Context,
            content: String,
            title: String,
            positiveText: String,
            negativeText: String,
            callBack: DialogCallBack
        ) {

            paymentDialog?.let {
                if (it.isShowing) {
                    return
                }
            }

            paymentDialog = Dialog(context)
            paymentDialog?.setCancelable(false)
            paymentDialog?.setCanceledOnTouchOutside(false)
            paymentDialog?.setContentView(R.layout.dialog_alert)
            paymentDialog?.window?.setBackgroundDrawable(
                ColorDrawable(
                    ContextCompat.getColor(
                        context,
                        R.color.transparent
                    )
                )
            )

            var width: Int = (context.resources.displayMetrics.widthPixels * 0.8).roundToInt()
            var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()

            paymentDialog?.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)


            var headerTextView = paymentDialog?.findViewById<TextView>(R.id.header)
            var contentTextView = paymentDialog?.findViewById<TextView>(R.id.content)

            var cancel = paymentDialog?.findViewById<TextView>(R.id.cancel)
            var ok = paymentDialog?.findViewById<TextView>(R.id.ok)

            headerTextView?.text = title
            contentTextView?.text = content

            cancel?.text = negativeText
            ok?.text = positiveText

            cancel?.setOnClickListener {
                callBack.onNegativeClick()
                paymentDialog?.dismiss()
            }

            ok?.setOnClickListener {
                callBack.onPositiveClick()
                paymentDialog?.dismiss()
            }



            paymentDialog?.show()

        }


//    fun showDialog(
//        context: Context,
//        header: String,
//        content: String,
//        positiveText: String,
//        negativeText: String,
//        dialogCallback: DialogCallback
//    ) {
//
//        var alertDialog = AlertDialog.Builder(context)
//        alertDialog.setTitle(header)
//        alertDialog.setMessage(content)
//        alertDialog.setCancelable(false)
//        alertDialog.setPositiveButton(positiveText) { _, _ ->
//            dialogCallback.positiveClickListner()
//        }
//        alertDialog.setNegativeButton(negativeText) { _, _ ->
//            dialogCallback.negativeClickListner()
//        }
//        alertDialog.show()
//    }
/*

        fun showLoader(context: Context) {

            if (loaderDialog != null) {
                if (loaderDialog!!.isShowing) {
                    loaderDialog!!.dismiss()
                }
            }

            loaderDialog = Dialog(context)
            loaderDialog?.setCancelable(false)
            loaderDialog?.setCanceledOnTouchOutside(false)
            loaderDialog?.window?.setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT
            )
            loaderDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val inflater = LayoutInflater.from(context)
            val view = inflater?.inflate(R.layout.dialog_loader, null)
            if (view != null) {
                loaderDialog!!.setContentView(view)
            }

            loaderDialog!!.anim_view.playAnimation()

            if (!loaderDialog?.isShowing!!) {
                loaderDialog?.show()
            }

        }

        fun dismissLoader() {
            if (loaderDialog != null) {
                if (loaderDialog!!.isShowing) {
                    loaderDialog!!.dismiss()
                }
            }
        }
*/

//    fun noInternetDialog(context: Context, singleTapListener: SingleTapListener) {
//
//        if (noInternetDialog != null) {
//            if (isShowingDialog) {
//
//            }
//        }
//
//        if (!isShowingDialog) {
//            noInternetDialog = Dialog(context, android.R.style.Theme_Light_NoTitleBar_Fullscreen)
//            noInternetDialog?.setCancelable(false)
//            noInternetDialog?.setCanceledOnTouchOutside(false)
//            noInternetDialog?.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT,
//                    WindowManager.LayoutParams.MATCH_PARENT)
//
//            val inflater = LayoutInflater.from(context)
//            val view = inflater?.inflate(R.layout.dialog_no_internet, null)
//            if (view != null) {
//                noInternetDialog!!.setContentView(view)
//            }
//
//            noInternetDialog!!.tryAgain.setOnClickListener {
//                if (NetworkUtils.isNetworkConnected(context)) {
//                    singleTapListener.singleTap()
//                    dismissInternetDialog(object : SingleTapListener {
//                        override fun singleTap() {
//                            singleTapListener.singleTap()
//                        }
//
//                    })
//                }
//            }
//
//
//            noInternetDialog?.show()
//            isShowingDialog = true
//        }
//    }
//
//    fun dismissInternetDialog(singleTapListener: SingleTapListener) {
//        if (noInternetDialog != null) {
//            if (isShowingDialog) {
//                noInternetDialog!!.dismiss()
//                singleTapListener.singleTap()
//                isShowingDialog = false
//            }
//        }
//    }

        fun checkGpsIsEnabled(context: Context): Boolean {
            val locationManager = context.getSystemService(LOCATION_SERVICE) as LocationManager

            return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        }


    }
}