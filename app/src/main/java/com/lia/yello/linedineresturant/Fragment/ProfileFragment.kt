package com.lia.yello.linedineresturant.Fragment

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.*
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import com.lia.yello.linedineresturant.R
import com.lia.yello.linedineresturant.Session.SharedHelper
import com.lia.yello.linedineresturant.Utils.DialogUtils
import com.lia.yello.linedineresturant.Utils.UiUtils
import com.lia.yello.linedineresturant.ViewModel.MapViewModel
import com.lia.yello.linedineresturant.activity.LoginActivity
import com.lia.yello.linedineresturant.activity.RestuarantIntroActivity
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.popup_password.view.*
import androidx.lifecycle.Observer
import com.lia.yello.linedineresturant.activity.DashBoardActivity
import com.lia.yello.linedineresturant.databinding.FragmentMenuOrderBinding
import com.lia.yello.linedineresturant.databinding.FragmentProfileBinding
import kotlinx.android.synthetic.main.fragment_home.rootdash
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.header.language2
import kotlinx.android.synthetic.main.headerhome.*
import kotlinx.android.synthetic.main.popreslayout.view.*
import kotlinx.android.synthetic.main.popup_password.view.pass2
import kotlinx.android.synthetic.main.popup_password.view.resetpassconfirm

class ProfileFragment : Fragment(R.layout.fragment_profile) {
    private var viewmodel: MapViewModel? = null
    var sharedHelper: SharedHelper? = null
    var homeFragment:HomeFragment?=null
    var newpass1: String = ""
    var newpass2: String = ""
    private lateinit var fragmentTransaction: FragmentTransaction
    lateinit var logoutt:ImageView
    var binding: FragmentProfileBinding? = null;

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
         binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())
        viewmodel = ViewModelProvider(this).get(MapViewModel::class.java)
        val head = view.findViewById<TextView>(R.id.head)
      /*  logoutprofile = view.findViewById<RelativeLayout>(R.id.logoutprofile)
        ressetpass = view.findViewById<RelativeLayout>(R.id.ressetpass)*/
        head.setText(getString(R.string.profile))
        logoutt = view.findViewById<ImageView>(R.id.logout1)

        edt_name.setText(sharedHelper!!.name)
        edt_mail.setText(sharedHelper!!.email)
     /*   edt_name.isClickable=false
        edt_mail.isClickable=false*/

        if (sharedHelper!!.language == "ar"){
            logout1.rotation= 180F
            imglog.rotation= 180F



        }else{
            imglog.setImageDrawable(
                ContextCompat.getDrawable(
                    requireContext(), // Context
                    R.drawable.ic_icon_feather_log_out // Drawable
                )
            )
        }
        logoutprofile.setOnClickListener(View.OnClickListener {



          sharedHelper?.loggedIn = false
                 sharedHelper?.token = ""
                 sharedHelper?.id = 0
                 sharedHelper?.name = ""
                 sharedHelper?.email = ""
                 //sharedHelper?.mobileNumber = ""
                 // sharedHelper?.countryCode = ""

                 val intent =
                         Intent(requireContext(), LoginActivity::class.java)
                 intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                 intent.flags =
                         Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                 startActivity(intent)


        })
        language.setOnClickListener{
            if (sharedHelper!!.language.equals("ar")){
                sharedHelper?.language = "en"
                reloadApp()
            }else if (sharedHelper!!.language.equals("en")){
                sharedHelper?.language = "ar"
                reloadApp()
            }
        }
        if (sharedHelper!!.language.equals("ar")) {

            language.text = "EN"
        }else{
            language.text = "AR"

        }

        logoutt.setOnClickListener(View.OnClickListener {

            homeFragment = HomeFragment()
            fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, homeFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()

        })
        ressetpass.setOnClickListener(View.OnClickListener {
            showDialog4()
        })

        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {

                homeFragment = HomeFragment()
                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, homeFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()



            }
        })

    }


    private fun showDialog4() {

        val mDialogView = LayoutInflater.from(requireContext()).inflate(R.layout.popreslayout, null)
        val mBuilder = AlertDialog.Builder(requireContext()).setView(mDialogView).setCancelable(true)
        val mAlertDialog = mBuilder.show()
        val layoutParams = WindowManager.LayoutParams()
        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        layoutParams.copyFrom(mAlertDialog.getWindow()?.getAttributes())
        layoutParams.width = ((displayWidth * 0.9f).toInt())
        mAlertDialog.getWindow()?.setAttributes(layoutParams)
        mAlertDialog.getWindow()?.setGravity(Gravity.CENTER)
        mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))


        mDialogView.closeend.setOnClickListener{
            mAlertDialog.dismiss()

        }
        mDialogView.resetpassconfirm.setOnClickListener {



            val imm: InputMethodManager =
                activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(mDialogView.windowToken, 0)

            if (mDialogView.passs1.text.toString().length==0 && mDialogView.passs2.text.toString().length==0 && mDialogView.passs3.text.toString().length==0 ){

                UiUtils.showSnack(profileroot,getString(R.string.enterallfields))

            } else if (mDialogView.passs1.text.toString().length==0 || mDialogView.passs2.text.toString().length==0 || mDialogView.passs3.text.toString().length==0){
                if( mDialogView.passs1.text.toString().length==0){

                    UiUtils.showSnack(profileroot,getString(R.string.enterallfields))

                }
                else if(mDialogView.passs2.text.toString().length==0){

                    UiUtils.showSnack(profileroot,getString(R.string.enterallfields))

                }

                else if(mDialogView.passs3.text.toString().length==0){

                    UiUtils.showSnack(profileroot,getString(R.string.enterallfields))

                }

                else if(mDialogView.passs1.text.toString() ==sharedHelper!!.oldpassword){

                    UiUtils.showSnack(profileroot,getString(R.string.passwordmismatch))

                    // mDialogView.oldpass.setError(getString(R.string.enterallfields))
                }
            }
            else{
                newpass1 = mDialogView.passs1.text.toString()
                newpass2 = mDialogView.passs2.text.toString()


                if(mDialogView.passs2.text.toString()!=mDialogView.passs3.text.toString()){

                    UiUtils.showSnack(profileroot, getString(R.string.passwordmismatch))

                }
                else if (mDialogView.passs1.text.toString()!=sharedHelper!!.oldpassword){

                    //  mAlertDialog.dismiss()
                    UiUtils.showSnack(profileroot, getString(R.string.oldpasswordisincorrect))

                }else{
                    if (newpass2.length<6){

                        UiUtils.showSnack(profileroot, getString(R.string.passwordlenghtisshort))

                    }/*else if (sharedHelper!!.oldpassword!=mDialogView.passs1.text.toString()){

                        UiUtils.showSnack(profileroot,getString(R.string.oldpassisincorrect))

                    }*/else{
                        resetpassword(newpass1, newpass2)
                        mAlertDialog.dismiss()
                    }
                }





            }











          /*  if (sharedHelper!!.oldpassword!=mDialogView.pass1.text.toString()){

                UiUtils.showSnack(profileroot,getString(R.string.oldpassisincorrect))
                //  mAlertDialog.dismiss()

            }else{

                newpass1 = mDialogView.pass1.text.toString()
                // new pass
                newpass2 = mDialogView.pass2.text.toString()
                // old pass
                resetpassword(newpass1, newpass2)
                mAlertDialog.dismiss()
            }
*/



        }
    }



    private fun resetpassword(finalpassword: String, finalpassword2: String){
        DialogUtils.showLoader(requireContext())
        viewmodel?.getresetpassword(requireContext(), finalpassword2,finalpassword )
            ?.observe(requireActivity(),
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                UiUtils.showSnack(profileroot, "msg")

                            } else {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(profileroot, msg)
                                       sharedHelper!!.oldpassword=finalpassword
                               /*     sharedHelper?.loggedIn = false
                                    sharedHelper?.token = ""
                                    sharedHelper?.id = 0
                                    sharedHelper?.name = ""
                                    sharedHelper?.email = ""


                                    val intent =
                                        Intent(requireContext(), LoginActivity::class.java)
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                    intent.flags =
                                        Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                    startActivity(intent)
*/
                                }

                            }
                        }

                    }
                })
    }


    private fun reloadApp() {



        val intent = Intent(requireContext(), DashBoardActivity::class.java)

        intent.putExtra("langfrom", "Profilefrag")

        startActivity(intent)



    }


}