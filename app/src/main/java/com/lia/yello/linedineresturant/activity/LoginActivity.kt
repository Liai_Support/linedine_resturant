package com.lia.yello.linedineresturant.activity

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.telephony.TelephonyManager
import android.text.Editable
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.lia.yello.linedineresturant.Fragment.MenuListFragment
import com.lia.yello.linedineresturant.Model.LoginData
import com.lia.yello.linedineresturant.R
import com.lia.yello.linedineresturant.Session.SharedHelper
import com.lia.yello.linedineresturant.Utils.BaseUtils
import com.lia.yello.linedineresturant.Utils.DialogUtils
import com.lia.yello.linedineresturant.Utils.UiUtils
import com.lia.yello.linedineresturant.ViewModel.MapViewModel
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_menu_list.*
import kotlinx.android.synthetic.main.popup_forget.view.*
import kotlinx.android.synthetic.main.popup_password.view.*

class LoginActivity : AppCompatActivity() {
    private var viewmodel: MapViewModel? = null
    var sharedHelper: SharedHelper? = null
    var finishAct = false
    var emailid: String = ""

    var verificationID: String = ""
    var loginstatus: String = ""
    var resid:Int=0
    var type=""
    var doubleBackToExitPressedOnce :Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        viewmodel = ViewModelProvider(this).get(MapViewModel::class.java)
        sharedHelper = SharedHelper(this)

        if (SharedHelper(this).loggedIn){


            val intent = Intent(this, DashBoardActivity::class.java)
            // intent.putExtra("langfrom", "inqueue")
            startActivity(intent)
        }


        logout2.setOnClickListener{
            val exitIntent=Intent(Intent.ACTION_MAIN)
            exitIntent.addCategory(Intent.CATEGORY_HOME)
            exitIntent.flags=Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(exitIntent)
        }





        emailtxt.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable) {

                if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailtxt.text.toString()).matches()){
                  //  UiUtils.showSnack(root, getString(R.string.entervalidmailid))
                    emailtxt.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(applicationContext,R.drawable.iccheckwhite), null)



                }else{
                    emailtxt.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(applicationContext,R.drawable.ic_icon_feather_check_square), null)

                }
            }
        })

    }


    fun Forget(view: View) {
        if (emailtxt.text.toString().trim() == ""){
            UiUtils.showSnack(root, "Please enter your emailid")
        } else if (!BaseUtils.isValidEmail(emailtxt.text.toString().trim())) {
            UiUtils.showSnack(root, "Please enter your valid emailid")
        } else {
            showDialog1(emailtxt.text.toString().trim())


        }

    }

    fun Termscondition(view: View) {

        val intent = Intent(
            "android.intent.action.VIEW",
            Uri.parse("https://lineandine.com/termsconditions.php")
        )
        startActivity(intent)


    }


    private fun tryAndPrefillPhoneNumber(): String? {
        if (checkCallingOrSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            val manager = getSystemService(TELEPHONY_SERVICE) as TelephonyManager
            //mPhoneNumber.setText(manager.line1Number)
            var hgh = "+"+manager.line1Number
            return hgh
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.READ_PHONE_STATE),
                0
            )
        }
        return tryAndPrefillPhoneNumber()
    }
    private fun showDialog1(email: String) {
      //  DialogUtils.showLoader(this)
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.popup_forget, null)
        val mBuilder = AlertDialog.Builder(this).setView(mDialogView).setCancelable(true)
        val  mAlertDialog = mBuilder.show()
        //mAlertDialog.getWindow()?.setLayout(100,40)
        val layoutParams = WindowManager.LayoutParams()
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        layoutParams.copyFrom(mAlertDialog.getWindow()?.getAttributes())
        /* layoutParams.width = WindowManager.LayoutParams.WRAP_CONTENT
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT*/
        layoutParams.width = ((displayWidth * 0.8f).toInt())
        //layoutParams.height = ((displayHeight * 0.5f).toInt())
        mAlertDialog.getWindow()?.setAttributes(layoutParams)
        mAlertDialog.getWindow()?.setGravity(Gravity.CENTER)
        mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.emailid.setText(email)
        mDialogView.confirm.setOnClickListener {
            emailid = mDialogView.emailid.text.toString()
            forgotpassword(emailid)
            mAlertDialog.dismiss()
        }
    }




    fun Login(view: View) {
     /*   if(!check1.isChecked()){
            snackbarToast(getString(R.string.pleasecheckthebox));
        }
        else */if (isValidInputs()) {
            DialogUtils.showLoader(this)
            viewmodel?.getlogin(this,emailtxt.text.toString().trim(), pswdtxt.text.toString().trim())
                    ?.observe(this,
                            Observer {
                                DialogUtils.dismissLoader()
                                it?.let {
                                    it.error?.let { error ->
                                        if (error) {
                                            it.message?.let { msg ->
                                                UiUtils.showSnack(root, getString(R.string.invalidcredentails))
                                                Log.d("qwsderfcgtbn","zserfvcgbhn")
                                            }
                                        } else {

                                            it.data?.let { data ->
                                                if (data.status.equals("active")){
                                                    handleResponse(data)
                                                    sharedHelper!!.oldpassword=pswdtxt.text.toString().trim()


                                                }
                                                else{
                                                    UiUtils.showSnack(root,getString(R.string.youcannotloginrightnowasadminclosedyourstatus))
                                                }

                                            }
                                        }
                                    }

                                }
                            })
        }


    }

    private fun handleResponse(data: LoginData) {

        data.user_mail?.let { sharedHelper?.email = it }
        data.user_id?.let { sharedHelper?.id = it }
        data.user_name?.let { sharedHelper?.name = it }
         sharedHelper!!.resid= data.resid!!
        Log.d("xderfdcvfgbv",""+data.resid!!)
        Log.d("zsdecvghb",""+data.user_id!!)
        Log.d("xsedcvgyuhn",""+data.user_mail!!)
        sharedHelper?.loggedIn = true
        loginstatus=data.status!!
        if (finishAct) {
            finish()
        } else {
            val intent =
                    Intent(this, DashBoardActivity::class.java)
            intent.putExtra("langfrom","dash")
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.flags =
                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

    }


    private fun isValidInputs(): Boolean {
        when {

            emailtxt.text.toString().trim() == "" -> {
                UiUtils.showSnack(root, getString(R.string.pleaseenteryouremailid))
                return false
            }
            !BaseUtils.isValidEmail(emailtxt.text.toString()) -> {
                UiUtils.showSnack(root, getString(R.string.pleaseentervalidemailid))
                return false
            }
            pswdtxt.text.toString().trim() == "" -> {
                UiUtils.showSnack(root, getString(R.string.pleaseenteryourpassword))
                return false
            }

            else -> return true
        }


    }

  /*  fun onConnectionFailed(p0: ConnectionResult) {
        TODO("Not yet implemented")
    }
*/
    private fun snackbarToast(msg: String) {
        var snackbar: Snackbar
        //contextView = findViewById(R.id.contextview);
        val view = findViewById<View>(R.id.signinbtn)
        Snackbar.make(view, msg, Snackbar.LENGTH_SHORT)
                .setTextColor(
                        ContextCompat.getColor(
                                this@LoginActivity,
                                R.color.design_default_color_error
                        )
                )
                .setBackgroundTint(ContextCompat.getColor(this@LoginActivity, R.color.white))
                .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                .show()
    }




    private fun forgotpassword(email: String){
        DialogUtils.showLoader(this)
        viewmodel?.getforgetpassword(this,email)
                ?.observe(this,
                        Observer {
                            DialogUtils.dismissLoader()

                            it?.let {
                                it.error?.let { error ->
                                    if (error) {

                                        it.message?.let { msg ->
                                            UiUtils.showSnack(root, msg)
                                        }

                                    } else {
                                        it.message?.let { msg ->
                                        UiUtils.showSnack(root, msg)
                                           // sharedHelper?.id = it.data!!.userid!!
                                           //
                                        }


                                    }
                                }

                            }
                        })
    }


    override fun onBackPressed() {
        // super.onBackPressed()

        if (doubleBackToExitPressedOnce) {
            val exitIntent=Intent(Intent.ACTION_MAIN)
            exitIntent.addCategory(Intent.CATEGORY_HOME)
            exitIntent.flags=Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(exitIntent)
            return
        }

        doubleBackToExitPressedOnce = true
        Toast.makeText(this, getString(R.string.pleaseclickbackagaintoexit), Toast.LENGTH_SHORT).show()

        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)

    }




}