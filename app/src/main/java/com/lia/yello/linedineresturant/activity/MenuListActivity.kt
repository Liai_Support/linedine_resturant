package com.lia.yello.linedineresturant.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.lia.yello.linedineresturant.Adapter.BookingListAdapter
import com.lia.yello.linedineresturant.Adapter.MenuListAdapter
import com.lia.yello.linedineresturant.Model.MenuDetailJson
import com.lia.yello.linedineresturant.Model.MenuDetails
import com.lia.yello.linedineresturant.R
import com.lia.yello.linedineresturant.Session.SharedHelper
import com.lia.yello.linedineresturant.Utils.DialogUtils
import com.lia.yello.linedineresturant.Utils.UiUtils
import com.lia.yello.linedineresturant.ViewModel.MapViewModel
import kotlinx.android.synthetic.main.activity_booking_list.*
import kotlinx.android.synthetic.main.activity_booking_list.recyclerbookinglist
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_menu_list.*
import kotlinx.android.synthetic.main.header.*
import java.io.Serializable

class MenuListActivity : AppCompatActivity() {
    var value:Int=0
    var bookingid:Int=0
    private var viewmodel: MapViewModel? = null
    var sharedHelper: SharedHelper? = null
    var list: ArrayList<MenuDetails>? = null
    var menulist:ArrayList<MenuDetailJson>? = ArrayList<MenuDetailJson>()
    val mapper = jacksonObjectMapper()


    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_list)
        value = intent.extras!!.getInt("tableid")
        bookingid = intent.extras!!.getInt("bookingid")
        Log.d("zxsxderfcvghyuijkmn",""+bookingid)

        viewmodel = ViewModelProvider(this).get(MapViewModel::class.java)
        sharedHelper = SharedHelper(this)
      //  getvalue(sharedHelper!!.resid)
        head.setText(getString(R.string.menulist))
        cancel.setOnClickListener(View.OnClickListener {

            val intent = Intent(this, AddNewMenuListActivity::class.java)
            val bundle = Bundle()
            bundle.putSerializable("menulist", menulist as Serializable)
          //  bundle.putString("menulist", menulist.toString())
            intent.putExtras(bundle)
            Log.d("serftgh",""+menulist)
           // Log.d("serftgh",""+menulist)
           // intent.putExtra("menulist",menulist as Serializable)
            startActivity(intent)
/*
        intent.putExtra("menulist",menulist as Serializable)
       // intent.putExtras(bundle)
           this.startActivity(intent)
            Log.d("serftgh",""+menulist)*/




        })


    }
}

