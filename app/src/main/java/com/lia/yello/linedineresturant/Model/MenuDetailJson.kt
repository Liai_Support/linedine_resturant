package com.lia.yello.linedineresturant.Model

import java.io.Serializable

data class MenuDetailJson(
    val menuid: Int,
    val quantity: Int,
    val cost: Double
)