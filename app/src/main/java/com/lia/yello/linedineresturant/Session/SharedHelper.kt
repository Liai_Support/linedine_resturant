package com.lia.yello.linedineresturant.Session

import android.content.Context

class SharedHelper(context: Context) {

    private var sharedPreference: SharedPref = SharedPref(context)

    var token: String
        get() : String {
            return sharedPreference.getKey("token")
        }
        set(value) {
            sharedPreference.putKey("token", value)
        }

    var fcmToken: String
        get() : String {
            return sharedPreference.getKey("fcmToken")
        }
        set(value) {
            sharedPreference.putKey("fcmToken", value)
        }

    var resid: Int
        get() : Int {
            return sharedPreference.getInt("resid")
        }
        set(value) {
            sharedPreference.putInt("resid", value)
        }

    var language: String
        get() : String {
            return if (sharedPreference.getKey("language") == "") {
                "en"
            } else {
                sharedPreference.getKey("language")
            }

        }
        set(value) {
            sharedPreference.putKey("language", value)
        }

    var id: Int
        get() : Int {
            return sharedPreference.getInt("id")
        }
        set(value) {
            sharedPreference.putInt("id", value)
        }

    var cartcount: Int
        get() : Int {
            return sharedPreference.getInt("cartcount")
        }
        set(value) {
            sharedPreference.putInt("cartcount", value)
        }



    var vat: Float
        get() : Float {
            return sharedPreference.getFloat("vat")
        }
        set(value) {
            sharedPreference.putFloat("vat", value)
        }

    var initialamount: Float
        get() : Float {
            return sharedPreference.getFloat("initialamount")
        }
        set(value) {
            sharedPreference.putFloat("initialamount", value)
        }

    var name: String
        get() : String {
            return sharedPreference.getKey("name")
        }
        set(value) {
            sharedPreference.putKey("name", value)
        }

    var email: String
        get() : String {
            return sharedPreference.getKey("email")
        }
        set(value) {
            sharedPreference.putKey("email", value)
        }


    var oldpassword: String
        get() : String {
            return sharedPreference.getKey("oldpassword")
        }
        set(value) {
            sharedPreference.putKey("oldpassword", value)
        }


    var mobileNumber: String
        get() : String {
            return sharedPreference.getKey("mobileNumber")
        }
        set(value) {
            sharedPreference.putKey("mobileNumber", value)
        }

    var imageUploadPath: String
        get() : String {
            return sharedPreference.getKey("imageUploadPath")
        }
        set(value) {
            sharedPreference.putKey("imageUploadPath", value)
        }
    var countryCode: String
        get() : String {
            return sharedPreference.getKey("countryCode")
        }
        set(value) {
            sharedPreference.putKey("countryCode", value)
        }

    var loggedIn: Boolean
        get() : Boolean {
            return sharedPreference.getBoolean("loggedIn")
        }
        set(value) {
            sharedPreference.putBoolean("loggedIn", value)
        }

    var iscurrentlocationseleted: Boolean
        get() : Boolean {
            return sharedPreference.getBoolean("iscurrentlocationseleted")
        }
        set(value) {
            sharedPreference.putBoolean("iscurrentlocationseleted", value)
        }



    var currentLat: String
        get() : String {
            return sharedPreference.getKey("currentLat")
        }
        set(value) {
            sharedPreference.putKey("currentLat", value)
        }

    var currentLng: String
        get() : String {
            return sharedPreference.getKey("currentLng")
        }
        set(value) {
            sharedPreference.putKey("currentLng", value)
        }

    var location: String
        get() : String {
            return sharedPreference.getKey("location")
        }
        set(value) {
            sharedPreference.putKey("location", value)
        }



    var currentlocation: String
        get() : String {
            return sharedPreference.getKey("currentLocation")
        }
        set(value) {
            sharedPreference.putKey("currentLocation", value)
        }

    var selectedLat: String
        get() : String {
            return sharedPreference.getKey("selectedLat")
        }
        set(value) {
            sharedPreference.putKey("selectedLat", value)
        }

    var selectedLng: String
        get() : String {
            return sharedPreference.getKey("selectedLng")
        }
        set(value) {
            sharedPreference.putKey("selectedLng", value)
        }

    var userImage: String
        get() : String {
            return sharedPreference.getKey("userImage")
        }
        set(value) {
            sharedPreference.putKey("userImage", value)
        }




    var adress: String
        get() : String {
            return sharedPreference.getKey("address")
        }
        set(value) {
            sharedPreference.putKey("address", value)
        }

    var chatTiming: Int
        get() : Int {
            return sharedPreference.getInt("chatTiming")
        }
        set(value) {
            sharedPreference.putInt("chatTiming", value)
        }

    var calendarTiming: Int
        get() : Int {
            return sharedPreference.getInt("calendarTiming")
        }
        set(value) {
            sharedPreference.putInt("calendarTiming", value)
        }




    var selectedaddressid: Int
        get() : Int {
            return sharedPreference.getInt("selectedaddressid")
        }
        set(value) {
            sharedPreference.putInt("selectedaddressid", value)
        }



    var ishistory: Boolean
        get() : Boolean {
            return sharedPreference.getBoolean("ishistory")
        }
        set(value) {
            sharedPreference.putBoolean("ishistory", value)
        }



}