package com.lia.yello.linedine.activity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class HomeResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: HomeData? = null


}


class HomeData : Serializable {



    @SerializedName("Vat")
    var vat: Double? = null

    @SerializedName("banner")
    var banner: ArrayList<Banner>? = null

    @SerializedName("resttypes")
    var resType: ArrayList<ResType>? = null

  @SerializedName("cuisines")
    var cuisines: ArrayList<Cuisines>? = null
}

class ResType : Serializable {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("image")
    var image: String? = null

    @SerializedName("type")
    var type: String? = null


}


class Cuisines : Serializable {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("cuisine")
    var cuisine: String? = null

}

class Banner : Serializable {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("image")
    var image: String? = null

}
