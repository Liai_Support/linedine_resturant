package com.lia.yello.linedine.adapter

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat.startActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.lia.yello.linedineresturant.Fragment.BookingListFragment
import com.lia.yello.linedineresturant.Fragment.HomeFragment
import com.lia.yello.linedineresturant.Model.TableDetail
import com.lia.yello.linedineresturant.R
import com.lia.yello.linedineresturant.activity.BookingList
import com.lia.yello.linedineresturant.activity.DashBoardActivity
import java.io.Serializable

class TableListAdapter(
    var homeFragment: HomeFragment,
     var context: Context,
    var list: ArrayList<TableDetail>?

) :
    RecyclerView.Adapter<TableListAdapter.HomeHeaderViewHolder>() {
    private lateinit var fragmentTransaction: FragmentTransaction
    public var bookingListFragment: BookingListFragment? = null
    var row_index: Int = -1
    var list2: ArrayList<Int>? = null

    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tableno: TextView
        var tablecount: TextView
        var linear_table: LinearLayout


        init {
            tableno = view.findViewById(R.id.tableno)
            tablecount = view.findViewById(R.id.menuquantitytxt)
            linear_table = view.findViewById(R.id.linear_table)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_subsubhome2,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {


        Log.d("gghghgh",""+list!!.size)
        Log.d("bookinglistsize",""+ list!![position].bookinglist!!.size)
         homeFragment.menuids!!.clear()



        if (list!![position].bookinglist!!.size!=null && list!![position].bookinglist!!.size!=0){

              for (i in 0 until list!![position].bookinglist!!.size){
                  if (list!![position].bookinglist!![i].bookingstatus!! == "booked"||list!![position].bookinglist!![i].bookingstatus!! == "processing"){

                      if (homeFragment.sharedHelper!!.ishistory==false){
                          Log.d("bbnbnnbnnnnb", list!![position].bookinglist!![i].bookingid!!.toString())

                          homeFragment.menuids.add(list!![position].bookinglist!![i].bookingid!!)
                          Log.d("kladlkdladl", homeFragment.menuids.size.toString())
                          holder.tablecount.visibility=View.VISIBLE
                          holder.tablecount.text= homeFragment.menuids!!.size.toString()
                      }


                  }else{
                      //homeFragment.menuids!!.clear()
                  }

              }

        }else {
            homeFragment.menuids!!.clear()

            holder.tablecount.visibility=View.GONE

        }


/*
     if (list!![position].bookinglist!!.size!=null && list!![position].bookinglist!!.size!=0){

         for (i in 0 until list!!.size){
             for (j in 0 until list!![i].bookinglist!!.size){
                 Log.d("hkdcdhjdchjdhj",""+ list!![i].bookinglist!![j].bookingstatus )
                 if (list!![i].bookinglist!![j].bookingstatus!! == "booked") {

                     Log.d("aertyuio",""+ list!![i].bookinglist!!.size)
                     holder.tablecount.visibility=View.VISIBLE
                     homeFragment.menuids.add(list!![i].bookinglist!![j].bookingid!!)
                     Log.d("edrtyuiophdueui", list!![i].bookinglist!![j].bookingid!!.toString())
                  // holder.tablecount.text= homeFragment.menuids.size.toString()

                 }else{
                     holder.tablecount.visibility=View.GONE

                 }
             }
         }

        }else {
            holder.tablecount.visibility=View.GONE

        }*/



           holder.tableno.text= list!![position].tablename.toString()
        holder.linear_table.setOnClickListener(View.OnClickListener {

            holder.linear_table.setBackgroundResource(R.drawable.paybg)

            val bundle = Bundle()
            bundle.putInt("tableid", list!![position].tableidd!!)
            bundle.putString("resimg", homeFragment.resimage)
            bookingListFragment = BookingListFragment()
            bookingListFragment!!.arguments = bundle
            fragmentTransaction =homeFragment.requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, bookingListFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()



       /*         val intent = Intent(dashBoardActivity, BookingList::class.java)
                intent.putExtra("tableid",list!![position].tableidd)
                dashBoardActivity.startActivity(intent)
                Log.d("tableidd",""+intent)
                Log.d("tableidd",""+list!![position].tableidd)*/



            })

    }
}