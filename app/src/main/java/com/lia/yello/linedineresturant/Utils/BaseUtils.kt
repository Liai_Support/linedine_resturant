package com.lia.yello.linedineresturant.Utils

import android.util.Log
import android.util.Patterns
import android.view.View
import com.google.android.material.snackbar.Snackbar

object BaseUtils {
    fun isValidEmail(email: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
    fun numberFormat(value: Int): String {
        return "%02d".format(value)
    }

    fun numberFormat(value: Double): String {
        return String.format("%.2f", value)
    }
}
