package com.lia.yello.linedineresturant.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.lia.yello.linedineresturant.R
import com.lia.yello.linedineresturant.Session.SharedHelper


class MainActivity : AppCompatActivity() {
    private val SPLASH_TIME_OUT: Long = 3000 // 1 sec
    var sharedHelper: SharedHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        sharedHelper = SharedHelper(this)
        sharedHelper?.location = ""
        sharedHelper?.selectedLat = "0.0"
        sharedHelper?.selectedLng = "0.0"
        sharedHelper?.ishistory = false

      /*  FlurryAgent.Builder()
            .withLogEnabled(true)
            .build(this, "G8MSXVTVCS87XJKGD67N")*/


        /*Handler().postDelayed({
            // This method will be executed once the timer is over
            // Start your app main activity

            startActivity(Intent(this, OnBoardActivity::class.java))

            // close this activity
            finish()
        }, SPLASH_TIME_OUT)*/
    }

    override fun onResume() {
        super.onResume()

        Handler(Looper.getMainLooper()).postDelayed({

            /* startActivity(Intent(this, GetStartedActivity::class.java))
             finish()*/
            startActivity(Intent(this, LoginActivity::class.java))
            finish()

        }, 2000)

    }
}