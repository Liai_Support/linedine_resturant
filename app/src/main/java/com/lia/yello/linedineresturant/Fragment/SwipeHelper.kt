package com.lia.yello.linedineresturant.Fragment
import android.content.Context
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.util.Log
import android.view.GestureDetector
import android.view.GestureDetector.SimpleOnGestureListener
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedineresturant.Model.MenuDetailJson
import com.lia.yello.linedineresturant.R
import java.util.*
abstract class SwipeHelper(context: Context?, recyclerView: RecyclerView, list: ArrayList<MenuDetailJson>?) : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
    private val recyclerView: RecyclerView
    public var list: ArrayList<MenuDetailJson>? = null
    private var buttons: MutableList<UnderlayButton>? = null
    private var gestureDetector: GestureDetector? = null
    private var swipedPos = -1
    private var swipeThreshold = 0.5f
    private val buttonsBuffer: MutableMap<Int, MutableList<UnderlayButton>?>
    private var recoverQueue: Queue<Int>? = null
    var context: Context? = null
    private val gestureListener: SimpleOnGestureListener = object : SimpleOnGestureListener() {
        override fun onSingleTapConfirmed(e: MotionEvent): Boolean {
            for (button in buttons!!) {
                if (button.onClick(e.x, e.y)) break
            }
            return true
        }
    }
    private val onTouchListener = OnTouchListener { view, e ->
        if (swipedPos < 0) return@OnTouchListener false
        val point =
            Point(e.rawX.toInt(), e.rawY.toInt())
        val swipedViewHolder =
            recyclerView.findViewHolderForAdapterPosition(swipedPos)
        if (swipedViewHolder != null) {
            val swipedItem = swipedViewHolder.itemView
            val rect = Rect()
            swipedItem.getGlobalVisibleRect(rect)
            if (e.action == MotionEvent.ACTION_DOWN || e.action == MotionEvent.ACTION_UP || e.action == MotionEvent.ACTION_MOVE) {
                if (rect.top < point.y && rect.bottom > point.y) gestureDetector!!.onTouchEvent(e) else {
                    recoverQueue?.add(swipedPos)
                    swipedPos = -1
                    recoverSwipedItem()
                }
            }
        }
        false
    }
    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return false
    }
    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val pos = viewHolder.adapterPosition
        Log.d("gfgc",""+viewHolder.adapterPosition+"h"+swipedPos+"ddf"+recoverQueue)
        if (swipedPos != pos) recoverQueue?.add(swipedPos)
        swipedPos = pos
        if (buttonsBuffer.containsKey(swipedPos)) buttons =
            buttonsBuffer[swipedPos] else buttons!!.clear()
        buttonsBuffer.clear()
        swipeThreshold = 0.5f * buttons!!.size * BUTTON_WIDTH
        recoverSwipedItem()
    }
    override fun getSwipeThreshold(viewHolder: RecyclerView.ViewHolder): Float {
        return swipeThreshold
    }
    override fun getSwipeEscapeVelocity(defaultValue: Float): Float {
        return 0.1f * defaultValue
    }
    override fun getSwipeVelocityThreshold(defaultValue: Float): Float {
        return 5.0f * defaultValue
    }
    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        val pos = viewHolder.adapterPosition
        var translationX = dX
        val itemView = viewHolder.itemView
        if (pos < 0) {
            swipedPos = pos
            return
        }
        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            if (dX < 0) {
                var buffer: MutableList<UnderlayButton>? =
                    ArrayList()
                if (!buttonsBuffer.containsKey(pos)) {
                    instantiateUnderlayButton(viewHolder, buffer as ArrayList<UnderlayButton>?)
                    buttonsBuffer[pos] = buffer
                } else {
                    buffer = buttonsBuffer[pos]
                }
                translationX =
                    dX * buffer!!.size * BUTTON_WIDTH / itemView.width
                drawButtons(c, itemView, buffer, pos, translationX)
            }
        }
        super.onChildDraw(
            c,
            recyclerView,
            viewHolder,
            translationX,
            dY,
            actionState,
            isCurrentlyActive
        )
    }
    @Synchronized
    private fun recoverSwipedItem() {
        while (recoverQueue?.isEmpty() == false) {
            val pos = recoverQueue!!.poll()
            if (pos > -1) {
                recyclerView.adapter!!.notifyItemChanged(pos)
            }
        }
    }
    private fun drawButtons(
        c: Canvas,
        itemView: View,
        buffer: List<UnderlayButton>?,
        pos: Int,
        dX: Float
    ) {
        var right = itemView.right.toFloat()
        val dButtonWidth = -1 * dX / buffer!!.size
        for (button in buffer) {
            val left = right - dButtonWidth
            button.onDraw(
                c,
                RectF(
                    left,
                    itemView.top + PADDING,
                    right,
                    itemView.bottom - PADDING
                ),
                pos
            )
            right = left
        }
    }
    fun attachSwipe() {
        val itemTouchHelper = ItemTouchHelper(this)
        itemTouchHelper.attachToRecyclerView(recyclerView)
    }
    abstract fun instantiateUnderlayButton(
        viewHolder: RecyclerView.ViewHolder?,
        underlayButtons: ArrayList<UnderlayButton>?
    )
    class UnderlayButton(
        var menuListFragment: MenuListFragment,
        private var list: ArrayList<MenuDetailJson>?,
        private val text: String,
        private val imageResId: Int,
        private val color: Int,
        private var clickListener: UnderlayButtonClickListener? = null
    ) {
        private var pos = 0
        private var clickRegion: RectF? = null
        fun onClick(x: Float, y: Float): Boolean {
            if (clickRegion != null && clickRegion!!.contains(x, y)) {
                clickListener?.onClick(pos)
                Log.d("scvsgd",""+pos)
                menuListFragment.deletecart(pos)
                return true
            }
            return false
        }
        fun onDraw(c: Canvas, rect: RectF, pos: Int) {



            //new added
            val p = Paint()

            // Draw background

            // Draw background
            p.color = color
            c.drawRect(rect, p)

            // Draw Text

            // Draw Text
            p.color = Color.WHITE
            p.textSize =20f

            val r = Rect()
            val cHeight = rect.height()
            val cWidth = rect.width()
            p.textAlign = Paint.Align.LEFT
            p.getTextBounds(text, 0, text.length, r)
            val x = cWidth / 2f - r.width() / 2f - r.left
            val y = cHeight / 2f + r.height() / 2f - r.bottom
            c.drawText(text, rect.left + x, rect.top + y, p)

            clickRegion = rect
            this.pos = pos



            /*  val p = Paint()
              val corners = floatArrayOf(0f, 0f, 20f, 20f, 20f, 20f, 0f, 0f)
              // Draw background
              p.color = color
              val path = Path()
              path.addRoundRect(rect, corners, Path.Direction.CW)
              c.drawPath(path, p)
              //            c.drawRoundRect(rect,corners, p);
              // Draw Text
              p.color = Color.WHITE
              p.textSize = TEXTSIZE
              val r = Rect()
              val cHeight = BUTTON_HEIGHT
              val cWidth = rect.width()
              p.textAlign = Paint.Align.LEFT
              p.getTextBounds(text, 0, text.length, r)
              val x = cWidth / 2f - r.width() / 2f - r.left
              val y = cHeight / 2f + r.height() / 2f - r.bottom
              //            c.drawText(text, rect.left + x, rect.top + y, p);
              val textWidth = p.measureText(text)
              val bounds = Rect()
              p.getTextBounds(text, 0, text.length, bounds)
              val combinedHeight =
                  bmp.height + SPACEHEIGHT + bounds.height()
              c.drawText(
                  text,
                  rect.centerX() - textWidth / 2,
                  rect.centerY() + combinedHeight / 2,
                  p
              )
              c.drawBitmap(
                  bmp,
                  rect.centerX() - bmp.width / 2,
                  rect.centerY() - combinedHeight / 2,
                  null
              )
              clickRegion = rect
              this.pos = pos*/
        }
    }
    public interface UnderlayButtonClickListener {
        fun onClick(pos: Int)
    }
    companion object {
        const val BUTTON_WIDTH = 150
        var TEXTSIZE = 0f
        var PADDING = 0f
        var SPACEHEIGHT = 0f
        var BUTTON_HEIGHT = 0f
        lateinit var bmp: Bitmap
    }
    init {
        /*  TEXTSIZE = Utils.convertDpToPixel(13, context)
          BUTTON_HEIGHT = Utils.convertDpToPixel(100, context)
          PADDING = Utils.convertDpToPixel(15, context)
          SPACEHEIGHT = Utils.convertDpToPixel(15, context)*/
        bmp = (ContextCompat.getDrawable(context!!, R.drawable.crossicon) as BitmapDrawable?)!!.bitmap
        this.recyclerView = recyclerView
        this.list = list
        buttons = ArrayList()
        gestureDetector = GestureDetector(context, gestureListener)
        this.recyclerView.setOnTouchListener(onTouchListener)
        buttonsBuffer = HashMap()
        /* recoverQueue = object : java.util.LinkedList<Int?>() {
             fun add(o: Int): Boolean {
                 return if (contains(o)) false else super.add(o)
             }
         }*/
        attachSwipe()
    }
}