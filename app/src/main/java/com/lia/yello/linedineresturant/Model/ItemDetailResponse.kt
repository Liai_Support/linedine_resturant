package com.lia.yello.linedineresturant.Model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ItemDetailResponse : Serializable {


    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: itemdetail? = null
}



class itemdetail : Serializable {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("menu_id")
    var menuid: Int? = null

    @SerializedName("res_id")
    var resid: Int? = null

    @SerializedName("calories")
    var calories: String? = null

    @SerializedName("item")
    var name: String? = null

    @SerializedName("description")
    var description :String? = null

    @SerializedName("price")
    var price: Double? = null

    @SerializedName("images")
    var images: String? = null

    @SerializedName("prepTime")
    var prepTime: String? = null

    @SerializedName("lat")
    var lat: Double? = null

    @SerializedName("lng")
    var lng: Double? = null
}
