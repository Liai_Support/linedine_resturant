package com.lia.yello.linedineresturant.activity

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.lia.yello.linedineresturant.Adapter.NewMenuListadapter
import com.lia.yello.linedineresturant.Model.MenuDetailJson
import com.lia.yello.linedineresturant.R
import com.lia.yello.linedineresturant.Session.SharedHelper
import com.lia.yello.linedineresturant.Utils.DialogUtils
import com.lia.yello.linedineresturant.Utils.UiUtils
import com.lia.yello.linedineresturant.ViewModel.MapViewModel
import kotlinx.android.synthetic.main.activity_add_new_menu_list.*
import kotlinx.android.synthetic.main.header.*
import org.json.JSONArray

class AddNewMenuListActivity : AppCompatActivity() {
    private var viewmodel: MapViewModel? = null
    var sharedHelper: SharedHelper? = null
    var lat:Double=0.0
    var lng:Double=0.0
    var isclosed = false
    var menuids2: MutableList<Int> = mutableListOf<Int>()
    var menuquantity2: MutableList<Int> = mutableListOf<Int>()
    var menulist:ArrayList<MenuDetailJson>? = ArrayList<MenuDetailJson>()

    val mapper = jacksonObjectMapper()
    var array: String=""
    lateinit var b:Bundle


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_new_menu_list)
        viewmodel = ViewModelProvider(this).get(MapViewModel::class.java)
        sharedHelper = SharedHelper(this)
      //  getvalues("All")

         /*if(intent.getSerializableExtra("menulist")!= null){
            menulist = intent.extras!!.getSerializable("menulist") as ArrayList<MenuDetailJson>

            Log.d("menulisttt", "" + menulist)
        }
*/

     /*   val extras = intent.extras
        if (extras != null) {
            menulist = intent.getSerializableExtra("menulist")  as ArrayList<MenuDetailJson>
        }*/

        b= intent.extras!!
        menulist= b.getSerializable("menulist")  as ArrayList<MenuDetailJson>
       // array= b.getString("menulist").toString()
       // Log.d("awedftyhnm", "" + array)




        bookmenu.setOnClickListener(View.OnClickListener {
            if (menulist!!.isEmpty() == false && menulist!!.size != 0) {
                val jsonArray = mapper.writeValueAsString(menulist)
                Log.d("chvb", "" + jsonArray)
                // getbookingtable(orderid!!,jsonArray )
            } else {

            }
        })
      //  val jsonArray = mapper.writeValueAsString(array)
      //  Log.d("chvb", "" + jsonArray)
        //menulist=array as ArrayList<MenuDetailJson>


    }


/*
    public fun getvalues(txt: String){
        DialogUtils.showLoader(this)
        viewmodel?.getMenuDetail(this, txt)
            ?.observe(this,
                Observer {

                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_subsubhome, msg)
                                    recycler.visibility = View.GONE

                                    //  Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()

                                }
                            } else {
                                recycler.visibility = View.VISIBLE
                                it.message?.let { msg ->
                                    // UiUtils.showSnack(root_subsubhome, msg)

                                    // Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
                                }

                                it.data?.let { data ->
                                    Log.d("asxdcfvgb", "" + data.restdetail)
                                    Log.d("asxdcfvgb", "" + data.data2)
                                    Log.d("asxdcfvgb", "" + data.menudetail)
                                    data.restdetail?.res_name?.let { menuname.text = it }

                                    rating.text = data.restdetail!!.rating + "/" + "5"


                                    //  data.restdetail?.rating?.let { rating.text = it }
                                    data.restdetail?.res_name?.let { head.text = it }
                                    data.restdetail?.description?.let { menu_description.text = it }
                                    if (data.data2!![0].closed.equals("0")) {
                                        recycler.visibility = View.VISIBLE
                                        if (data.data2!!.size != 0) {

                                            var time1: String? = data.data2!![0].openingTime
                                            var time2: String? = data.data2!![0].closingTime
                                            var time3: String? = " " + time1 + " - " + time2 + " "
                                            Log.d("sdfghjkl", "" + time3)
                                            work.visibility = View.VISIBLE
                                            time.text = time3
                                        } else {
                                            time.text = getString(R.string.invalidtime)
                                        }
                                    } else {
                                        work.visibility = View.GONE
                                        time.text = getString(R.string.closed)
                                        isclosed = true
                                        //recycler.visibility=View.GONE

                                    }

                                    lat = data.restdetail?.lat!!
                                    lng = data.restdetail?.lng!!
                                    data.restdetail?.bannerimg?.let {
                                        UiUtils.loadImage(
                                            bannerimg,
                                            it,
                                            ContextCompat.getDrawable(
                                                this,
                                                R.drawable.maskgroup46
                                            )!!
                                        )
                                    }

                                    recycler.layoutManager =
                                        GridLayoutManager(
                                            this,
                                            2
                                        )
                                    recycler.adapter = NewMenuListadapter(
                                        this, data.menudetail, isclosed, menuquantity2, menuids2
                                    )

                                }
                            }
                        }

                    }
                })
    }
*/
}