package com.lia.yello.linedineresturant.Fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.Observer

import androidx.recyclerview.widget.LinearLayoutManager
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.lia.yello.linedineresturant.Adapter.NewMenuListadapter
import com.lia.yello.linedineresturant.Model.MenuDetailJson
import com.lia.yello.linedineresturant.Model.MenuDetailJson2
import com.lia.yello.linedineresturant.Model.MenuDetailJsonFinal
import com.lia.yello.linedineresturant.Model.MenuDetails
import com.lia.yello.linedineresturant.R
import com.lia.yello.linedineresturant.Session.SharedHelper
import com.lia.yello.linedineresturant.Utils.DialogUtils
import com.lia.yello.linedineresturant.Utils.UiUtils
import com.lia.yello.linedineresturant.ViewModel.MapViewModel
import com.lia.yello.linedineresturant.activity.DashBoardActivity
import com.lia.yello.linedineresturant.databinding.FragmentMenuOrderBinding
import kotlinx.android.synthetic.main.fragment_menu_order.*
import kotlinx.android.synthetic.main.headerhome.*
import kotlin.collections.ArrayList

class MenuOrderFragment : Fragment(R.layout.fragment_menu_order) {

    private var viewmodel: MapViewModel? = null
    var sharedHelper: SharedHelper? = null
    var resid: Int = 0
    var paymenttype: String = ""
    var booked: String = ""
    var doubleBackToExitPressedOnce: Boolean = false
  var binding: FragmentMenuOrderBinding? = null;
    var value: Int = 0
    var bookingid: Int = 0
    var jsonArrayy:String=""
    var finaljsonArrayy:String=""

    var list: ArrayList<MenuDetails>? = null
    val mapper = jacksonObjectMapper()
    var menulist:ArrayList<MenuDetailJson>? = ArrayList<MenuDetailJson>()
    var menulist2:ArrayList<MenuDetailJson2>? = ArrayList<MenuDetailJson2>()
    var menulist3:ArrayList<MenuDetailJsonFinal>? = ArrayList<MenuDetailJsonFinal>()
    private var menuListFragment: MenuListFragment? = null
    private lateinit var fragmentTransaction: FragmentTransaction
    var frombooking:String=""
    lateinit var head:TextView
    var menuids2: MutableList<Int> = mutableListOf<Int>()
    var menuquantity2: MutableList<Int> = mutableListOf<Int>()
    private var homeFragment: HomeFragment? = null
    var isclosed = false
    var jsonArray:String=""
    lateinit var logoutt:ImageView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
      binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())
        viewmodel = ViewModelProvider(this).get(MapViewModel::class.java)
        val head = view.findViewById<TextView>(R.id.head)
        logoutt = view.findViewById<ImageView>(R.id.logout1)

        head.text = getString(R.string.menu)
        menulist!!.clear()

        //menulist = arguments?.getSerializable("menulist") as ArrayList<MenuDetailJson>
        bookingid=arguments!!.getInt("bookingid")

        value = arguments!!.getInt("tableid")
        Log.d("asdfghjk",""+bookingid)
        Log.d("menyulisst",""+menulist)
        getvalue(sharedHelper!!.resid)

        if (sharedHelper!!.language == "ar"){
            logout1.rotation= 180F

        }
        language.setOnClickListener{
            if (sharedHelper!!.language.equals("ar")){
                sharedHelper?.language = "en"
                reloadApp()
            }else if (sharedHelper!!.language.equals("en")){
                sharedHelper?.language = "ar"
                reloadApp()
            }
        }
        if (sharedHelper!!.language.equals("ar")) {

            language.text = "EN"
        }else{
            language.text = "AR"

        }
        logoutt.setOnClickListener(View.OnClickListener {

            //requireActivity().supportFragmentManager.popBackStack()
            val bundle = Bundle()
            bundle.putInt("bookingid",bookingid)
            bundle.putInt("tableid",value)
            bundle.putString("booked","true")
            bundle.putString("resimg","")
            menuListFragment = MenuListFragment()
            menuListFragment!!.arguments = bundle
            fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, menuListFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()

        })



        done.setOnClickListener{


            for (i in 0 until menulist2!!.size){
                if (menulist2!![i].quantity!=0){

                    menulist3!!.add(
                        MenuDetailJsonFinal(
                            menulist2!![i].menuid,
                            menulist2!![i].quantity,
                            menulist2!![i].cost
                        )
                    )


                    finaljsonArrayy=
                        mapper.writeValueAsString(menulist3)
                    Log.d("mnhbvgcx ", "" +finaljsonArrayy.length)







                 }else{

                }



            }
            Log.d("mnhbvgcx ", "" +finaljsonArrayy.length)


            if (finaljsonArrayy.length!=0){
                getbookingmenu(bookingid,
                    finaljsonArrayy
                )

            }else{
                UiUtils.showSnack(root_ordersfragment, getString(R.string.pleaseselectthemenu))
            }






        }



    }



    public fun getvalues(txt: String){
        DialogUtils.showLoader(requireContext())
        viewmodel?.getMenuDetail(requireContext(), txt)
            ?.observe(requireActivity(),
                Observer {

                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_ordersfragment, msg)
                                    menulistrecycler.visibility = View.GONE

                                    //  Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()

                                }
                            } else {
                                menulistrecycler.visibility = View.VISIBLE
                                it.message?.let { msg ->
                                    // UiUtils.showSnack(root_subsubhome, msg)

                                    // Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
                                }

                                it.data?.let { data ->

                                 /*   menulistrecycler.layoutManager =
                                        GridLayoutManager(
                                            requireContext(),
                                            2
                                        )*/


                                    menulistrecycler.layoutManager =
                                        LinearLayoutManager(requireContext())
                                    menulistrecycler.adapter = NewMenuListadapter(this,
                                        requireContext(), data.menudetail, isclosed, menuquantity2, menuids2
                                    )


                                }
                            }
                        }

                    }
                })
    }


    public fun getbookingmenu(orderid: Int, jsonArray: String) {
        DialogUtils.showLoader(requireContext())
        viewmodel?.gettablebookingmenu(requireContext(), orderid, jsonArray)
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->

                                UiUtils.showSnack(root_ordersfragment, msg)
                            }
                        } else {
                            it.message?.let { msg ->
                                UiUtils.showSnack(root_ordersfragment, msg)

                             //   getvalue(sharedHelper!!.resid)
                                val bundle = Bundle()
                                bundle.putInt("bookingid",bookingid)
                                bundle.putInt("tableid",value)
                                bundle.putString("booked","true")

                                Log.d("asdfrgthyjukilo;",""+bookingid)
                                menuListFragment = MenuListFragment()
                                menuListFragment!!.arguments = bundle
                                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                                fragmentTransaction.setCustomAnimations(R.anim.screen_in,R.anim.screen_out)
                                fragmentTransaction.replace(R.id.container, menuListFragment!!)
                                fragmentTransaction.addToBackStack(null)
                                fragmentTransaction.commit()


                            }
                        }

                    }
                }
            })


    }






    fun getvalue(resid: Int){
        DialogUtils.showLoader(requireContext())
        viewmodel?.gettabledetails(requireContext(),resid)
            ?.observe(requireActivity(),
                Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_ordersfragment, msg)
                                }
                            }
                            else {
                                it.data?.let {data ->
                                    for (i in 0 until data.size){
                                        if (value==data[i].tableidd){
                                            Log.d("qwserftgghyj",""+data[i].bookinglist!!.size)
                                            Log.d("vattttt",""+sharedHelper!!.vat)
                                            sharedHelper!!.vat = data[i]!!.VAT!!.toFloat()
                                            menulist!!.clear()

                                            for (k in 0 until data[i].bookinglist!!.size){
                                                if (bookingid==data[i].bookinglist!![k].bookingid){

                                                       if (data[i].bookinglist!![k].menulistdetails!!.size!=0){
                                                           for (a in 0 until data[i].bookinglist!![k].menulistdetails!!.size) {


                                                               menulist!!.add(
                                                                   MenuDetailJson(
                                                                       data[i].bookinglist!![k].menulistdetails!![a].menuid!!,
                                                                       data[i].bookinglist!![k].menulistdetails!![a].quantity!!,
                                                                       data[i].bookinglist!![k].menulistdetails!![a].menucost!!
                                                                   )
                                                               )

                                                               val jsonArray =
                                                                   mapper.writeValueAsString(menulist)
                                                               Log.d("jsonArrayghb ", "" + jsonArray)


                                                               Log.d(
                                                                   "menudetailks",
                                                                   "" + data[i].bookinglist!![k].menulistdetails!![a].id
                                                               )

                                                           }

                                                       }












                                                }


                                            }
                                            getvalues("All")



                                        }






                                    }



                                }


                            }
                        }

                    }

                })
    }
    private fun reloadApp() {



        val intent = Intent(requireContext(), DashBoardActivity::class.java)

        intent.putExtra("langfrom", "Menuorderfrag")
        intent.putExtra("tableid", value)
        intent.putExtra("bookingid", bookingid)
        startActivity(intent)



    }


}