package com.lia.yello.linedineresturant.Model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class LoginResponseModel : Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: LoginData? = null


}


class LoginData : Serializable {

    @SerializedName("user_id")
    var user_id: Int? = null

    @SerializedName("user_name")
    var user_name: String? = null

    @SerializedName("user_mail")
    var user_mail: String? = null



    @SerializedName("user_mobile")
    var user_mobile: String? = null


    @SerializedName("resid")
    var resid: Int? = null


    @SerializedName("accessToken")
    var token: String? = null



    @SerializedName("status")
    var status: String? = null


}