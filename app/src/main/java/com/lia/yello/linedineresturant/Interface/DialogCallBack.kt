package com.lia.yello.linedineresturant.Interface

interface DialogCallBack {
    fun onPositiveClick()
    fun onNegativeClick()
}