package com.lia.yello.linedineresturant.Interface

interface SingleTapListener {
    fun singleTap()
}