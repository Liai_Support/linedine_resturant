package com.lia.yello.linedineresturant.activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.IntentFilter
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.provider.SyncStateContract
import androidx.annotation.RequiresApi
import androidx.multidex.MultiDexApplication
//import com.amazonaws.mobileconnectors.s3.transferutility.TransferNetworkLossHandler
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

import com.lia.yello.linedineresturant.Utils.Constants


class AppController : MultiDexApplication() {


    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        instance = this
    }

    companion object {

        private val TAG: String = AppController::class.java.simpleName
        private var instance: AppController? = null
        private var requestQueue: RequestQueue? = null
       // private var mFirebaseAnalytics: FirebaseAnalytics? = null
        private val callTone =
            RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION).toString()

        @Synchronized
        fun getInstance(): AppController {
            return instance as AppController
        }

    }


    private fun getRequestQueue(): RequestQueue? {

        if (requestQueue == null)
            requestQueue = Volley.newRequestQueue(instance)

        return requestQueue
    }

    fun <T> addrequestToQueue(request: Request<T>) {
        request.tag = TAG
        getRequestQueue()?.add(request)
    }

    override fun onCreate() {
        super.onCreate()

    //    mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
      //  mFirebaseAnalytics?.setAnalyticsCollectionEnabled(true)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel()
            createNotificationChannelCall()
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel() {
/*
        registerReceiver(
            TransferNetworkLossHandler.getInstance(applicationContext), IntentFilter(
                ConnectivityManager.CONNECTIVITY_ACTION
            )
        )*/

        val attributesCall = AudioAttributes.Builder()
            .setUsage(AudioAttributes.USAGE_NOTIFICATION)
            .build()


        val notificationManager =
            instance?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val incomingCall =
            NotificationChannel("Alert", "Alert", NotificationManager.IMPORTANCE_HIGH)

        val message =
            NotificationChannel("message", "message", NotificationManager.IMPORTANCE_HIGH)

        incomingCall.setSound(Uri.parse(callTone), attributesCall)
        message.setSound(Uri.parse(callTone), attributesCall)
        notificationManager.createNotificationChannel(incomingCall)
        notificationManager.createNotificationChannel(message)

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannelCall() {

        val attributesCall = AudioAttributes.Builder()
            .setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
            .build()

        val notificationManager =
            instance?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val incomingCall = NotificationChannel(
            Constants.NotificationIds.INCOMING_CALL_CHANNEL_ID,
            Constants.NotificationIds.INCOMING_CALL_CHANNEL_NAME,
            NotificationManager.IMPORTANCE_HIGH
        )
        /*val chatGroup = NotificationChannel(
            SyncStateContract.Constants.NotificationIds.HANGUP_CALL_CHANNEL_ID,
            SyncStateContract.Constants.NotificationIds.HANGUP_CALL_CHANNEL_NAME,
            NotificationManager.IMPORTANCE_HIGH
        )*/
/*

        incomingCall.setSound(Uri.parse(callTone), attributesCall)
        chatGroup.setSound(null, null)

        notificationManager.createNotificationChannel(incomingCall)
        notificationManager.createNotificationChannel(chatGroup)
*/

    }

}
