package com.lia.yello.linedineresturant.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.lia.yello.linedineresturant.Adapter.NewMenuListadapter
import com.lia.yello.linedineresturant.Model.MenuDetailJson
import com.lia.yello.linedineresturant.Model.itemdetail
import com.lia.yello.linedineresturant.R
import com.lia.yello.linedineresturant.Session.SharedHelper
import com.lia.yello.linedineresturant.Utils.DialogUtils
import com.lia.yello.linedineresturant.Utils.UiUtils
import com.lia.yello.linedineresturant.ViewModel.MapViewModel
import kotlinx.android.synthetic.main.activity_add_new_menu_list.*
import kotlinx.android.synthetic.main.activity_add_new_menu_list.view
import kotlinx.android.synthetic.main.activity_item_detail.*
import kotlinx.android.synthetic.main.card_menulist.*
import kotlinx.android.synthetic.main.header.*
import java.io.Serializable

class ItemDetailActivity : AppCompatActivity() {
    private var viewmodel: MapViewModel? = null
    var sharedHelper: SharedHelper? = null
    var menulist:ArrayList<MenuDetailJson>? = ArrayList<MenuDetailJson>()
    val mapper = jacksonObjectMapper()
    var menuid1:Int=0
    var menurating:Double=0.0
    var inarray:Boolean=false
    var count:Int=0
    var subtotal: Double = 0.0
    var vat:Double=0.0
    var lat:Double=0.0
    var lng:Double=0.0
    var cost:Double=0.0
    public var menuid:Int? = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_detail)
        viewmodel = ViewModelProvider(this).get(MapViewModel::class.java)
        sharedHelper = SharedHelper(this)
        menuid1 = intent.extras!!.getInt("id")
        menurating =intent.extras!!.getDouble("menurating")!!
        inarray = intent.extras!!.getBoolean("inarray")!!
        menulist = intent.extras!!.getSerializable("menulist") as ArrayList<MenuDetailJson>
        getvalues(menuid1)

        backtomenu.setOnClickListener {
            val intent = Intent(this, AddNewMenuListActivity::class.java)

            val bundle = Bundle()
            bundle.putSerializable("menulist",menulist as Serializable)
            intent.putExtras(bundle)
            Log.d("serftgh",""+menulist)
            startActivity(intent)
        }


        plus.setOnClickListener(View.OnClickListener {
            count++
            txtcount.setText("" + count)
            Log.d("count", "" + count)
        })
        minus.setOnClickListener(View.OnClickListener {
            if (count == 0 || count == 1) {
                txtcount.setText("" + count)
                //  UiUtils.showSnack(root_finedineitemdetail,"itemcannotbelessthan0")
            } else {
                count--
                txtcount.setText("" + count)
            }
        })
   /*     additem.setOnClickListener(View.OnClickListener {
            if (inarray == true){
                for (i in 0 until menulist!!.size){
                    if(menulist!![i].menuid == menuid1){
                        menulist!!.set(i,MenuDetailJson(menuid1!!,count))
                    }
                }
                UiUtils.showSnack(root_subsubsubhome,getString(R.string.menuisalreadyadded))
            }
            else{
                if (count==0){
                    UiUtils.showSnack(root_subsubsubhome,getString(R.string.productquantityis0))
                }
                else {
                    inarray = true
                    menulist!!.add(MenuDetailJson(menuid1!!, count))
                    val jsonArray = mapper.writeValueAsString(menulist)
                    Log.d("ghb ", "" + jsonArray)
                }
            }
        })*/


    }
    public fun getvalues(id:Int){
        DialogUtils.showLoader(this)
        viewmodel?.getItemDetail(this, id)
            ?.observe(this,
                Observer {

                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_subsubhome, msg)


                                }
                            } else {  it.data?.let { data ->
                                handleResponse(data)
                            }
                            }
                        }

                    }
                })
    }



    fun handleResponse(data: itemdetail) {
        data.name?.let { name.text = it }
        data.description?.let { itemdescription.text = it }
        data.price?.let { total.text = it.toString() + " SAR" }
        data.prepTime?.let { preparetime.text = it }
        data.description?.let { itemdescription.text = it }
        menuid = data.menuid
        cost = data.price!!
        lat = data.lat!!
        lng = data.lng!!
        data.images?.let {
            UiUtils.loadImage(
                itemimg,
                it,
                ContextCompat.getDrawable(this, R.drawable.maskgroup46)!!
            )
        }
    }
    fun snackbarToast(msg: String) {
        var snackbar: Snackbar
        Snackbar.make(view!!, msg, Snackbar.LENGTH_SHORT)
            .setTextColor(ContextCompat.getColor(this, R.color.design_default_color_error))
            .setBackgroundTint(ContextCompat.getColor(this, R.color.white))
            .setDuration(BaseTransientBottomBar.LENGTH_LONG)
            .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
            .show()
    }

}