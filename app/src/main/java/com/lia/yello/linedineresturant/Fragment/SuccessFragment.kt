package com.lia.yello.linedineresturant.Fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import com.lia.yello.linedineresturant.R
import com.lia.yello.linedineresturant.Session.SharedHelper
import com.lia.yello.linedineresturant.Utils.DialogUtils
import com.lia.yello.linedineresturant.Utils.UiUtils
import com.lia.yello.linedineresturant.ViewModel.MapViewModel
import kotlinx.android.synthetic.main.fragment_invoice.*
import androidx.lifecycle.Observer
import com.lia.yello.linedineresturant.activity.DashBoardActivity
import com.lia.yello.linedineresturant.databinding.FragmentSuccessBinding
import kotlinx.android.synthetic.main.fragment_invoice.root_invoice
import kotlinx.android.synthetic.main.fragment_success.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.header.language2
import kotlinx.android.synthetic.main.headerhome.*


class SuccessFragment : Fragment(R.layout.fragment_success) {


    var sharedHelper: SharedHelper? = null
    private var viewmodel: MapViewModel? = null

    private var homeFragment: HomeFragment? = null
    var bookingid:Int=0
    var value:Int=0
    var paymenttype:String=""
    lateinit var logoutt: ImageView
   var binding:FragmentSuccessBinding?=null
    private lateinit var fragmentTransaction: FragmentTransaction
    lateinit var closeebtn:Button
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
      binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())
        viewmodel = ViewModelProvider(this).get(MapViewModel::class.java)
        closeebtn = view.findViewById<Button>(R.id.closeebtn)
        logoutt = view.findViewById<ImageView>(R.id.logout1)

/*        value = arguments!!.getInt("tableid")
        bookingid=arguments!!.getInt("bookingid")
        paymenttype= arguments!!.getString("paymenttype")!!*/
        closeebtn.setOnClickListener(View.OnClickListener {

            Log.d("zxsdcfvgbnm",""+bookingid)
            Log.d("zxsdcfvgbnm",""+value)

        })
        if (sharedHelper!!.language.equals("ar")) {

            language.text = "EN"
        }else{
            language.text = "AR"

        }

        if (sharedHelper!!.language == "ar"){
            logout1.rotation= 180F

        }
        language.setOnClickListener{
            if (sharedHelper!!.language.equals("ar")){
                sharedHelper?.language = "en"
                reloadApp()
            }else if (sharedHelper!!.language.equals("en")){
                sharedHelper?.language = "ar"
                reloadApp()
            }
        }
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {

                homeFragment = HomeFragment()
                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, homeFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()



            }
        })


        logoutt.setOnClickListener(View.OnClickListener {

            homeFragment = HomeFragment()
            fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, homeFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()

        })

    }

    private fun reloadApp() {



        val intent = Intent(requireContext(), DashBoardActivity::class.java)

        intent.putExtra("langfrom", "Successfrag")
        intent.putExtra("tableid", value)
        intent.putExtra("bookingid", bookingid)
        intent.putExtra("paymenttype", paymenttype)

        startActivity(intent)



    }


}