package com.lia.yello.linedineresturant.Model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class UpdateTableStatusResponse: Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null


}