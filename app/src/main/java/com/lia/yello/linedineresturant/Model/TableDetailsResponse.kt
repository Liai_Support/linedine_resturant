package com.lia.yello.linedineresturant.Model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class TableDetailsResponse : Serializable {


    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: ArrayList<TableDetail>? = null
}

class TableDetail : Serializable {

    @SerializedName("tableid")
    var tableidd: Int? = null

    @SerializedName("name")
    var tablename: String? = null


    @SerializedName("seats")
    var seats: Int? = null

    @SerializedName("type")
    var type: String? = null

    @SerializedName("resid")
    var resid: Int? = null


    @SerializedName("VAT")
    var VAT: Double? = null

    @SerializedName("bookingdetails")
    var bookinglist: ArrayList<BookingListData>? = null
}
class BookingListData : Serializable {

    @SerializedName("bookingid")
    var bookingid: Int? = null


    @SerializedName("customername")
    var customername: String? = null

    @SerializedName("customerphone")
    var customerphone: String? = null


    @SerializedName("date")
    var date: String? = null

    @SerializedName("timeslots")
    var timeslots: String? = null

    @SerializedName("bookingstatus")
    var bookingstatus: String? = null

    @SerializedName("booked_seats")
    var booked_seats: Int? = null

    @SerializedName("initial_amount")
    var initial_amount: Double? = null

    @SerializedName("paidstatus")
    var paidstatus: String? = null

    @SerializedName("discount_name")
    var discount_name: String? = null

    @SerializedName("discount_amount")
    var discount_amount: Double? = null

    @SerializedName("menudetails")
    var menulistdetails: ArrayList<MenuDetails>? = null


}


class MenuDetails : Serializable {

    @SerializedName("id")
    var id: Int? = null


    @SerializedName("menuid")
    var menuid: Int? = null


    @SerializedName("status")
    var status: String? = null


    @SerializedName("menuname")
    var menuname: String? = null


    @SerializedName("quantity")
    var quantity: Int? = null


    @SerializedName("menucost")
    var menucost: Double? = null

    @SerializedName("images")
    var images: String? = null
}
