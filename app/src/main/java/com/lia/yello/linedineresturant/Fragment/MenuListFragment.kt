package com.lia.yello.linedineresturant.Fragment

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import androidx.fragment.app.Fragment
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.lia.yello.linedineresturant.Adapter.MenuListAdapter
import com.lia.yello.linedineresturant.Model.MenuDetailJson
import com.lia.yello.linedineresturant.Model.MenuDetails
import com.lia.yello.linedineresturant.R
import com.lia.yello.linedineresturant.Session.SharedHelper
import com.lia.yello.linedineresturant.Utils.DialogUtils
import com.lia.yello.linedineresturant.Utils.UiUtils
import com.lia.yello.linedineresturant.ViewModel.MapViewModel
import com.lia.yello.linedineresturant.activity.DashBoardActivity
import com.lia.yello.linedineresturant.databinding.FragmentMenuListBinding
import kotlinx.android.synthetic.main.activity_add_new_menu_list.*
import kotlinx.android.synthetic.main.activity_dash_board.*
import kotlinx.android.synthetic.main.activity_menu_list.*
import kotlinx.android.synthetic.main.activity_menu_list.addnewmenu
import kotlinx.android.synthetic.main.activity_menu_list.ordersubtotal
import kotlinx.android.synthetic.main.activity_menu_list.placeorder
import kotlinx.android.synthetic.main.activity_menu_list.recyclermenulist
import kotlinx.android.synthetic.main.activity_menu_list.root_suborder
import kotlinx.android.synthetic.main.activity_menu_list.vat_txt
import kotlinx.android.synthetic.main.fragment_menu_list.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.headerhome.*
import kotlinx.android.synthetic.main.pop_up_movetopay.view.*
import kotlinx.android.synthetic.main.popup_password.view.*
import java.io.Serializable
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*
import kotlin.collections.ArrayList


class MenuListFragment  : Fragment(R.layout.fragment_menu_list){
    private var viewmodel: MapViewModel? = null
    var sharedHelper: SharedHelper? = null
    var resid: Int = 0
    var paymenttype: String = ""
    var booked: String = ""
    var doubleBackToExitPressedOnce: Boolean = false
    var binding: FragmentMenuListBinding? = null;
    var value: Int = 0
    var bookingid: Int = 0
    var list: ArrayList<MenuDetails>? = null
    var menulist:ArrayList<MenuDetailJson>? = ArrayList<MenuDetailJson>()
    val mapper = jacksonObjectMapper()
/*    lateinit var addnewmenu:TextView
    lateinit var table_list: RecyclerView
    lateinit var resname: TextView
    lateinit var res_description: TextView
    lateinit var prodcuttotal: TextView
    lateinit var bannerimg: ImageView
    lateinit var cancel: LinearLayout
    lateinit var rrproducttotal: RelativeLayout
    lateinit var recyclermenulist: RecyclerView*/
    private lateinit var fragmentTransaction: FragmentTransaction
    private var addNewMenuListFragment: AddNewMenuListFragment? = null
    private var invoiceFragment: InvoiceFragment? = null
    private var menuListFragment: MenuListFragment? = null
    private var menuOrderFragment: MenuOrderFragment? = null
    private var bookingListFragment: BookingListFragment? = null
    var subtotal: Double = 0.0
    var finalsubtotal: Double = 0.0
    var totalamt:Double=0.0
    var vat:Double=0.0


  /*  lateinit var ordersubtotal:TextView
    lateinit var vat_txt:TextView
    lateinit var Bookimgamount:TextView
    lateinit var placeorder:TextView
    lateinit var totalamtt:TextView
    lateinit var txtnomenubooked:TextView
    lateinit var nomenuyet:TextView
    lateinit var discountamt:TextView
    lateinit var card:Button
    lateinit var cash:Button
    lateinit var relative_suborder:RelativeLayout
    lateinit var discountlayout:RelativeLayout
    lateinit var pop_up_movetopay:LinearLayout
    lateinit var bot:LinearLayout*/
    var popupopen:String="true"
    var resimage:String=""
    var frombooking:String=""
    var statuspaid:String=""
    var tablename:String=""
    var delete:String="true"
    var initial_amtt:Double=0.0
    var dicountmmt:Double=0.0
    var vatfinal:Double=0.0
    var ismenuadd:Boolean=false
    lateinit var logoutt:ImageView



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
         sharedHelper = SharedHelper(requireContext())
         viewmodel = ViewModelProvider(this).get(MapViewModel::class.java)
         val head = view.findViewById<TextView>(R.id.head)
        logoutt = view.findViewById<ImageView>(R.id.logout1)

        head.text=getString(R.string.order)
        language.setOnClickListener{
            if (sharedHelper!!.language.equals("ar")){
                sharedHelper?.language = "en"
                reloadApp()
            }else if (sharedHelper!!.language.equals("en")){
                sharedHelper?.language = "ar"
                reloadApp()
            }
        }
/*         addnewmenu = view.findViewById<TextView>(R.id.addnewmenu)
         ordersubtotal = view.findViewById<TextView>(R.id.ordersubtotal)
         vat_txt = view.findViewById<TextView>(R.id.vat_txt)
         totalamtt = view.findViewById<TextView>(R.id.totalamtt)
         Bookimgamount = view.findViewById<TextView>(R.id.amountdeducted)
         discountamt = view.findViewById<TextView>(R.id.discountamt)
         placeorder = view.findViewById<TextView>(R.id.placeorder)
       //  txtnomenubooked = view.findViewById<TextView>(R.id.txtnomenubooked)
        nomenuyet = view.findViewById<TextView>(R.id.nomenuyet)
        prodcuttotal = view.findViewById<TextView>(R.id.prodcuttotal)
        logoutt = view.findViewById<ImageView>(R.id.logout1)

      // cancel = view.findViewById<LinearLayout>(R.id.cancel)
//         relative_suborder = view.findViewById<RelativeLayout>(R.id.relative_suborder)
        discountlayout = view.findViewById<RelativeLayout>(R.id.discountlayout)
        rrproducttotal = view.findViewById<RelativeLayout>(R.id.rrproducttotal)
        // pop_up_movetopay = view.findViewById<LinearLayout>(R.id.pop_up_movetopay)
     //    bot = view.findViewById<LinearLayout>(R.id.bot)
    *//*     cash = view.findViewById<Button>(R.id.cash)
         card = view.findViewById<Button>(R.id.card)*//*
         recyclermenulist = view.findViewById<RecyclerView>(R.id.recyclermenulist)*/
         value = arguments!!.getInt("tableid")
         bookingid = arguments!!.getInt("bookingid")
         booked = arguments!!.getString("booked")!!
       //  resimage= arguments!!.getString("resimg")!!
//        relative_suborder.alpha=1.0f

        Log.d("qawertgyhujik",""+bookingid)
         Log.d("wesxdfvgbn",""+booked)
         getintialvalues()
         getvalue(sharedHelper!!.resid)
        if (sharedHelper!!.language == "ar"){
            logout1.rotation= 180F

        }
        if (sharedHelper!!.language.equals("ar")) {

            language.text = "EN"
        }else{
            language.text = "AR"

        }
      logoutt.setOnClickListener(View.OnClickListener {
          //  requireActivity().supportFragmentManager.popBackStack()

             val bundle = Bundle()
             bundle.putInt("tableid",value)
             bundle.putString("resimg",resimage)
             bookingListFragment = BookingListFragment()
             bookingListFragment!!.arguments = bundle
             fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
             fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
             fragmentTransaction.replace(R.id.container, bookingListFragment!!)
             fragmentTransaction.addToBackStack(null)
             fragmentTransaction.commit()

        })
        if (popupopen.equals("true")){
            popupopen="false"
        }

        if (booked.equals("false")){
            placeorder.visibility=View.GONE
            addnewmenu.visibility=View.GONE
        }else{
//            cancel.visibility=View.VISIBLE

        }

            addnewmenu.setOnClickListener(View.OnClickListener {

                if (statuspaid.equals("paid")){
                    UiUtils.showSnack(root_suborder,getString(R.string.youcantmenuasyoualreadlypaid))
                }else{
                    val bundle = Bundle()
                    bundle.putInt("bookingid",bookingid)
                    bundle.putInt("tableid",value)
                    //bundle.putSerializable("menulist", menulist as Serializable)
                   // bundle.putString("frombooking","")
                    //bundle.putString("resimg",resimage)
                    menuOrderFragment = MenuOrderFragment()
                    menuOrderFragment!!.arguments = bundle
                    fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                    fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                    fragmentTransaction.replace(R.id.container, menuOrderFragment!!)
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
                }



         /* val intent = Intent(this, AddNewMenuListActivity::class.java)
            val bundle = Bundle()
            bundle.putSerializable("menulist", menulist as Serializable)
            //  bundle.putString("menulist", menulist.toString())
            intent.putExtras(bundle)
            Log.d("serftgh",""+menulist)
            // Log.d("serftgh",""+menulist)
            // intent.putExtra("menulist",menulist as Serializable)
            startActivity(intent)*/
        })

          placeorder.setOnClickListener(View.OnClickListener {
                 delete="false"

              if (ismenuadd==true){
                  UiUtils.showSnack(root_suborder, getString(R.string.pleaseaddthemenufirst))

              }else{
                  if (statuspaid.equals("paid")){
                      //    pop_up_movetopay.visibility=View.VISIBLE
                      // relative_suborder.alpha=0.5f
                      //placeorder.isEnabled=false
                      //addnewmenu.isEnabled=false
                      showDialog4()

                  }else{
                      popupopen="true"
                      val jsonArray = mapper.writeValueAsString(menulist)
                      getbookingmenu(bookingid,jsonArray)
                      // pop_up_movetopay.visibility=View.VISIBLE
                      showDialog4()
                      //    relative_suborder.alpha=0.5f
                      //root_suborder.isEnabled=false
                      //  placeorder.isEnabled=false
                      //addnewmenu.isEnabled=false

                  }
              }



        })


        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {



                val bundle = Bundle()
                bundle.putInt("tableid",value)
                bundle.putString("resimg",resimage)
                bookingListFragment = BookingListFragment()
                bookingListFragment!!.arguments = bundle
                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, bookingListFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()



            }
        })

}

    private fun getvalue(resid: Int){
        DialogUtils.showLoader(requireContext())
        viewmodel?.gettabledetails(requireContext(),resid)
            ?.observe(requireActivity(),
                Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_suborder, msg)
                                }
                            }
                            else {
                                it.data?.let {data ->
                                    for (i in 0 until data.size){
                                        if (value==data[i].tableidd){
                                            Log.d("qwserftgghyj",""+data[i].bookinglist!!.size)
                                            Log.d("vattttt",""+sharedHelper!!.vat)
                                            sharedHelper!!.vat = data[i]!!.VAT!!.toFloat()

                                            for (k in 0 until data[i].bookinglist!!.size){
                                                if (bookingid==data[i].bookinglist!![k].bookingid){
                                                    statuspaid= data[i].bookinglist!![k].paidstatus!!
                                                    Log.d("AZsxdcfvgbhnjmk,.",""+statuspaid)



                                                    Log.d("aertyuioiuhgfdcxcv","i  and k "+i +" and "+k+" value ="+data[i].bookinglist!![k].menulistdetails!!.size)
                                                    if(data[i].bookinglist!![k].menulistdetails!!.size == 0){
                                                        addnewmenu.isEnabled=true
                                                        frombooking="frombooking"
                                                        recyclermenulist.visibility=View.GONE
                                                       nomenuyet.visibility=View.VISIBLE
                                                      //  placeorder.isClickable=false
                                                        ismenuadd=true


                                                        /*     ordersubtotal.text =
                                                                subtotal.toString()+" SAR"
                                                             totalamtt.text =
                                                              totalamt.toString()+" SAR"
                                                             vat_txt.text =
                                                               vat.toString()+" SAR"*/

                                                        ordersubtotal.text=  String.format(Locale("en", "US"),"%.2f",subtotal)+" SAR"
                                                        totalamtt.text =  String.format(Locale("en", "US"),"%.2f",totalamt)+" SAR"
                                                        vat_txt.text = String.format(Locale("en", "US"),"%.2f",vat)+" SAR"


                                                        //    Bookimgamount.setText("" + String.format("%.2f", 50)+" SAR")

                                                       // Bookimgamount.text = "-"+  data[i].bookinglist!![k].initial_amount+" SAR"

                                                        if (data[i].bookinglist!![k].initial_amount.toString().equals("0")){
                                                            bookingamtrtelat.visibility=View.GONE
                                                        }else{
                                                            bookingamtrtelat.visibility=View.VISIBLE

                                                        }
                                                        amountdeducted.text = "-"+  String.format(Locale("en", "US"),"%.2f",data[i].bookinglist!![k].initial_amount)+" SAR"
                                                        initial_amtt =   String.format(Locale("en", "US"),"%.2f",data[i].bookinglist!![k].initial_amount).toDouble()



                                                    }
                                                    else {
                                                        frombooking = ""
                                                        recyclermenulist.visibility = View.VISIBLE
                                                        nomenuyet.visibility = View.GONE
                                                        placeorder.isEnabled = true
                                                        addnewmenu.isEnabled = true

                                                        subtotal = 0.0
                                                        vat = 0.0
                                                        totalamt = 0.0
                                                        menulist!!.clear()
                                                        for (a in 0 until data[i].bookinglist!![k].menulistdetails!!.size) {

                                                            //    if(bookingid==data[i].bookinglist!![k].bookingid){
                                                            Log.d("asdfghjkl", "" + bookingid)
                                                            Log.d(
                                                                "wertyuiop",
                                                                "" + data[i].bookinglist!![k].bookingid
                                                            )

                                                            //calculating subtotal
                                                            subtotal += data[i].bookinglist!![k].menulistdetails!![a]!!.menucost!! * data[i].bookinglist!![k].menulistdetails!![a]!!.quantity!!
                                                            subtotal= String.format(Locale("en", "US"),"%.2f",subtotal).toDouble()
                                                            tablename= data[i].tablename.toString()
                                                            Log.d("subtotalllll", "" + subtotal)
//                                                            txtnomenubooked.visibility = View.GONE
///                                                            relative_suborder.visibility =
  //                                                              View.VISIBLE
                                                            recyclermenulist.layoutManager =
                                                                LinearLayoutManager(requireContext())
                                                            recyclermenulist.adapter =
                                                                MenuListAdapter(
                                                                    this,
                                                                    requireContext(),
                                                                    data[i].bookinglist!![k].menulistdetails
                                                                )

                                                                if(data[i].bookinglist!![k].menulistdetails!![a].quantity!=0){
                                                                    menulist!!.add(
                                                                        MenuDetailJson(
                                                                            data[i].bookinglist!![k].menulistdetails!![a].menuid!!,
                                                                            data[i].bookinglist!![k].menulistdetails!![a].quantity!!,
                                                                            data[i].bookinglist!![k].menulistdetails!![a].menucost!!
                                                                        )
                                                                    )
                                                                }


                                                            val jsonArray =
                                                                mapper.writeValueAsString(menulist)
                                                            Log.d("ghb ", "" + jsonArray)


                                                            Log.d(
                                                                "menudetailks",
                                                                "" + data[i].bookinglist!![k].menulistdetails!![a].id
                                                            )

                                                        }
                                                        vat =
                                                            (subtotal / 100.0f) * sharedHelper!!.vat

                                                        vat= String.format(Locale("en", "US"),"%.2f",vat).toDouble()
                                                        totalamt = subtotal + vat - String.format(Locale("en", "US"),"%.2f",data[i].bookinglist!![k].initial_amount!!).toDouble()
                                                        totalamt = String.format(Locale("en", "US"),"%.2f",totalamt).toDouble()


                                                        Log.d("totallllllllll", "" + vat)
                                                        Log.d("awserftghjnkm", "" + totalamt)

                                                     /*   ordersubtotal.text =
                                                            "$subtotal SAR"
                                                        totalamtt.text =
                                                            "$totalamt SAR"
                                                        vat_txt.text =
                                                            "$vat SAR"*/

                                                        ordersubtotal.text=  String.format(Locale("en", "US"),"%.2f",subtotal)+" SAR"
                                                        totalamtt.text =  String.format(Locale("en", "US"),"%.2f",totalamt)+" SAR"
                                                        vat_txt.text = String.format(Locale("en", "US"),"%.2f",vat)+" SAR"

                                                        //    Bookimgamount.setText("" + String.format("%.2f", 50)+" SAR")

                                                        amountdeducted.text = "-"+  String.format(Locale("en", "US"),"%.2f",data[i].bookinglist!![k].initial_amount)+" SAR"
                                                        initial_amtt =   String.format(Locale("en", "US"),"%.2f",data[i].bookinglist!![k].initial_amount).toDouble()

                                                        if (statuspaid != "paid" && booked!="false") {
                                                            if (sharedHelper!!.language.equals("ar")) {
                                                                val swipeHelper: SwipeHelper2 =
                                                                    object :
                                                                        SwipeHelper2(
                                                                            requireContext(),
                                                                            recyclermenulist,
                                                                            menulist
                                                                        ) {
                                                                        override fun instantiateUnderlayButton(
                                                                            viewHolder: RecyclerView.ViewHolder?,
                                                                            underlayButtons: ArrayList<UnderlayButton>?
                                                                        ) {
                                                                            underlayButtons!!.add(
                                                                                UnderlayButton(
                                                                                    this@MenuListFragment,
                                                                                    menulist,
                                                                                    getString(R.string.delete),
                                                                                    0,
                                                                                    Color.parseColor(
                                                                                        "#A41620"
                                                                                    ),
                                                                                    null
                                                                                )
                                                                            )
                                                                        }


                                                                    }
                                                                swipeHelper.attachSwipe()
                                                            } else {
                                                                val swipeHelper: SwipeHelper =
                                                                    object :
                                                                        SwipeHelper(
                                                                            requireContext(),
                                                                            recyclermenulist,
                                                                            menulist
                                                                        ) {
                                                                        override fun instantiateUnderlayButton(
                                                                            viewHolder: RecyclerView.ViewHolder?,
                                                                            underlayButtons: ArrayList<UnderlayButton>?
                                                                        ) {
                                                                            underlayButtons!!.add(
                                                                                UnderlayButton(
                                                                                    this@MenuListFragment,
                                                                                    menulist,
                                                                                    getString(R.string.delete),
                                                                                    0,
                                                                                    Color.parseColor(
                                                                                        "#A41620"
                                                                                    ),
                                                                                    null
                                                                                )
                                                                            )
                                                                        }


                                                                    }
                                                                swipeHelper.attachSwipe()
                                                            }
                                                        }
                                                    }








                                                    if (data[i].bookinglist!![k].discount_name!!.isEmpty()){

                                                        discountlayout.visibility=View.GONE
                                                        rrproducttotal.visibility=View.GONE

                                                        Log.d("wertyuiop[","zxcvbnm,./")
                                                    }else{
                                                        discountlayout.visibility=View.VISIBLE
                                                        rrproducttotal.visibility=View.VISIBLE
                                                        finalsubtotal=subtotal

                                                        val subtotall =subtotal-data[i].bookinglist!![k].discount_amount!!.toDouble()
                                                       // subtotal=subtotall
                                                        ordersubtotal.text=  String.format(Locale("en", "US"),"%.2f",subtotall)+" SAR"
                                                            //  prodcuttotal.text=  String.format(Locale("en", "US"),"%.2f",subtotal+data[i].bookinglist!![k].discount_amount!!.toDouble())+" SAR"
                                                        prodcuttotal.text=  String.format(Locale("en", "US"),"%.2f",subtotal)+" SAR"
                                                        val vatt = (subtotall / 100.0f) * sharedHelper!!.vat
                                                        vat=vatt
                                                        vat_txt.text = String.format(Locale("en", "US"),"%.2f",vatt)+" SAR"
                                                        dicountmmt=  data[i].bookinglist!![k].discount_amount!!.toDouble()

                                                        discountamt.text = "-" +String.format(Locale("en", "US"),"%.2f",
                                                            data[i].bookinglist!![k].discount_amount)+" SAR"

                                                        val total=subtotall+vat- initial_amtt!!
                                                        totalamt=total

                                                        totalamtt.text =  String.format(Locale("en", "US"),"%.2f",total)+" SAR"



                                                    }

                                                }


                                            }


                                        }else{

                                        }






                                    }


                                }


                            }
                        }

                    }

                })
    }




    private fun getintialvalues(){
        DialogUtils.showLoader(requireContext())
        viewmodel?.gethomeDetails(requireContext())
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_suborder, msg)
                                }
                            } else {
                                it.data?.let { data ->



                                }
                            }
                        }

                    }
                })
    }



    private fun getupdateinvoice(){
        DialogUtils.showLoader(requireContext())
        viewmodel?.getupdateinvoice(requireContext(),bookingid,subtotal,vat,totalamt,paymenttype)
                ?.observe(viewLifecycleOwner,
                        Observer {
                            DialogUtils.dismissLoader()

                            it?.let {
                                it.error?.let { error ->
                                    if (error) {
                                        it.message?.let { msg ->
                                            UiUtils.showSnack(root_suborder, msg)
                                        }
                                    }
                                    else {

                                       }
                                }

                            }
                        })
    }



    public fun getbookingmenu(orderid: Int, jsonArray: String) {
        DialogUtils.showLoader(requireContext())
        viewmodel?.gettablebookingmenu(requireContext(), orderid, jsonArray)
                ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->

                                    UiUtils.showSnack(root_suborder, msg)
                                }
                            } else {
                                it.message?.let { msg ->
                                    if (delete.equals("true")){
                                        val bundle = Bundle()
                                        bundle.putInt("tableid",value)
                                        Log.d("tableiddd",""+value)
                                        bundle.putInt("bookingid", bookingid)
                                        bundle.putString("resimg",resimage)
                                        bundle.putString("booked", "true")
                                        Log.d("sdfghjkl", ""+bookingid)
                                        menuListFragment = MenuListFragment()
                                        menuListFragment!!.arguments = bundle
                                        fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                                        fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                                        fragmentTransaction.replace(R.id.container, menuListFragment!!)
                                        fragmentTransaction.commit()
                                    }else{
                                        UiUtils.showSnack(root_suborder, msg)

                                    }


                                }
                            }

                        }
                    }
                })


    }



    fun deletecart(pos: Int){

        menulist!!.removeAt(pos)
        Log.d("sxdcfvgbnm,.",""+menulist)
        val jsonArray = mapper.writeValueAsString(menulist)

        getbookingmenu(bookingid,jsonArray )


    }

    private fun showDialog4() {

        val mDialogView = LayoutInflater.from(requireContext()).inflate(R.layout.popreslayout2, null)
        val mBuilder = AlertDialog.Builder(requireContext()).setView(mDialogView).setCancelable(true)
        val mAlertDialog = mBuilder.show()
        val layoutParams = WindowManager.LayoutParams()
        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        layoutParams.copyFrom(mAlertDialog.getWindow()?.getAttributes())
        layoutParams.width = ((displayWidth * 0.9f).toInt())
        mAlertDialog.getWindow()?.setAttributes(layoutParams)
        mAlertDialog.getWindow()?.setGravity(Gravity.CENTER)
        mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        var cash =  mAlertDialog.findViewById<TextView>(R.id.cash)
       var card = mAlertDialog. findViewById<TextView>(R.id.card)
       var close = mAlertDialog. findViewById<ImageView>(R.id.close)
        card.setOnClickListener {
            paymenttype="card"
          //  pop_up_movetopay.visibility=View.GONE
         //   relative_suborder.alpha=1.0f
            getupdateinvoice()
            val bundle = Bundle()
            bundle.putInt("bookingid",bookingid)
            bundle.putInt("tableid",value)
            bundle.putDouble("subtotal",subtotal)
            bundle.putDouble("vat",vat)
            bundle.putDouble("total",totalamt)
            bundle.putDouble("bookingamount",initial_amtt)
            bundle.putDouble("dicountmmt",dicountmmt)
            bundle.putString("paymenttype",paymenttype)
            bundle.putString("resimg",resimage)
            bundle.putString("tablename",tablename)
            invoiceFragment = InvoiceFragment()
            invoiceFragment!!.arguments = bundle
            fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, invoiceFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
            mAlertDialog.dismiss()


        }


        cash.setOnClickListener {



            paymenttype="cod"
        //    pop_up_movetopay.visibility=View.GONE
        //    relative_suborder.alpha=1.0f
            getupdateinvoice()
            val bundle = Bundle()
            bundle.putInt("bookingid",bookingid)
            bundle.putInt("tableid",value)
            bundle.putDouble("subtotal",subtotal)
            bundle.putDouble("vat",vat)
            bundle.putDouble("total",totalamt)
            bundle.putDouble("bookingamount",initial_amtt)
            bundle.putString("paymenttype",paymenttype)
            bundle.putString("resimg",resimage)
            bundle.putString("tablename",tablename)

            invoiceFragment = InvoiceFragment()
            invoiceFragment!!.arguments = bundle
            fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, invoiceFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
            mAlertDialog.dismiss()

        }


        close.setOnClickListener{
            mAlertDialog.dismiss()

        }
        }


    private fun reloadApp() {



        val intent = Intent(requireContext(), DashBoardActivity::class.java)

        intent.putExtra("langfrom", "Menulistfrag")
        intent.putExtra("tableid", value)
        intent.putExtra("bookingid", bookingid)
        intent.putExtra("booked", booked)
        startActivity(intent)



    }




}




