package com.lia.yello.linedineresturant.Adapter

import android.R.attr
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedineresturant.Fragment.BookingListFragment
import com.lia.yello.linedineresturant.Fragment.MenuListFragment
import com.lia.yello.linedineresturant.Model.BookingListData
import com.lia.yello.linedineresturant.R
import com.lia.yello.linedineresturant.Utils.UiUtils
import com.lia.yello.linedineresturant.activity.BookingList
import com.lia.yello.linedineresturant.activity.MenuListActivity
import android.R.attr.data
import android.app.AlertDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.DisplayMetrics
import android.view.*
import android.widget.LinearLayout
import androidx.core.content.ContentProviderCompat.requireContext
import com.lia.yello.linedineresturant.Fragment.HomeFragment
import com.lia.yello.linedineresturant.Fragment.MenuOrderFragment
import kotlinx.android.synthetic.main.fragment_booking_list.*
import kotlinx.android.synthetic.main.popupbackfd.view.*
import java.io.Serializable
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList


class BookingListAdapter(
    var bookingListFragment: BookingListFragment,
    var context: Context,
    var list: ArrayList<BookingListData>?

) :
    RecyclerView.Adapter<BookingListAdapter.HomeHeaderViewHolder>() {
    private lateinit var fragmentTransaction: FragmentTransaction
    private var menuListFragment: MenuListFragment? = null
    private var menuOrderFragment: MenuOrderFragment? = null
    var v :Int= 0
    var k:Int = 0
    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var custumername: TextView
        var time: TextView
        var date: TextView
        var seat: TextView
        var status: TextView
        var clicktomenu: TextView
        var relative_booking: LinearLayout


        init {
            custumername = view.findViewById(R.id.cust_name)
            time = view.findViewById(R.id.time)
            status = view.findViewById(R.id.status)
            date = view.findViewById(R.id.date)
            clicktomenu = view.findViewById(R.id.clicktomenu)

//            cancelbooking = view.findViewById(R.id.cancelbooking)
            seat = view.findViewById(R.id.seat)
            relative_booking = view.findViewById(R.id.relative_booking)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.bookinglist,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {

        return list!!.size
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {
     /*   if (list!![position].bookingstatus!!.equals("cancelled")&&list!![position].bookingstatus!!.equals("closed")){
            holder.cancelbooking.visibility=View.GONE
        }else{
            holder.cancelbooking.visibility=View.GONE

        }*/


        for (i in 0 until list!!.size){

            if (list!![position].bookingstatus.equals("cancelled")){
                Log.d("aggdsgdgd","true")
            }
        }

        for (i in 0 until list!!.size){

            if (list!![position].bookingstatus.equals("closed")){
                Log.d("bvbvbbvbvvb","false")
            }
        }


        holder.custumername.text = list!![position].customername


          if (list!![position].bookingstatus=="booked"){
              holder.status.text = context.getString(R.string.booked)
              holder.status.setTextColor(context.resources.getColor(R.color.green))
              holder.clicktomenu.visibility=View.VISIBLE

          }else if(list!![position].bookingstatus=="processing"){
              holder.status.text = context.getString(R.string.processing)

              holder.status.setTextColor(context.resources.getColor(R.color.appred))
              holder.clicktomenu.visibility=View.GONE


          }else if (list!![position].bookingstatus=="closed"){
              holder.clicktomenu.visibility=View.GONE
              holder.status.text = context.getString(R.string.closed)
              holder.status.setTextColor(context.resources.getColor(R.color.green))
              k=k+1
              Log.d("sjnsjsj",""+k)


          }else if (list!![position].bookingstatus=="cancelled"){
              holder.clicktomenu.visibility=View.GONE
              holder.status.text = context.getString(R.string.cancelled)
              holder.status.setTextColor(context.resources.getColor(R.color.appred))
                v=v+1

              Log.d("sjnsjsj",""+v)

          }
        bookingListFragment.sharedHelper!!.initialamount=list!![position].initial_amount!!.toFloat()
        holder.time.text = " "+list!![position].timeslots
        holder.date.text =" "+list!![position].date

     //   holder.date.text = list!![position].
        holder.seat.text = context.getString(R.string.id)+list!![position].bookingid.toString()

        if (list!![position].bookingstatus!!.equals("pending")){
            //holder.relative_booking.isEnabled=false
        }
        else{

            holder.relative_booking.isEnabled=true

        }

        holder.relative_booking.setOnClickListener(View.OnClickListener {
/*            val intent = Intent(bookingList, MenuListActivity::class.java)
            intent.putExtra("tableid", bookingList.value)
            intent.putExtra("bookingid", list!![position].bookingid)
            bookingList.startActivity(intent)
            Log.d("tableidd", "" + intent)*/
             bookingListFragment.bookingid= list!![position].bookingid!!
            if (list!![position].bookingstatus!!.equals("booked")){
                val bundle = Bundle()
                bundle.putInt("tableid",bookingListFragment.value)
                Log.d("tableiddd",""+bookingListFragment.value)
                bundle.putInt("bookingid", list!![position].bookingid!!)
              //  bundle.putString("resimg",bookingListFragment.resimage)
                bundle.putString("booked", "true")
                Log.d("sdfghjkl", ""+list!![position].bookingid!!)
                menuListFragment = MenuListFragment()
                menuListFragment!!.arguments = bundle
                fragmentTransaction =bookingListFragment.requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, menuListFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            } else if (list!![position].bookingstatus!!.equals("processing")){
            val bundle = Bundle()
            bundle.putInt("tableid",bookingListFragment.value)
            Log.d("tableiddd",""+bookingListFragment.value)
            bundle.putInt("bookingid", list!![position].bookingid!!)
          //  bundle.putString("resimg",bookingListFragment.resimage)
            bundle.putString("booked", "true")
            Log.d("sdfghjkl", ""+list!![position].bookingid!!)
            menuListFragment = MenuListFragment()
            menuListFragment!!.arguments = bundle
            fragmentTransaction =bookingListFragment.requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, menuListFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        } else if (list!![position].bookingstatus!!.equals("pending")){
            Log.d("aertyuiop","bookingispending")
                val mDialogView = LayoutInflater.from(context).inflate(R.layout.popupbackfd, null)
                val mBuilder = AlertDialog.Builder(context).setView(mDialogView).setCancelable(false)
                val mAlertDialog = mBuilder.show()
                val layoutParams = WindowManager.LayoutParams()
                val displayMetrics = DisplayMetrics()
                bookingListFragment.requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
                val displayWidth = displayMetrics.widthPixels
                val displayHeight = displayMetrics.heightPixels
                layoutParams.copyFrom(mAlertDialog.getWindow()?.getAttributes())
                layoutParams.width = ((displayWidth * 0.9f).toInt())
                mAlertDialog.getWindow()?.setAttributes(layoutParams)
                mAlertDialog.getWindow()?.setGravity(Gravity.CENTER)
                mAlertDialog.getWindow()?.setBackgroundDrawable(
                    ColorDrawable(
                        Color.TRANSPARENT)
                )
                mDialogView.popup_ok.setOnClickListener {
                    mAlertDialog.dismiss()


                    bookingListFragment.homeFragment = HomeFragment()
                    fragmentTransaction = bookingListFragment.requireActivity().supportFragmentManager.beginTransaction()
                    fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                    fragmentTransaction.replace(R.id.container,  bookingListFragment.homeFragment!!)
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
                }


            }
            else{
                val bundle = Bundle()
                bundle.putInt("tableid",bookingListFragment.value)
                Log.d("tableiddd",""+bookingListFragment.value)
                bundle.putInt("bookingid", list!![position].bookingid!!)
                bundle.putString("booked", "false")
              //  bundle.putString("resimg",bookingListFragment.resimage)
                Log.d("sdfghjkl", ""+list!![position].bookingid!!)
                menuListFragment = MenuListFragment()
                menuListFragment!!.arguments = bundle
                fragmentTransaction =bookingListFragment.requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, menuListFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
               }



        })


         holder.clicktomenu.setOnClickListener{

             val bundle = Bundle()
             bundle.putInt("tableid",bookingListFragment.value)
             Log.d("tableiddd",""+bookingListFragment.value)
             bundle.putInt("bookingid", list!![position].bookingid!!)
             bundle.putSerializable("menulist", bookingListFragment.menulist as Serializable)
             Log.d("sdfghjkl", ""+list!![position].bookingid!!)
             menuOrderFragment = MenuOrderFragment()
             menuOrderFragment!!.arguments = bundle
             fragmentTransaction =bookingListFragment.requireActivity().supportFragmentManager.beginTransaction()
             fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
             fragmentTransaction.replace(R.id.container, menuOrderFragment!!)
             fragmentTransaction.addToBackStack(null)
             fragmentTransaction.commit()

         }

    }
}