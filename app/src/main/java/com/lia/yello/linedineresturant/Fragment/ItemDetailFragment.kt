package com.lia.yello.linedineresturant.Fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.lia.yello.linedineresturant.Model.MenuDetailJson
import com.lia.yello.linedineresturant.Model.MenuDetails
import com.lia.yello.linedineresturant.Model.itemdetail
import com.lia.yello.linedineresturant.R
import com.lia.yello.linedineresturant.Session.SharedHelper
import com.lia.yello.linedineresturant.Utils.DialogUtils
import com.lia.yello.linedineresturant.Utils.UiUtils
import com.lia.yello.linedineresturant.ViewModel.MapViewModel
import com.lia.yello.linedineresturant.activity.AddNewMenuListActivity
import kotlinx.android.synthetic.main.activity_add_new_menu_list.*
import kotlinx.android.synthetic.main.activity_item_detail.*
import java.io.Serializable

class ItemDetailFragment  : Fragment(R.layout.fragment_item_detail) {
    private var mapViewModel: MapViewModel? = null
    public var menuid1:Int? = 0
    public var fav:Boolean? =false
    public var menuid:Int? = 0
    public var orderid:Int? = 0
    public var resid1:Int? = 0
    public var cost:Double? = 0.0
    var count:Int=0
    var costprice:Double=0.0
    var subtotal: Double = 0.0
    var vat:Double=0.0
    var menurating:Double=0.0
    var inarray:Boolean = false
    private var viewmodel: MapViewModel? = null
    var sharedHelper: SharedHelper? = null
    var resid: Int = 0
    var lat: Double = 0.0
    var lng: Double = 0.0
    var isclosed = false
    var menuids2: MutableList<Int> = mutableListOf<Int>()
    var menuquantity2: MutableList<Int> = mutableListOf<Int>()
    var value: Int = 0
    var bookingid: Int = 0
    var list: ArrayList<MenuDetails>? = null
    var menulist: ArrayList<MenuDetailJson>? = ArrayList<MenuDetailJson>()
    val mapper = jacksonObjectMapper()
    lateinit var addnewmenu: TextView
    lateinit var name: TextView
    lateinit var ratingitemdetail: TextView
    lateinit var itemdescription: TextView
    lateinit var total: TextView
    lateinit var txtcount: TextView
    lateinit var itemimg: ImageView
    lateinit var additem: TextView
    lateinit var backtomenu: TextView
    private lateinit var fragmentTransaction: FragmentTransaction
    private var addNewMenuListFragment: AddNewMenuListFragment? = null
    var resimage:String=""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //   binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())
        viewmodel = ViewModelProvider(this).get(MapViewModel::class.java)
        val head = view.findViewById<TextView>(R.id.head)
        head.text=getString(R.string.menu)
        name = view.findViewById<TextView>(R.id.name)
        ratingitemdetail = view.findViewById<TextView>(R.id.ratingitemdetail)
        itemdescription = view.findViewById<TextView>(R.id.itemdescription)
        total = view.findViewById<TextView>(R.id.total)
        txtcount = view.findViewById<TextView>(R.id.txtcount)
        additem = view.findViewById<TextView>(R.id.additem)
        backtomenu = view.findViewById<TextView>(R.id.backtomenu)
        itemimg = view.findViewById<ImageView>(R.id.itemimg)

        menuid1 = arguments!!.getInt("menuid")
        value = arguments!!.getInt("tableid")
        bookingid = arguments!!.getInt("bookingid")
        menurating =arguments!!.getDouble("menurating")!!
        inarray = arguments!!.getBoolean("inarray")!!
        resimage= arguments!!.getString("resimg")!!

        menulist = arguments!!.getSerializable("menulist") as ArrayList<MenuDetailJson>
        Log.d("fdfdbvf",""+menulist)
        val jsonArray = mapper.writeValueAsString(menulist)
        Log.d("ghffreb ",""+jsonArray)
        if(inarray == true){
            for (i in 0 until menulist!!.size){
                if(menulist!![i].menuid == menuid1){
                    count = menulist!![i].quantity
                }
            }
        }
        txtcount.text = count.toString()
        getvalues(menuid1!!)
        val back = view.findViewById<ImageView>(R.id.back)
        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F

        }

        back.setOnClickListener(View.OnClickListener {

          //  requireActivity().supportFragmentManager.popBackStack()
            val bundle = Bundle()
            bundle.putSerializable("menulist",menulist as Serializable)
            bundle.putInt("bookingid",bookingid)
            bundle.putInt("tableid",value)
            bundle.putString("frombooking","")
            bundle.putString("resimg",resimage)
            Log.d("menulisstttttt",""+menulist)
            addNewMenuListFragment = AddNewMenuListFragment()
            addNewMenuListFragment!!.arguments = bundle
            fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, addNewMenuListFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()

        })
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                val bundle = Bundle()
                bundle.putSerializable("menulist",menulist as Serializable)
                bundle.putInt("bookingid",bookingid)
                bundle.putInt("tableid",value)
                bundle.putString("frombooking","")
                bundle.putString("resimg",resimage)
                Log.d("menulisstttttt",""+menulist)
                addNewMenuListFragment = AddNewMenuListFragment()
                addNewMenuListFragment!!.arguments = bundle
                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, addNewMenuListFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()

            }
        })


        backtomenu.setOnClickListener {
            val bundle = Bundle()
            bundle.putSerializable("menulist",menulist as Serializable)
            bundle.putInt("bookingid",bookingid)
            bundle.putInt("tableid",value)
            bundle.putString("frombooking","")
            bundle.putString("resimg",resimage)

            addNewMenuListFragment = AddNewMenuListFragment()
            addNewMenuListFragment!!.arguments = bundle
            fragmentTransaction =requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, addNewMenuListFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        }


        plus.setOnClickListener(View.OnClickListener {
            count++
            txtcount.setText("" + count)
            Log.d("count", "" + count)
        })
        minus.setOnClickListener(View.OnClickListener {
            if (count == 0 || count == 1) {
                txtcount.setText("" + count)
                //  UiUtils.showSnack(root_finedineitemdetail,"itemcannotbelessthan0")
            } else {
                count--
                txtcount.setText("" + count)
            }
        })
        additem.setOnClickListener(View.OnClickListener {
            if (inarray == true){
                for (i in 0 until menulist!!.size){
                      costprice=count* cost!!
                    if(menulist!![i].menuid == menuid1){
                        menulist!!.set(i,MenuDetailJson(menuid1!!,count,costprice))
                    }
                }
                UiUtils.showSnack(root_subsubsubhome,getString(R.string.menuisupdatedaddedsuccessfully))
            }
            else{
                if (count==0){
                    UiUtils.showSnack(root_subsubsubhome,getString(R.string.productquantityis0))
                }
                else {
                    inarray = true
                    costprice=count* cost!!
                    menulist!!.add(MenuDetailJson(menuid1!!, count,costprice))
                    val jsonArray = mapper.writeValueAsString(menulist)
                    Log.d("ghb ", "" + jsonArray)
                }
            }
        })


    }
    public fun getvalues(id:Int){
        DialogUtils.showLoader(requireContext())
        viewmodel?.getItemDetail(requireContext(), id)
            ?.observe(requireActivity(),
                Observer {

                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_subsubsubhome, msg)


                                }
                            } else {  it.data?.let { data ->
                                handleResponse(data)
                            }
                            }
                        }

                    }
                })
    }



    fun handleResponse(data: itemdetail) {
        data.name?.let { name.text = it }
        data.description?.let { itemdescription.text = it }
        data.price?.let { total.text = it.toString() + " SAR" }
        data.prepTime?.let { preparetime.text = it }
        data.description?.let { itemdescription.text = it }
        menuid = data.menuid
        cost = data.price!!
        lat = data.lat!!
        lng = data.lng!!
        data.images?.let {
            UiUtils.loadImage(
                itemimg,
                it,
                ContextCompat.getDrawable(requireContext(), R.drawable.logo1)!!
            )
        }
    }
    fun snackbarToast(msg: String) {
        var snackbar: Snackbar
        Snackbar.make(view!!, msg, Snackbar.LENGTH_SHORT)
            .setTextColor(ContextCompat.getColor(requireContext(), R.color.design_default_color_error))
            .setBackgroundTint(ContextCompat.getColor(requireContext(), R.color.white))
            .setDuration(BaseTransientBottomBar.LENGTH_LONG)
            .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
            .show()
    }

}
