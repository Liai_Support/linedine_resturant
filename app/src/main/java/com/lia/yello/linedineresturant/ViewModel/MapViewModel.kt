package com.lia.yello.linedineresturant.ViewModel

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.lia.yello.linedine.activity.HomeResponse
import com.lia.yello.linedineresturant.Model.*
import com.lia.yello.linedineresturant.Repository.MapRepository
import com.lia.yello.linedineresturant.Session.SharedHelper
import com.lia.yello.linedineresturant.Utils.Constants
import com.lia.yello.linedineresturant.network.ApiInput
import com.lia.yello.linedineresturant.network.UrlHelper
import org.json.JSONArray
import org.json.JSONObject

class MapViewModel(application: Application) : AndroidViewModel(application) {

    var repository: MapRepository = MapRepository.getInstance()
    var applicationIns: Application = application
    var sharedHelper: SharedHelper? = SharedHelper(applicationIns.applicationContext)

    private fun getApiParams(context: Context,jsonObject: JSONObject?, methodName: String): ApiInput {

        val header: MutableMap<String, String> = HashMap()
        sharedHelper?.let { header[Constants.ApiKeys.LANG] = it.language }
        //sharedHelper?.let { header[Constants.ApiKeys.AUTHORIZATION] = it.token }
        //header[Constants.ApiKeys.ROLE] = Constants.ApiKeys.PATIENT

        val apiInputs = ApiInput()
        apiInputs.context = context
        apiInputs.jsonObject = jsonObject
        apiInputs.headers = header
        apiInputs.url = methodName

        Log.d("Headers:",""+header)

        return apiInputs
    }



    private fun getApiParams2(context: Context,jsonObject: JSONObject?, methodName: String): ApiInput {

        val header: MutableMap<String, String> = HashMap()
         sharedHelper?.let { header[Constants.ApiKeys.LANG] = it.language }
        //sharedHelper?.let { header[Constants.ApiKeys.AUTHORIZATION] = it.token }
        //header[Constants.ApiKeys.ROLE] = Constants.ApiKeys.PATIENT

        val apiInputs = ApiInput()
        apiInputs.context = context
        apiInputs.jsonObject = jsonObject
      //  apiInputs.headers = header
        apiInputs.url = methodName

        Log.d("Headers:",""+header)

        return apiInputs
    }


    fun getlogin(
        context: Context, email: String, password: String

    ): LiveData<LoginResponseModel>? {

        val jsonObject = JSONObject()

        jsonObject.put("email", email)
        jsonObject.put("password", password)
        Log.d("response",""+jsonObject)

        return repository.signIn(getApiParams(context,jsonObject, UrlHelper.LOGIN))
    }




    fun getforgetpassword(
        context: Context, email: String

    ): LiveData<ForgetPasswordResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("email", email)
        Log.d("response",""+jsonObject)

        return repository.forgetpassword(getApiParams(context,jsonObject, UrlHelper.FORGETPASSWORD))
    }



    fun getresetpassword(
        context: Context,
        password: String,
        oldpass:String

    ): LiveData<ResetPasswordResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put("userid", sharedHelper?.id)
        jsonObject.put("password", password)
        jsonObject.put("oldpassword", oldpass)
        Log.d("response",""+jsonObject)

        return repository.resetpassword(getApiParams(context,jsonObject, UrlHelper.RESETPASSWORD))
    }



    fun getresturantdetail(
        context: Context,
        resid: Int

    ): LiveData<RestuarantDetailResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put("resid", resid)

        Log.d("response",""+jsonObject)

        return repository.resturantdetail(getApiParams(context,jsonObject, UrlHelper.RESTDETAIL))
    }


    fun gettabledetails(
        context: Context,
        resid: Int

    ): LiveData<TableDetailsResponse>? {

        val jsonObject = JSONObject()
        jsonObject.put("resid", resid)

        Log.d("response",""+jsonObject)

        return repository.tabledetails(getApiParams(context,jsonObject, UrlHelper.LISTTABLES))
    }

    fun cancelbooking(
        context: Context, id: Int

    ): LiveData<CancelBookingResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("bookingid", id)

        return repository.cancelbooking(getApiParams(context, jsonObject, UrlHelper.CANCELBOOKING))
    }

    fun getMenuDetail(
        context: Context,
        category: String

    ): LiveData<MenuDetailResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("resid", sharedHelper!!.resid)
        jsonObject.put("category", category)
        jsonObject.put("userid", sharedHelper!!.id)

        return repository.getmenudetail(getApiParams(context,jsonObject, UrlHelper.MENUDETAILS))
    }



    fun getItemDetail(
        context: Context, id: Int


    ): LiveData<ItemDetailResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("itemid", id)
        jsonObject.put("resid", sharedHelper!!.resid)
        return repository.getitemdetail(getApiParams(context,jsonObject, UrlHelper.ITEMDETAIL))
    }



    fun gettablebookingmenu(
        context: Context,
        bookingid: Int,
        jsonArray: String

    ): LiveData<TableBookingMenuResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("bookingid", bookingid)
        jsonObject.put("menu", jsonArray)

        return repository.tablebookingmenu(getApiParams(context,jsonObject, UrlHelper.UPDATEMENU))
    }


    fun gethomeDetails(context : Context): LiveData<HomeResponse>? {
        val jsonObject = JSONObject()
        sharedHelper?.let { jsonObject.put(Constants.ApiKeys.ID, it.id) }
        return repository.gethomedetails(getApiParams(context,jsonObject, UrlHelper.GETHOME))
    }




    fun getupdateinvoice(
            context: Context,
            bookingid: Int,
            subtotal: Double,
            vat: Double,
            totalamount: Double,
            paymenttype: String,

    ): LiveData<UpdateTableInvoiceResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("bookingid", bookingid)
        jsonObject.put("subtotal", subtotal)
        jsonObject.put("vat", vat)
        jsonObject.put("totalamount", totalamount)
        jsonObject.put("paymenttype", paymenttype)

        return repository.getupdateinvoice(getApiParams(context,jsonObject, UrlHelper.UPDATEINVOICE))
    }



    fun getupdatetablestatus(
            context: Context,
            bookingid: Int,
            txt: String,
            tableid: Int


    ): LiveData<UpdateTableStatusResponse>? {

        val jsonObject = JSONObject()

        jsonObject.put("bookingid", bookingid)
        jsonObject.put("resid", sharedHelper!!.resid)
        jsonObject.put("status", txt)
        jsonObject.put("tableid", tableid)
        return repository.getupdatetablestatus(getApiParams(context,jsonObject, UrlHelper.UPDATETABLESTATUS))
    }



    fun updateDeviceToken(context: Context): LiveData<CommonResponseUpdateToken>? {

        var jsonObject = JSONObject()

        jsonObject.put("token", "")
        jsonObject.put("userid", sharedHelper?.id)
        return repository.getupdatedevicetoken(getApiParams2(context,jsonObject, UrlHelper.UPDATEDEVICETOKEN))
        Log.d("asdfghjm",""+jsonObject)

    }


}