package com.lia.yello.linedineresturant.activity

import android.app.AlertDialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.LocaleList
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.lia.yello.linedineresturant.Fragment.*
import com.lia.yello.linedineresturant.R
import com.lia.yello.linedineresturant.Session.SharedHelper
import com.lia.yello.linedineresturant.Utils.DialogUtils
import com.lia.yello.linedineresturant.Utils.UiUtils
import com.lia.yello.linedineresturant.ViewModel.MapViewModel
import kotlinx.android.synthetic.main.activity_dash_board.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_menu_list.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.headerhome.*
import java.util.*


class DashBoardActivity : BaseActivity() {
    private var viewmodel: MapViewModel? = null
    var sharedHelper: SharedHelper? = null
    var resid:Int=0
    private lateinit var fragmentTransaction: FragmentTransaction
     lateinit var fragmentManager: FragmentManager
    private var homeFragment: HomeFragment? = null
    private var bookingListFragment: BookingListFragment? = null
    private var profileFragment: ProfileFragment? = null
    private var menuListFragment: MenuListFragment? = null
    private var menuOrderFragment: MenuOrderFragment? = null
    private var successFragment: SuccessFragment? = null
    private var invoiceFragment: InvoiceFragment? = null
    private var imageList = arrayListOf<ImageView>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dash_board)
        viewmodel = ViewModelProvider(this).get(MapViewModel::class.java)
        sharedHelper = SharedHelper(this)



 if (intent.extras == null) {
          setAdapter(0)

     Log.d("dfghjkl;","nnnnnnn")

        } else {

     var value = intent.extras!!.getString("langfrom")

     if(value.equals("Bookinglistfrag")){

         var tableid = intent.extras!!.getInt("tableid")
         var resimage = intent.extras!!.getString("resimg")

         val bundle = Bundle()
         bundle.putInt("tableid", tableid)
         bundle.putString("resimg", resimage)

         bookingListFragment = BookingListFragment()
         bookingListFragment!!.arguments = bundle
         fragmentTransaction =supportFragmentManager.beginTransaction()
         fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
         fragmentTransaction.replace(R.id.container, bookingListFragment!!)
         fragmentTransaction.addToBackStack(null)
         fragmentTransaction.commit()
     } else if(value.equals("Menulistfrag")){

         var tableid = intent.extras!!.getInt("tableid")
         var bookingid = intent.extras!!.getInt("bookingid")
         var booked = intent.extras!!.getString("booked")

         val bundle = Bundle()
         bundle.putInt("tableid", tableid)
         bundle.putInt("bookingid", bookingid)
         bundle.putString("booked", booked)

         menuListFragment = MenuListFragment()
         menuListFragment!!.arguments = bundle
         fragmentTransaction =supportFragmentManager.beginTransaction()
         fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
         fragmentTransaction.replace(R.id.container, menuListFragment!!)
         fragmentTransaction.addToBackStack(null)
         fragmentTransaction.commit()
     } else if(value.equals("Menuorderfrag")){

         var tableid = intent.extras!!.getInt("tableid")
         var bookingid = intent.extras!!.getInt("bookingid")

         val bundle = Bundle()
         bundle.putInt("tableid", tableid)
         bundle.putInt("bookingid", bookingid)

         menuOrderFragment = MenuOrderFragment()
         menuOrderFragment!!.arguments = bundle
         fragmentTransaction =supportFragmentManager.beginTransaction()
         fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
         fragmentTransaction.replace(R.id.container, menuOrderFragment!!)
         fragmentTransaction.addToBackStack(null)
         fragmentTransaction.commit()
     } else if(value.equals("Profilefrag")){



         profileFragment = ProfileFragment()
         fragmentTransaction =supportFragmentManager.beginTransaction()
         fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
         fragmentTransaction.replace(R.id.container, profileFragment!!)
         fragmentTransaction.addToBackStack(null)
         fragmentTransaction.commit()
     }else if(value.equals("Invoicefrag")){


         var tableid = intent.extras!!.getInt("tableid")
         var resid = intent.extras!!.getInt("resid")
         var bookingid = intent.extras!!.getInt("bookingid")
         var subtotal = intent.extras!!.getDouble("subtotal")
         var vat = intent.extras!!.getDouble("vat")
         var total = intent.extras!!.getDouble("total")
         var bookingamount = intent.extras!!.getDouble("bookingamount")
         var dicountmmt = intent.extras!!.getDouble("dicountmmt")
         var tablename = intent.extras!!.getString("tablename")
         var paymenttype = intent.extras!!.getString("paymenttype")
         var resimage = intent.extras!!.getString("resimg")

         val bundle = Bundle()
         bundle.putInt("tableid", tableid)
         bundle.putInt("bookingid", bookingid)
         bundle.putInt("resid", resid)
         bundle.putDouble("subtotal", subtotal)
         bundle.putDouble("vat", vat)
         bundle.putDouble("total", total)
         bundle.putDouble("bookingamount", bookingamount)
         bundle.putDouble("dicountmmt", dicountmmt)
         bundle.putString("tablename", tablename)
         bundle.putString("paymenttype", paymenttype)
         bundle.putString("resimg", resimage)

         invoiceFragment = InvoiceFragment()
         invoiceFragment!!.arguments = bundle
         fragmentTransaction =supportFragmentManager.beginTransaction()
         fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
         fragmentTransaction.replace(R.id.container, invoiceFragment!!)
         fragmentTransaction.addToBackStack(null)
         fragmentTransaction.commit()
     }else if(value.equals("Successfrag")){


         var tableid = intent.extras!!.getInt("tableid")

         var bookingid = intent.extras!!.getInt("bookingid")

         var paymenttype = intent.extras!!.getString("paymenttype")

         val bundle = Bundle()
         bundle.putInt("tableid", tableid)
         bundle.putInt("bookingid", bookingid)

         bundle.putString("paymenttype", paymenttype)

         successFragment = SuccessFragment()
         successFragment!!.arguments = bundle
         fragmentTransaction =supportFragmentManager.beginTransaction()
         fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
         fragmentTransaction.replace(R.id.container, successFragment!!)
         fragmentTransaction.addToBackStack(null)
         fragmentTransaction.commit()
     }else if(value.equals("dash")){



        setAdapter(0)
     }
 }




        bottom_navigation_main.itemIconTintList = null
        bottom_navigation_main.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.fav -> {
                    sharedHelper!!.ishistory=true

                    setAdapter(2)
                    true
                }
                R.id.home -> {
                    sharedHelper!!.ishistory=false

                    setAdapter(0)
                    true
                }

                R.id.profile -> {
                    setAdapter(1)
                    true
                }
                else -> false
            }
        }








    }



    public fun setAdapter(position: Int) {
        setUiTab(position)
        when (position) {
            0 -> {

                homeFragment = HomeFragment()
                fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, homeFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()


            }
            1 -> {
                profileFragment = ProfileFragment()
                fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, profileFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }


         2-> {
             homeFragment = HomeFragment()
             fragmentTransaction = supportFragmentManager.beginTransaction()
             fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
             fragmentTransaction.replace(R.id.container, homeFragment!!)
             fragmentTransaction.addToBackStack(null)
             fragmentTransaction.commit()
            }



        }
    }

    private fun setUiTab(position: Int) {
        for (i in 0 until imageList.size) {
            if (position == i) {
                // imageList[i].background = ContextCompat.getDrawable(this, R.drawable.tab_selected_background)
                //  imageList[i].setColorFilter(UiUtils.fetchAccentColor(this))
            } else {
                imageList[i].setBackgroundColor(ContextCompat.getColor(this, R.color.black))
                imageList[i].setColorFilter(ContextCompat.getColor(this, R.color.black))
            }
        }
    }

    fun onLanguageClick(view: View) {
        if (sharedHelper!!.language.equals("ar")){
            sharedHelper?.language = "en"
            setPhoneLanguage()
        }else if (sharedHelper!!.language.equals("en")){
            sharedHelper?.language = "ar"
            setPhoneLanguage()
        }
    }




    private fun setPhoneLanguage() {
        var sharedHelper = SharedHelper(this)
        val res = resources
        val conf = res.configuration
        val locale = Locale(sharedHelper.language.toLowerCase())
        Locale.setDefault(locale)
        conf.setLocale(locale)
        val dm = res.displayMetrics
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            conf.setLocales(LocaleList(locale))

        } else {
            conf.locale = locale

        }
        res.updateConfiguration(conf, dm)
        reloadApp()
    }
    private fun reloadApp() {

        val intent = Intent(this, DashBoardActivity::class.java)

        startActivity(intent)


    }






}