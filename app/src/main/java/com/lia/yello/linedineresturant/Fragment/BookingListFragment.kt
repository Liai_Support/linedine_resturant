package com.lia.yello.linedineresturant.Fragment

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.LocaleList
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedineresturant.Adapter.BookingListAdapter
import com.lia.yello.linedineresturant.Model.BookingListData
import com.lia.yello.linedineresturant.R
import com.lia.yello.linedineresturant.Session.SharedHelper
import com.lia.yello.linedineresturant.Utils.DialogUtils
import com.lia.yello.linedineresturant.Utils.UiUtils
import com.lia.yello.linedineresturant.ViewModel.MapViewModel
import kotlinx.android.synthetic.main.activity_booking_list.*
import kotlinx.android.synthetic.main.activity_login.*
import java.io.Serializable
import android.provider.CallLog.Calls
import androidx.databinding.DataBindingUtil
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.lia.yello.linedineresturant.Model.MenuDetailJson
import com.lia.yello.linedineresturant.Model.MenuDetails
import com.lia.yello.linedineresturant.activity.BookingList
import com.lia.yello.linedineresturant.activity.DashBoardActivity
import com.lia.yello.linedineresturant.databinding.FragmentBookingListBinding
import kotlinx.android.synthetic.main.activity_booking_list.liner_frag_order
import kotlinx.android.synthetic.main.activity_booking_list.recyclerbookinglist
import kotlinx.android.synthetic.main.activity_booking_list.rooot_ordersfragment
import kotlinx.android.synthetic.main.activity_booking_list.txtnobooking
import kotlinx.android.synthetic.main.alertdialog.*
import kotlinx.android.synthetic.main.fragment_booking_list.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.headerhome.*
import kotlinx.android.synthetic.main.headerhome.language
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList


class BookingListFragment : Fragment(R.layout.fragment_booking_list) {
    private var viewmodel: MapViewModel? = null
    var sharedHelper: SharedHelper? = null
    var resid: Int = 0
    var binding: FragmentBookingListBinding? = null
    var value:Int=0
    var bookingid:Int=0
    var resimage:String=""
    public var homeFragment: HomeFragment? = null
    public var bookingListFragment: BookingListFragment? = null
    private lateinit var fragmentTransaction: FragmentTransaction
    var booked: ArrayList<BookingListData> = ArrayList()
    var cancelled: ArrayList<BookingListData> = ArrayList()

    var list: ArrayList<MenuDetails>? = null
    var menulist:ArrayList<MenuDetailJson>? = ArrayList<MenuDetailJson>()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())
        viewmodel = ViewModelProvider(this).get(MapViewModel::class.java)
        val head = view.findViewById<TextView>(R.id.head)



        head.text=getString(R.string.bookinglist)

        if (sharedHelper!!.language.equals("ar")) {

            //  language.setImageResource(R.drawable.enicon);
            language2.text = "EN"
        }else{
            language2.text = "AR"

        }
        getvalue(sharedHelper!!.resid)
        value=arguments!!.getInt("tableid")
        resimage= arguments!!.getString("resimg")!!

        val back = view.findViewById<ImageView>(R.id.back)
        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F

        }
        language2.setOnClickListener{
            if (sharedHelper!!.language.equals("ar")){
                sharedHelper?.language = "en"
                reloadApp()
            }else if (sharedHelper!!.language.equals("en")){
                sharedHelper?.language = "ar"
                reloadApp()
            }
        }
        Log.d("bndbdcb",""+sharedHelper!!.ishistory)

        back.setOnClickListener(View.OnClickListener {
            homeFragment = HomeFragment()
            fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, homeFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
      /*      if (sharedHelper!!.ishistory==false){
                homeFragment = HomeFragment()
                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, homeFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }else{
                sharedHelper!!.ishistory==false
                val intent = Intent(requireContext(), DashBoardActivity::class.java)
                // intent.putExtra("langfrom", "inqueue")
                startActivity(intent)
            }*/
        //    requireActivity().supportFragmentManager.popBackStack()


        })





        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {

                homeFragment = HomeFragment()
                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, homeFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()



            }
        })


    }

    private fun getvalue(resid: Int){
        DialogUtils.showLoader(requireContext())
        viewmodel?.gettabledetails(requireContext(),resid)
            ?.observe(requireActivity(),
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {

                                it.message?.let { msg ->
                                    UiUtils.showSnack(rooot_ordersfragment, msg)

                                }

                            } else {
                                it.data?.let { data ->
                                    booked.clear()
                                    cancelled.clear()
                                    for (i in 0 until data.size){

                                        if (value==data[i].tableidd){
                                            if (data[i].bookinglist!!.size!=0){


                                               for(k in 0 until data[i].bookinglist!!.size){

                                                   if(data[i].bookinglist!![k].bookingstatus.equals("booked")||data[i].bookinglist!![k].bookingstatus.equals("pending")||data[i].bookinglist!![k].bookingstatus.equals("processing")) {
                                                       Log.d(
                                                           "asdfgthyjukl",
                                                           "" + data[i].bookinglist!![k].bookingid
                                                       )


                                                       booked.add(data[i].bookinglist!![k])
                                                       Log.d(
                                                           "sizebooked",
                                                           "" + data[i].bookinglist!!.size
                                                       )

                                                       liner_frag_order.visibility = View.VISIBLE
                                                       txtnobooking.visibility = View.GONE
                                                       Log.d("valuessss", "" + data[i].tableidd)


                                                       if (sharedHelper!!.ishistory == false) {
                                                           recyclerbookinglist.layoutManager =
                                                               LinearLayoutManager(requireContext())

                                                           recyclerbookinglist.adapter =
                                                               BookingListAdapter(
                                                                   this, requireContext(),
                                                                   booked
                                                               )

                                                           canceelled.visibility = View.GONE
                                                           closedd.visibility = View.GONE
                                                       } else if (sharedHelper!!.ishistory == true && cancelled.size == 0) {

                                                           liner_frag_order.visibility = View.GONE
                                                           txtnobooking.visibility = View.VISIBLE
                                                           Log.d("wertyuiop", "kanikashanmugam")


                                                       }
                                                   }
                                                   else{
                                                       Log.d("wertghyuj",""+data[i].bookinglist!![k].bookingid)
                                                       cancelled.add(data[i].bookinglist!![k])

                                                       if (sharedHelper!!.ishistory==true){
                                                           Log.d("kanikasjkaak","shajnsgs")

                                                           if (cancelled.size!=0){
                                                               Log.d("esawaeewewew","yuyuyuyu")

                                                               recyclerbookinglist.layoutManager =
                                                                   LinearLayoutManager(requireContext())

                                                               recyclerbookinglist.adapter =
                                                                   BookingListAdapter(
                                                                       this,requireContext(),
                                                                       cancelled
                                                                   )
                                                               canceelled.visibility=View.VISIBLE
                                                               closedd.visibility=View.VISIBLE


                                                           }else{
                                                               Log.d("nbnbnbnvc","awesedrx")

                                                               liner_frag_order.visibility=View.GONE
                                                               txtnobooking.visibility=View.VISIBLE
                                                           }

                                                       }else if (sharedHelper!!.ishistory==false&&booked.size==0){

                                                         liner_frag_order.visibility=View.GONE
                                                           txtnobooking.visibility=View.VISIBLE
                                                              Log.d("wertyuiop","kanikashanmugam")
                                                       }else{
                                                           Log.d("aewaeweaw","khgfsasdd")

                                                       }
                                                       Log.d("sizecancelled",""+data[i].bookinglist!!.size)


                                                   }
                                               }

                                            }
                                            else{
                                               liner_frag_order.visibility=View.GONE
                                                txtnobooking.visibility=View.VISIBLE
                                                 Log.d("sxdcfvgbhbnjkm","yes")





                                            }



                                        }else{

                                        }
                                    }
                                       var count=0
                                       var count1=0
                                    for (i in 0 until cancelled.size ){
                                        if (cancelled[i].bookingstatus=="closed"){
                                            count++
                                            Log.d("uytrewtre",""+count)
                                            closedd.text=getString(R.string.closed)+" : "+count

                                        }else if (cancelled[i].bookingstatus=="cancelled"){
                                            count1++
                                            Log.d("bnnbbnncvbvvcv",""+count1)
                                           canceelled.text=getString(R.string.cancelled)+" : "+count1

                                        }
                                    }




                                }


                            }
                        }

                    }
                })
    }



    public fun cancelbooking(id:Int){
        DialogUtils.showLoader(requireContext())

        viewmodel?.cancelbooking(requireContext(),id)
            ?.observe(viewLifecycleOwner,
                Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(rooot_ordersfragment, msg)

                                }
                            }
                            else {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(rooot_ordersfragment, msg)
                                    homeFragment = HomeFragment()

                                    fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                                    fragmentTransaction.setCustomAnimations(R.anim.screen_in,R.anim.screen_out)
                                    fragmentTransaction.replace(R.id.container, homeFragment!!)
                                    fragmentTransaction.addToBackStack(null)
                                    fragmentTransaction.commit()

                                }
                            }
                        }

                    }
                })

    }







    private fun reloadApp() {
        val intent = Intent(requireContext(), DashBoardActivity::class.java)

        intent.putExtra("langfrom", "Bookinglistfrag")
        intent.putExtra("tableid", value)
        intent.putExtra("resimg", resimage)
        startActivity(intent)



    }
}