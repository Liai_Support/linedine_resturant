package com.lia.yello.linedineresturant.Fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.lia.yello.linedineresturant.Adapter.NewMenuListadapter
import com.lia.yello.linedineresturant.Model.MenuDetailJson
import com.lia.yello.linedineresturant.Model.MenuDetails
import com.lia.yello.linedineresturant.R
import com.lia.yello.linedineresturant.Session.SharedHelper
import com.lia.yello.linedineresturant.Utils.DialogUtils
import com.lia.yello.linedineresturant.Utils.UiUtils
import com.lia.yello.linedineresturant.ViewModel.MapViewModel
import kotlinx.android.synthetic.main.activity_add_new_menu_list.*
import kotlinx.android.synthetic.main.header.*
import java.util.*
import kotlin.collections.ArrayList

class AddNewMenuListFragment  : Fragment(R.layout.fragment_add_new_menu_list) {
    private var viewmodel: MapViewModel? = null
    var sharedHelper: SharedHelper? = null
    var resid: Int = 0
    var lat:Double=0.0
    var lng:Double=0.0
    var isclosed = false
    var menuids2: MutableList<Int> = mutableListOf<Int>()
    var menuquantity2: MutableList<Int> = mutableListOf<Int>()
    var value: Int = 0
    var bookingid: Int = 0
    var list: ArrayList<MenuDetails>? = null
    var menulist: ArrayList<MenuDetailJson>? = ArrayList<MenuDetailJson>()
    val mapper = jacksonObjectMapper()
    lateinit var addnewmenu: TextView
    lateinit var menuname: TextView
    lateinit var work: TextView
    lateinit var rating: TextView
    lateinit var menu_description: TextView
    lateinit var time: TextView
    lateinit var location: TextView
    lateinit var bannerimg: ImageView
    lateinit var bookmenu: Button
    lateinit var recycler: RecyclerView
    private var menuListFragment: MenuListFragment? = null
    private lateinit var fragmentTransaction: FragmentTransaction
    var frombooking:String=""
    lateinit var head:TextView
    private var homeFragment: HomeFragment? = null
    var resimage:String=""


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //   binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())
        viewmodel = ViewModelProvider(this).get(MapViewModel::class.java)
         head = view.findViewById<TextView>(R.id.head)
        head.text=getString(R.string.menu)
        val back = view.findViewById<ImageView>(R.id.back)

        recycler = view.findViewById<RecyclerView>(R.id.recycler)
        menuname = view.findViewById<TextView>(R.id.menuname)
        work = view.findViewById<TextView>(R.id.work)
        rating = view.findViewById<TextView>(R.id.rating)
        location = view.findViewById<TextView>(R.id.location)
        menu_description = view.findViewById<TextView>(R.id.menu_description)
        time = view.findViewById<TextView>(R.id.time)
        bannerimg = view.findViewById<ImageView>(R.id.bannerimg)
        bookmenu = view.findViewById<Button>(R.id.bookmenu)
        resimage= arguments!!.getString("resimg")!!

        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F

        }
        back.setOnClickListener(View.OnClickListener {

            //requireActivity().supportFragmentManager.popBackStack()
            val bundle = Bundle()
            bundle.putInt("bookingid",bookingid)
            bundle.putInt("tableid",value)
            bundle.putString("booked","true")
            bundle.putString("resimg",resimage)
            menuListFragment = MenuListFragment()
            menuListFragment!!.arguments = bundle
            fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, menuListFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()

        })

        location.setOnClickListener(View.OnClickListener {
            val uri = String.format(
                Locale.ENGLISH,
                "google.navigation:q=$lat,$lng"
            )
            val gmmIntentUri = Uri.parse(uri)
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            requireActivity().startActivity(mapIntent)
        })

        frombooking= arguments!!.getString("frombooking").toString()

        if (frombooking.equals("frombooking")){

               bookingid=arguments!!.getInt("bookingid")
               Log.d("asdfghjk",""+bookingid)
            value = arguments!!.getInt("tableid")

           }else{
               
               bookingid=arguments!!.getInt("bookingid")
               Log.d("asdfghjk",""+bookingid)

                value = arguments!!.getInt("tableid")
               menulist = arguments?.getSerializable("menulist") as ArrayList<MenuDetailJson>
               Log.d("zsxdcfvgbnhmjj,.",""+menulist)
               Log.d("menulisstttttt",""+menulist)
               Log.d("zsxdcfvgbnhmjj,.",""+bookingid)

           }


        getvalues("All")





        bookmenu.setOnClickListener(View.OnClickListener {
            if(menulist!!.isEmpty()==false && menulist!!.size!=0){
                val jsonArray = mapper.writeValueAsString(menulist)
                Log.d("chvb",""+jsonArray)
                getbookingmenu(bookingid,jsonArray )
            }
            else{

                UiUtils.showSnack(root_subsubhome, getString(R.string.addsomemenu))
            }
        })

        val jsonArray = mapper.writeValueAsString(menulist)
        Log.d("chvb",""+jsonArray)


        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {


                val bundle = Bundle()
                bundle.putInt("bookingid",bookingid)
                bundle.putInt("tableid",value)
                bundle.putString("booked","true")
                bundle.putString("resimg",resimage)
                menuListFragment = MenuListFragment()
                menuListFragment!!.arguments = bundle
                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, menuListFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }
        })

    }
    public fun getvalues(txt: String){
        DialogUtils.showLoader(requireContext())
        viewmodel?.getMenuDetail(requireContext(), txt)
            ?.observe(requireActivity(),
                Observer {

                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_subsubhome, msg)
                                    recycler.visibility = View.GONE

                                    //  Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()

                                }
                            } else {
                                recycler.visibility = View.VISIBLE
                                it.message?.let { msg ->
                                    // UiUtils.showSnack(root_subsubhome, msg)

                                    // Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
                                }

                                it.data?.let { data ->
                                    Log.d("asxdcfvgb", "" + data.restdetail)
                                    Log.d("asxdcfvgb", "" + data.data2)
                                    Log.d("asxdcfvgb", "" + data.menudetail)
                                    data.restdetail?.res_name?.let { menuname.text = it }

                                    rating.text = data.restdetail!!.rating + "/" + "5"


                                    //  data.restdetail?.rating?.let { rating.text = it }
                                    data.restdetail?.res_name?.let { head.text = it }
                                    data.restdetail?.description?.let { menu_description.text = it }
                                    if (data.data2!![0].closed.equals("0")) {
                                        recycler.visibility = View.VISIBLE
                                        if (data.data2!!.size != 0) {

                                            var time1: String? = data.data2!![0].openingTime
                                            var time2: String? = data.data2!![0].closingTime
                                            var time3: String? = " " + time1 + " - " + time2 + " "
                                            Log.d("sdfghjkl", "" + time3)
                                            work.visibility = View.VISIBLE
                                            time.text = time3
                                        } else {
                                            time.text = getString(R.string.invalidtime)
                                        }
                                    } else {
                                        work.visibility = View.GONE
                                        time.text = getString(R.string.closed)
                                        isclosed = true
                                        //recycler.visibility=View.GONE

                                    }

                                    lat = data.restdetail?.lat!!
                                    lng = data.restdetail?.lng!!
                                    data.restdetail?.bannerimg?.let {
                                        UiUtils.loadImage(
                                            bannerimg,
                                            it,
                                            ContextCompat.getDrawable(
                                                requireContext(),
                                                R.drawable.logo1
                                            )!!
                                        )
                                    }

                                    recycler.layoutManager =
                                        GridLayoutManager(
                                            requireContext(),
                                            2
                                        )
                                  /*  recycler.adapter = NewMenuListadapter(this,
                                        requireContext(), data.menudetail, isclosed, menuquantity2, menuids2
                                    )*/

                                }
                            }
                        }

                    }
                })
    }


    public fun getbookingmenu(orderid: Int, jsonArray: String) {
        DialogUtils.showLoader(requireContext())
        viewmodel?.gettablebookingmenu(requireContext(), orderid, jsonArray)
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->

                                UiUtils.showSnack(root_subsubhome, msg)
                            }
                        } else {
                            it.message?.let { msg ->
                                UiUtils.showSnack(root_subsubhome, msg)
                                val bundle = Bundle()
                                bundle.putInt("bookingid",bookingid)
                                bundle.putInt("tableid",value)
                                bundle.putString("booked","true")
                                bundle.putString("resimg",resimage)

                                Log.d("asdfrgthyjukilo;",""+bookingid)
                                menuListFragment = MenuListFragment()
                                menuListFragment!!.arguments = bundle
                                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                                fragmentTransaction.setCustomAnimations(R.anim.screen_in,R.anim.screen_out)
                                fragmentTransaction.replace(R.id.container, menuListFragment!!)
                                fragmentTransaction.addToBackStack(null)
                                fragmentTransaction.commit()


                            }
                        }

                    }
                }
            })


    }



}