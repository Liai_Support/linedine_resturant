package com.lia.yello.linedineresturant.Fragment

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.LocaleList
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.widget.CompoundButton
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.widget.SwitchCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedine.adapter.TableListAdapter
import com.lia.yello.linedineresturant.R
import com.lia.yello.linedineresturant.Session.SharedHelper
import com.lia.yello.linedineresturant.Utils.DialogUtils
import com.lia.yello.linedineresturant.Utils.UiUtils
import com.lia.yello.linedineresturant.ViewModel.MapViewModel
import com.lia.yello.linedineresturant.activity.DashBoardActivity
import com.lia.yello.linedineresturant.activity.LoginActivity
import com.lia.yello.linedineresturant.activity.RestuarantIntroActivity
import com.lia.yello.linedineresturant.databinding.FragmentHomeBinding
import kotlinx.android.synthetic.main.activity_dash_board.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_menu_list.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.headerhome.*
import kotlinx.android.synthetic.main.popup_password.view.*
import java.util.*

class HomeFragment : Fragment(R.layout.fragment_home) {
    private var viewmodel: MapViewModel? = null
    var sharedHelper: SharedHelper? = null
    var resid:Int=0
    var resimage:String=""
    var doubleBackToExitPressedOnce :Boolean = false
    var binding: FragmentHomeBinding? = null;
    lateinit var resname:TextView
    lateinit var chooselanguage:TextView
    lateinit var res_description:TextView
    lateinit var resetpassword:TextView
    lateinit var bannerimg:ImageView
    var newpass1: String = ""
    var newpass2: String = ""
    var finalpass: String = ""
    var langcode :String = ""

    var menuids: MutableList<Int> = mutableListOf<Int>()
    var menuids2: MutableList<Int> = mutableListOf<Int>()
    var menuquantity: MutableList<Int> = mutableListOf<Int>()
    var menuquantity2: MutableList<Int> = mutableListOf<Int>()
   // lateinit var customSwitch1:SwitchCompat
   lateinit var logoutt:ImageView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())
        viewmodel = ViewModelProvider(this).get(MapViewModel::class.java)
        val head = view.findViewById<TextView>(R.id.head)
        logoutt = view.findViewById<ImageView>(R.id.logout1)



        if (sharedHelper!!.ishistory==true){
            head.setText(getString(R.string.history))

        }else{
            head.setText(getString(R.string.listoftables))

        }

       //getvalue(sharedHelper!!.resid)
        gettabledetail(sharedHelper!!.resid)
       if (sharedHelper!!.language.equals("ar")) {

           language.text = "EN"
       }else{
           language.text = "AR"

       }


        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {


                if (sharedHelper!!.ishistory==true){
                    sharedHelper!!.ishistory=false


                    gettabledetail(sharedHelper!!.resid)
                    head.setText(getString(R.string.listoftables))

                }else{
                    if (doubleBackToExitPressedOnce) {
                        val exitIntent=Intent(Intent.ACTION_MAIN)
                        exitIntent.addCategory(Intent.CATEGORY_HOME)
                        exitIntent.flags=Intent.FLAG_ACTIVITY_CLEAR_TOP
                        startActivity(exitIntent)
                        return
                    }

                    doubleBackToExitPressedOnce = true
                    Toast.makeText(requireContext(), getString(R.string.pleaseclickbackagaintoexit), Toast.LENGTH_SHORT).show()

                    Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
                }




            }
        })

       if (sharedHelper!!.language == "ar"){
           logout1.rotation= 180F

       }




/*
        logoutt.setOnClickListener(View.OnClickListener {


            val intent = Intent(requireContext(), RestuarantIntroActivity::class.java)
            // intent.putExtra("langfrom", "inqueue")
            startActivity(intent)
       */
/*     sharedHelper?.loggedIn = false
            sharedHelper?.token = ""
            sharedHelper?.id = 0
            sharedHelper?.name = ""
            sharedHelper?.email = ""
            //sharedHelper?.mobileNumber = ""
            // sharedHelper?.countryCode = ""

            val intent =
                    Intent(requireContext(), LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.flags =
                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)*//*



        })
*/

       /* resetpassword.setOnClickListener(View.OnClickListener {
            showDialog4()
        })*/



        /* if (sharedHelper!!.language.equals("ar")){
             chooselanguage.text="Switch Language"
         }else  if (sharedHelper!!.language.equals("en")){
             chooselanguage.text="اختر اللغة"

         }*/
    /*   chooselanguage.setOnClickListener(View.OnClickListener {
           if (sharedHelper!!.language.equals("ar")){
               sharedHelper?.language = "en"
               setPhoneLanguage()
           }else if (sharedHelper!!.language.equals("en")){
               sharedHelper?.language = "ar"
               setPhoneLanguage()
           }

          })*/
       getupdatetoken()


        logoutt.setOnClickListener(View.OnClickListener {


            if (sharedHelper!!.ishistory==true){
                sharedHelper!!.ishistory=false


                gettabledetail(sharedHelper!!.resid)
                head.setText(getString(R.string.listoftables))

            }else{
                       val exitIntent=Intent(Intent.ACTION_MAIN)
                        exitIntent.addCategory(Intent.CATEGORY_HOME)
                        exitIntent.flags=Intent.FLAG_ACTIVITY_CLEAR_TOP
                        startActivity(exitIntent)
            }


        })


   }


/*
    private fun showDialog4() {

        val mDialogView = LayoutInflater.from(requireContext()).inflate(R.layout.popup_password, null)
        val mBuilder = AlertDialog.Builder(requireContext()).setView(mDialogView).setCancelable(true)
        val mAlertDialog = mBuilder.show()
        val layoutParams = WindowManager.LayoutParams()
        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
        val displayWidth = displayMetrics.widthPixels
        val displayHeight = displayMetrics.heightPixels
        layoutParams.copyFrom(mAlertDialog.getWindow()?.getAttributes())
        layoutParams.width = ((displayWidth * 0.9f).toInt())
        mAlertDialog.getWindow()?.setAttributes(layoutParams)
        mAlertDialog.getWindow()?.setGravity(Gravity.CENTER)
        mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        newpass1 = mDialogView.pass1.text.toString()
        newpass2 = mDialogView.pass2.text.toString()
        mDialogView.resetpassconfirm.setOnClickListener {
              if (sharedHelper!!.oldpassword!=mDialogView.pass1.text.toString()){

                  UiUtils.showSnack(rootdash,getString(R.string.oldpassisincorrect))
                //  mAlertDialog.dismiss()

              }else{

                  newpass1 = mDialogView.pass1.text.toString()
                  // new pass
                  newpass2 = mDialogView.pass2.text.toString()
                  // old pass
                  resetpassword(newpass1, newpass2)
                  mAlertDialog.dismiss()
              }




        }
    }
*/



/*
    private fun resetpassword(finalpassword: String, finalpassword2: String){
        DialogUtils.showLoader(requireContext())
        viewmodel?.getresetpassword(requireContext(), finalpassword, finalpassword2)
                ?.observe(requireActivity(),
                        Observer {
                            DialogUtils.dismissLoader()

                            it?.let {
                                it.error?.let { error ->
                                    if (error) {
                                        UiUtils.showSnack(rootdash, "msg")

                                    } else {
                                        it.message?.let { msg ->
                                            UiUtils.showSnack(rootdash, msg)
                                            sharedHelper?.loggedIn = false
                                            sharedHelper?.token = ""
                                            sharedHelper?.id = 0
                                            sharedHelper?.name = ""
                                            sharedHelper?.email = ""


                                            val intent =
                                                    Intent(requireContext(), LoginActivity::class.java)
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                            intent.flags =
                                                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                            startActivity(intent)

                                        }

                                    }
                                }

                            }
                        })
    }
*/



/*
    private fun getvalue(resid: Int){
        DialogUtils.showLoader(requireContext())
        viewmodel?.getresturantdetail(requireContext(), resid)
            ?.observe(requireActivity(),
                    Observer {
                        DialogUtils.dismissLoader()

                        it?.let {
                            it.error?.let { error ->
                                if (error) {

                                    it.message?.let { msg ->
                                        UiUtils.showSnack(rootdash, msg)
                                    }

                                } else {
                                    it.data?.let { data ->
                                        resname.text = data.res_name
                                        res_description.text = data.description
                                        data.banner?.let {
                                            UiUtils.loadImage(
                                                    bannerimg,
                                                    it,
                                                  ContextCompat.getDrawable(
                                                            context!!,
                                                            R.drawable.logo1
                                                    )!!
                                            )

                                        }
                                        resimage = data.banner!!

                                    }


                                }
                            }

                        }
                    })
    }
*/



    private fun gettabledetail(resid: Int){
        DialogUtils.showLoader(requireContext())
        viewmodel?.gettabledetails(requireContext(), resid)
            ?.observe(requireActivity(),
                    Observer {
                        DialogUtils.dismissLoader()

                        it?.let {
                            it.error?.let { error ->
                                if (error) {

                                    it.message?.let { msg ->
                                        UiUtils.showSnack(rootdash, msg)
                                    }

                                } else {
                                    it.data?.let { data ->


                                        table_list.layoutManager =
                                                GridLayoutManager(
                                                        requireContext(),
                                                        2
                                                )
                                        table_list.adapter = TableListAdapter(
                                                this, requireContext(), data
                                        )

                                    }


                                }
                            }

                        }
                    })

    }


    private fun setPhoneLanguage() {
        var sharedHelper = SharedHelper(requireContext())
        val res = resources
        val conf = res.configuration
        val locale = Locale(sharedHelper.language.toLowerCase())
        Locale.setDefault(locale)
        conf.setLocale(locale)
        val dm = res.displayMetrics
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            conf.setLocales(LocaleList(locale))

        } else {
            conf.locale = locale

        }
        res.updateConfiguration(conf, dm)
        reloadApp()
    }
    private fun reloadApp() {

        val intent = Intent(requireContext(), DashBoardActivity::class.java)
        intent.putExtra("langfrom", "settings")
        startActivity(intent)


    }


    public fun getupdatetoken() {
        DialogUtils.showLoader(requireContext())
        viewmodel?.updateDeviceToken(requireContext())
                ?.observe(requireActivity(),
                        Observer {
                            DialogUtils.dismissLoader()

                            it?.let {
                                it.error?.let { error ->
                                    if (error) {
                                        it.message?.let { msg ->

                                            UiUtils.showSnack(rootdash, msg)
                                        }
                                    } else {
                                        it.data?.let { data ->

                                            if (data.status.equals("inactive")) {
                                                val builder = AlertDialog.Builder(requireContext())
                                                builder.setTitle(R.string.alert)
                                                builder.setMessage(R.string.admincloseyourstatuspleaseexit)
                                                // Set a positive button and its click listener on alert dialog
                                                builder.setPositiveButton("ok") { dialog, which ->
                                                    sharedHelper?.loggedIn = false
                                                    sharedHelper?.token = ""
                                                    sharedHelper?.id = 0
                                                    sharedHelper?.name = ""
                                                    sharedHelper?.email = ""


                                                    val intent =
                                                            Intent(requireContext(), LoginActivity::class.java)
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                                    intent.flags =
                                                            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                                    startActivity(intent)

                                                }


                                                val dialog: AlertDialog = builder.create()
                                                dialog.show()
                                                dialog.setCancelable(false)


                                            }
                                        }


                                    }
                                }
                            }
                        })


    }


}

