package com.lia.yello.linedineresturant.Interface

import org.json.JSONObject

interface ApiResponseCallback {
    fun setResponseSuccess(jsonObject: JSONObject)
    fun setErrorResponse(error : String)
}