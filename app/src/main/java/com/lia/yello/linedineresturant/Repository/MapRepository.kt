package com.lia.yello.linedineresturant.Repository
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.lia.yello.linedine.activity.HomeResponse
import com.lia.yello.linedineresturant.Interface.ApiResponseCallback
import com.lia.yello.linedineresturant.Model.*
import com.lia.yello.linedineresturant.network.Api
import com.lia.yello.linedineresturant.network.ApiInput
import org.json.JSONObject

class MapRepository private constructor() {

    companion object {
        var repository: MapRepository? = null

        fun getInstance(): MapRepository {
            if (repository == null) {
                repository = MapRepository()
            }
            return repository as MapRepository
        }
    }


    fun signIn(input: ApiInput): LiveData<LoginResponseModel>? {

        val apiResponse: MutableLiveData<LoginResponseModel> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: LoginResponseModel =
                        gson.fromJson(jsonObject.toString(), LoginResponseModel::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = LoginResponseModel()
                response.message = error
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun forgetpassword(input: ApiInput): LiveData<ForgetPasswordResponse>? {

        val apiResponse: MutableLiveData<ForgetPasswordResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: ForgetPasswordResponse =
                    gson.fromJson(jsonObject.toString(), ForgetPasswordResponse::class.java)
                response.error = false
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = ForgetPasswordResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }



    fun resetpassword(input: ApiInput): LiveData<ResetPasswordResponse>? {

        val apiResponse: MutableLiveData<ResetPasswordResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: ResetPasswordResponse =
                    gson.fromJson(jsonObject.toString(), ResetPasswordResponse::class.java)
                response.error = false
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = ResetPasswordResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun resturantdetail(input: ApiInput): LiveData<RestuarantDetailResponse>? {

        val apiResponse: MutableLiveData<RestuarantDetailResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: RestuarantDetailResponse =
                    gson.fromJson(jsonObject.toString(), RestuarantDetailResponse::class.java)
                response.error = false
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = RestuarantDetailResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }


    fun tabledetails(input: ApiInput): LiveData<TableDetailsResponse>? {

        val apiResponse: MutableLiveData<TableDetailsResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: TableDetailsResponse =
                    gson.fromJson(jsonObject.toString(), TableDetailsResponse::class.java)
                response.error = false
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = TableDetailsResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun cancelbooking(input: ApiInput): LiveData<CancelBookingResponse>? {

        val apiResponse: MutableLiveData<CancelBookingResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CancelBookingResponse =
                    gson.fromJson(jsonObject.toString(), CancelBookingResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CancelBookingResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getmenudetail(input: ApiInput): LiveData<MenuDetailResponse>? {

        val apiResponse: MutableLiveData<MenuDetailResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: MenuDetailResponse =
                    gson.fromJson(jsonObject.toString(), MenuDetailResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = MenuDetailResponse()
                response.error = true
                apiResponse.value = response
            }
        })
        return apiResponse

    }



    fun getitemdetail(input: ApiInput): LiveData<ItemDetailResponse>? {

        val apiResponse: MutableLiveData<ItemDetailResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: ItemDetailResponse = gson.fromJson(jsonObject.toString(), ItemDetailResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = ItemDetailResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }



    fun tablebookingmenu(input: ApiInput): LiveData<TableBookingMenuResponse>? {

        val apiResponse: MutableLiveData<TableBookingMenuResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: TableBookingMenuResponse =
                    gson.fromJson(jsonObject.toString(), TableBookingMenuResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = TableBookingMenuResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }


    fun gethomedetails(input: ApiInput): LiveData<HomeResponse>? {

        val apiResponse: MutableLiveData<HomeResponse> = MutableLiveData()

        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: HomeResponse =
                    gson.fromJson(jsonObject.toString(), HomeResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = HomeResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }



    fun getupdateinvoice(input: ApiInput): LiveData<UpdateTableInvoiceResponse>? {

        val apiResponse: MutableLiveData<UpdateTableInvoiceResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: UpdateTableInvoiceResponse =
                        gson.fromJson(jsonObject.toString(), UpdateTableInvoiceResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = UpdateTableInvoiceResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }




    fun getupdatetablestatus(input: ApiInput): LiveData<UpdateTableStatusResponse>? {

        val apiResponse: MutableLiveData<UpdateTableStatusResponse> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: UpdateTableStatusResponse =
                        gson.fromJson(jsonObject.toString(), UpdateTableStatusResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = UpdateTableStatusResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }
    fun getupdatedevicetoken(input: ApiInput): LiveData<CommonResponseUpdateToken>? {

        val apiResponse: MutableLiveData<CommonResponseUpdateToken> = MutableLiveData()

        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponseUpdateToken =
                        gson.fromJson(jsonObject.toString(), CommonResponseUpdateToken::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CommonResponseUpdateToken()
                response.error = true
                response.message = error
                apiResponse.value = response
            }
        })

        return apiResponse
    }



}