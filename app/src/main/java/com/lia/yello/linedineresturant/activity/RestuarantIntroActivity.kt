package com.lia.yello.linedineresturant.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.lia.yello.linedineresturant.R
import com.lia.yello.linedineresturant.Session.SharedHelper
import com.lia.yello.linedineresturant.Utils.DialogUtils
import com.lia.yello.linedineresturant.Utils.UiUtils
import com.lia.yello.linedineresturant.ViewModel.MapViewModel
import kotlinx.android.synthetic.main.fragment_home.*
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_restuarant_intro.*
import kotlin.system.exitProcess

class RestuarantIntroActivity : AppCompatActivity() {

    private var viewmodel: MapViewModel? = null
    var sharedHelper: SharedHelper? = null
    var resimage:String=""
    var doubleBackToExitPressedOnce :Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_restuarant_intro)
        viewmodel = ViewModelProvider(this).get(MapViewModel::class.java)
        sharedHelper = SharedHelper(this)
        getvalue(sharedHelper!!.resid)
        sharedHelper?.ishistory = false

        next.setOnClickListener{
            val intent = Intent(this, DashBoardActivity::class.java)
            intent.putExtra("langfrom", "dash")
            startActivity(intent)
        }

    }


    private fun getvalue(resid: Int){
        DialogUtils.showLoader(this)
        viewmodel?.getresturantdetail(this, resid)
            ?.observe(this,
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {

                                it.message?.let { msg ->
                                    UiUtils.showSnack(rootdash, msg)
                                }

                            } else {
                                it.data?.let { data ->
                                    restname.text = data.res_name
                                    restdescrip.text = data.description
                                  data.banner?.let {
                                        UiUtils.loadImage(
                                            imagerest,
                                            it,
                                            ContextCompat.getDrawable(
                                                this,
                                                R.drawable.logo1
                                            )!!
                                        )

                                    }
                                   // restimgroot.setBackgroundDrawable(ContextCompat.getDrawable(this, data.banner!!.toInt()) )
                                    resimage = data.banner!!

                                }


                            }
                        }

                    }
                })
    }

    override fun onBackPressed() {
       // super.onBackPressed()

        if (doubleBackToExitPressedOnce) {
            val exitIntent=Intent(Intent.ACTION_MAIN)
            exitIntent.addCategory(Intent.CATEGORY_HOME)
            exitIntent.flags=Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(exitIntent)
            return
        }

        doubleBackToExitPressedOnce = true
        Toast.makeText(this, getString(R.string.pleaseclickbackagaintoexit), Toast.LENGTH_SHORT).show()

        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)

    }




}