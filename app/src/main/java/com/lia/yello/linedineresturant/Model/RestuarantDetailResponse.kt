package com.lia.yello.linedineresturant.Model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class RestuarantDetailResponse: Serializable {

    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: RestDetail? = null


}


class RestDetail : Serializable {

    @SerializedName("res_name")
    var res_name: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("banner")
    var banner: String? = null


    @SerializedName("lat")
    var lat: Double? = null


    @SerializedName("lng")
    var lng: Double? = null


    @SerializedName("rating")
    var rating: String? = null

    @SerializedName("kids")
    var kids: String? = null

    @SerializedName("phone")
    var phone: String? = null

    @SerializedName("res_status")
    var res_status: String? = null
}