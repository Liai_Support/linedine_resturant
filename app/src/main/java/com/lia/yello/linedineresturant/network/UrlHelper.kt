package com.lia.yello.linedineresturant.network
import com.lia.yello.linedineresturant.R


object UrlHelper {
 // private const val BASE = "http://192.168.1.12/"
private const val BASE = "https://portal.lineandine.com"
 //private const val BASE = "http://staging.lineandine.com"
 private const val BASE_URL = BASE + "/line_dine_api/restaurant/"
 private const val BASE_URL2 = BASE + "/line_dine_api/customer_ver3/"
 private const val BASE_URL4 = BASE + "/customer_ver3/"
 private const val BASE_URL3 = BASE + "/restaurant_ver3/"

 const val LOGIN = BASE_URL3 + "waiterlogin"
 const val RESETPASSWORD = BASE_URL3 + "resetpassword"
 const val FORGETPASSWORD = BASE_URL3 + "forgotpassword"
 const val LISTTABLES = BASE_URL3 + "listtables"
 const val RESTDETAIL = BASE_URL3 + "resdetail"
 const val UPDATEMENU = BASE_URL3 + "updatemenu"
 const val UPDATEINVOICE = BASE_URL3 + "updateinvoice"
 const val UPDATETABLESTATUS = BASE_URL3+"updatetablestatus"
 const val UPDATEDEVICETOKEN =  BASE_URL3 + "updateusertoken"
 //const val MENUDETAILS =  BASE_URL + "menu"
 const val MENUDETAILS =  BASE_URL4 + "menu"
 const val ITEMDETAIL = BASE_URL3 +  "itemdet"
 const val GETHOME =  BASE_URL3 + "initialvalues"

 const val CANCELBOOKING =  BASE_URL3 + "cancelbooking"






/* const val GOOGLE_API_BASE_URL = "https://maps.googleapis.com/maps/api/"
 const val GOOGLE_API_DIRECTION_BASE_URL = GOOGLE_API_BASE_URL + "directions/json?"
 const val GOOGLE_API_AUTOCOMPLETE_BASE_URL = GOOGLE_API_BASE_URL + "place/autocomplete/json?"
 const val GOOGLE_API_PLACE_DETAILS_BASE_URL = GOOGLE_API_BASE_URL + "place/details/json?"
 const val GOOGLE_API_STATIC_MAP_BASE_URL = GOOGLE_API_BASE_URL + "staticmap?"
 const val GOOGLE_API_GEOCODE_BASE_URL = GOOGLE_API_BASE_URL + "geocode/json?"*/




}