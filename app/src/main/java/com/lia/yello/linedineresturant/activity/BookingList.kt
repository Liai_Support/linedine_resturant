package com.lia.yello.linedineresturant.activity

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedineresturant.Adapter.BookingListAdapter
import com.lia.yello.linedineresturant.R
import com.lia.yello.linedineresturant.Session.SharedHelper
import com.lia.yello.linedineresturant.Utils.DialogUtils
import com.lia.yello.linedineresturant.Utils.UiUtils
import com.lia.yello.linedineresturant.ViewModel.MapViewModel
import kotlinx.android.synthetic.main.activity_booking_list.*
import kotlinx.android.synthetic.main.activity_dash_board.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.header.*

class BookingList : AppCompatActivity() {
    var value:Int=0
    private var viewmodel: MapViewModel? = null
    var sharedHelper: SharedHelper? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_list)
        value = intent.extras!!.getInt("tableid")
        Log.d("tabless",""+value)
        viewmodel = ViewModelProvider(this).get(MapViewModel::class.java)
        sharedHelper = SharedHelper(this)
       // getvalue(sharedHelper!!.resid)
        head.setText(getString(R.string.bookinglist))



    }




}