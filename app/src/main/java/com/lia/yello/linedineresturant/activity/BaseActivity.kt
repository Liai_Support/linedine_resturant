package com.lia.yello.linedineresturant.activity

import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.os.LocaleList
import androidx.appcompat.app.AppCompatActivity
import com.lia.yello.linedineresturant.Session.SharedHelper
import java.util.*

abstract class BaseActivity: AppCompatActivity(){

    private var sharedHelper: SharedHelper? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedHelper = SharedHelper(this)
        setPhoneLanguage()
    }

    override fun attachBaseContext(newBase: Context) {
        sharedHelper = SharedHelper(newBase)
        val currentLanguage = sharedHelper!!.language.toLowerCase()
        val locale = Locale(currentLanguage)
        Locale.setDefault(locale)
        val configuration: Configuration = newBase.getResources().getConfiguration()
        configuration.setLocale(locale)
        super.attachBaseContext(newBase.createConfigurationContext(configuration))
    }

    override fun applyOverrideConfiguration(overrideConfiguration: Configuration?) {
        if (overrideConfiguration != null) {
            val uiMode: Int = overrideConfiguration.uiMode
            overrideConfiguration.setTo(baseContext.resources.configuration)
            overrideConfiguration.uiMode = uiMode
        }
        super.applyOverrideConfiguration(overrideConfiguration)
    }


    private fun setPhoneLanguage() {
        var sharedHelper = SharedHelper(this)
        val res = resources
        val conf = res.configuration
        val locale = Locale(sharedHelper.language.toLowerCase())
        Locale.setDefault(locale)
        conf.setLocale(locale)
        applicationContext.createConfigurationContext(conf)
        val dm = res.displayMetrics
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            conf.setLocales(LocaleList(locale))

        } else {
            conf.locale = locale

        }
        res.updateConfiguration(conf, dm)
    }
}