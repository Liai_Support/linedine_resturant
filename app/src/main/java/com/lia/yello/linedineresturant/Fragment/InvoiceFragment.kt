package com.lia.yello.linedineresturant.Fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lia.yello.linedineresturant.Adapter.BookingListAdapter
import com.lia.yello.linedineresturant.R
import com.lia.yello.linedineresturant.Session.SharedHelper
import com.lia.yello.linedineresturant.Utils.DialogUtils
import com.lia.yello.linedineresturant.Utils.UiUtils
import com.lia.yello.linedineresturant.ViewModel.MapViewModel
import com.lia.yello.linedineresturant.activity.DashBoardActivity
import com.lia.yello.linedineresturant.databinding.FragmentInvoiceBinding
import kotlinx.android.synthetic.main.activity_add_new_menu_list.*
import kotlinx.android.synthetic.main.activity_add_new_menu_list.root_subsubhome
import kotlinx.android.synthetic.main.activity_booking_list.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.fragment_invoice.*
import kotlinx.android.synthetic.main.fragment_success.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.header.language2
import kotlinx.android.synthetic.main.headerhome.*
import java.io.Serializable
import java.util.*


class InvoiceFragment : Fragment(R.layout.fragment_invoice) {
    private var viewmodel: MapViewModel? = null
    var sharedHelper: SharedHelper? = null
    var resid: Int = 0
    var doubleBackToExitPressedOnce: Boolean = false
    var binding: FragmentInvoiceBinding? = null
    var value: Int = 0
/*  lateinit var subtotall: TextView
    lateinit var vatt: TextView
    lateinit var totalamtt: TextView
    lateinit var prodcuttotal: TextView
    lateinit var bookingidd: TextView
    lateinit var tableidd: TextView
    lateinit var bookingamt: TextView
    lateinit var discountamt1: TextView
    lateinit var back: ImageView
    lateinit var invoiceimg: ImageView
    lateinit var finaltotalinvoice: TextView
    lateinit var discountlayout1: LinearLayout
    lateinit var linr_invoicept: LinearLayout*/
    var bookingid:Int=0
    var tableid:Int=0
    var status:String=""
    var paymenttype:String=""
    var tablename:String=""
    private var homeFragment: HomeFragment? = null
    private var successFragment: SuccessFragment? = null
    private var menuListFragment: MenuListFragment? = null
    private lateinit var fragmentTransaction: FragmentTransaction

   // lateinit var closetable:Button
    var subtotal: Double = 0.0
    var totalamt:Double=0.0
    var vat:Double=0.0
    var dicountmmt:Double=0.0
    var bookingamount:Double=0.0
    var resimage:String=""
    var paid :Boolean= false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DataBindingUtil.findBinding(view)
        sharedHelper = SharedHelper(requireContext())
        viewmodel = ViewModelProvider(this).get(MapViewModel::class.java)
       // val head = view.findViewById<TextView>(R.id.head)
      val refresh = view.findViewById<ImageView>(R.id.refresh)
      // refresh.visibility=View.VISIBLE
        getvalue(sharedHelper!!.resid)

       // head.text=getString(R.string.invoice)
        DialogUtils.dismissLoader()

/*        subtotall = view.findViewById<TextView>(R.id.nvoice_status_subtotal)
        vatt= view.findViewById<TextView>(R.id.nvoice_status_vat)
        prodcuttotal= view.findViewById<TextView>(R.id.prodcuttotal)
        totalamtt = view.findViewById<TextView>(R.id.invoice_status_amt)
        closetable = view.findViewById<Button>(R.id.closetable)
        tableidd = view.findViewById<Button>(R.id.invoice_status_date)
        bookingidd = view.findViewById<Button>(R.id.invoice_status_id)
        bookingamt = view.findViewById<TextView>(R.id.nvoice_status_bookingamt)
        discountlayout1 = view.findViewById<LinearLayout>(R.id.linr_invoice6)
        linr_invoicept = view.findViewById<LinearLayout>(R.id.linr_invoicept)
        discountamt1 = view.findViewById<TextView>(R.id.discountamt)
        back = view.findViewById<ImageView>(R.id.back)
        invoiceimg = view.findViewById<ImageView>(R.id.invoiceimg)
        finaltotalinvoice = view.findViewById<TextView>(R.id.finaltotalinvoice)*/
        value = arguments!!.getInt("tableid")
        bookingid=arguments!!.getInt("bookingid")
        resid=arguments!!.getInt("resid")
        subtotal=arguments!!.getDouble("subtotal")
        vat=arguments!!.getDouble("vat")
        totalamt=arguments!!.getDouble("total")
        bookingamount=arguments!!.getDouble("bookingamount")
        dicountmmt=arguments!!.getDouble("dicountmmt")
        tablename= arguments!!.getString("tablename")!!
        Log.d("Zxcvbnmaswedrftgyhujk",""+bookingamount)
        paymenttype=arguments!!.getString("paymenttype")!!
        resimage= arguments!!.getString("resimg")!!

        Log.d("hhhjjsjs",""+dicountmmt)
        if (sharedHelper!!.language.equals("ar")) {

            language11.text = "EN"
        }else{
            language11.text = "AR"

        }
        language11.setOnClickListener{
            if (sharedHelper!!.language.equals("ar")){
                sharedHelper?.language = "en"
                reloadApp()
            }else if (sharedHelper!!.language.equals("en")){
                sharedHelper?.language = "ar"
                reloadApp()
            }
        }
        if (dicountmmt!=0.0){
            Log.d("hhhjjsjs","fgtgfgfhg")

            linr_invoice6.visibility=View.VISIBLE
            linr_invoicept.visibility=View.VISIBLE
            discountamt.text=" - "+String.format(Locale("en", "US"),"%.2f",dicountmmt)+ " SAR"
            prodcuttotal.text=String.format(Locale("en", "US"),"%.2f",dicountmmt + subtotal)+ " SAR"

        }else{
            Log.d("vbvbv","bnbn")

            linr_invoice6.visibility=View.GONE
            linr_invoicept.visibility=View.GONE


        }



       // vatt.text = String.format("%.2f", vat) + " SAR"
        nvoice_status_vat.text= String.format(Locale("en", "US"),"%.2f",vat)+ " SAR"


        //totalamtt.text =  totalamt.toString()+" SAR"
     //   totalamtt.text = String.format("%.2f", totalamt) + " SAR"
        invoice_status_amt.text= String.format(Locale("en", "US"),"%.2f",totalamt)+ " SAR"
        finaltotalinvoice.text= String.format(Locale("en", "US"),"%.2f",totalamt)+ " SAR"
      //  subtotall.text = "$subtotal SAR"
       // subtotall.text =String.format("%.2f", subtotal) + " SAR"
        nvoice_status_subtotal.text=String.format(Locale("en", "US"),"%.2f",subtotal)+ " SAR"
       // bookingamt.text = sharedHelper!!.initialamount.toString()+" SAR"
            //  bookingamt.text = String.format("%.2f", sharedHelper!!.initialamount) + " SAR"
        nvoice_status_bookingamt.text="- "+String.format(Locale("en", "US"),"%.2f",sharedHelper!!.initialamount)+" SAR"

       /* totalamtt.text= totalamt.toString()
        subtotall.text= subtotal.toString()*/
        invoice_status_id.text= bookingid.toString()





        val back = view.findViewById<ImageView>(R.id.back)
        if (sharedHelper!!.language == "ar"){
            back.rotation= 180F
            nvoice_status_vat.gravity=Gravity.LEFT
            invoice_status_amt.gravity=Gravity.LEFT
            nvoice_status_subtotal.gravity=Gravity.LEFT
            nvoice_status_bookingamt.gravity=Gravity.LEFT
            discountamt.gravity=Gravity.LEFT
            prodcuttotal.gravity=Gravity.LEFT

        }
        back.setOnClickListener(View.OnClickListener {

          //  requireActivity().supportFragmentManager.popBackStack()
            val bundle = Bundle()
            bundle.putInt("bookingid",bookingid)
            bundle.putInt("tableid",value)
            bundle.putString("booked","true")
            bundle.putString("resimg",resimage)

            menuListFragment = MenuListFragment()
            menuListFragment!!.arguments = bundle
            fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
            fragmentTransaction.replace(R.id.container, menuListFragment!!)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()

        })


        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {


                val bundle = Bundle()
                bundle.putInt("bookingid",bookingid)
                bundle.putInt("tableid",value)
                bundle.putString("booked","true")
                bundle.putString("resimg",resimage)
                menuListFragment = MenuListFragment()
                menuListFragment!!.arguments = bundle
                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(R.anim.screen_in, R.anim.screen_out)
                fragmentTransaction.replace(R.id.container, menuListFragment!!)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()

            }
        })


        refresh.setOnClickListener(View.OnClickListener {
            getvalue(sharedHelper!!.resid)
          /*  if (paid==true){
               // getvalue(sharedHelper!!.resid)
                closetable.visibility=View.VISIBLE

            }*/
        })


        closetable.setOnClickListener{
            gettablestatus(bookingid,"closed",value,paymenttype)

        }


    }




    private fun getvalue(resid: Int) {
        DialogUtils.showLoader(requireContext())
        viewmodel?.gettabledetails(requireContext(), resid)
            ?.observe(requireActivity(),
                Observer {
                    DialogUtils.dismissLoader()

                    it?.let {
                        it.error?.let { error ->
                            if (error) {

                                it.message?.let { msg ->
                                    UiUtils.showSnack(root_invoice, msg)

                                }

                            } else {
                                it.data?.let { data ->
                                    for (i in 0 until data.size) {

                                        if (data[i].bookinglist!!.size != 0) {
                                            for (k in 0 until data[i].bookinglist!!.size) {
                                                if (data[i].bookinglist!![k].bookingid == bookingid) {
                                                    // want status for particular booking id


                                                    if (data[i].bookinglist!![k].paidstatus.equals("paid")){
                                                        closetable.visibility=View.VISIBLE
                                                        invoice_status_date.text= data[i].tablename

                                                        Log.d("azsxdcfvgbhnjmkl",""+data[i].tablename)

                                                        paid=true



                                                        if(data[i].bookinglist!![k].menulistdetails!!.size == 0){
                                                            for (a in 0 until data[i].bookinglist!![k].menulistdetails!!.size) {
                                                                subtotal += data[i].bookinglist!![k].menulistdetails!![a]!!.menucost!! * data[i].bookinglist!![k].menulistdetails!![a]!!.quantity!!

                                                            }

                                                            vat = (subtotal / 100.0f) * sharedHelper!!.vat
                                                            //  bookingamount =   String.format(Locale("en", "US"),"%.2f",data[i].bookinglist!![k].initial_amount).toDouble()
                                                            // totalamt = subtotal + vat - String.format(Locale("en", "US"),"%.2f",data[i].bookinglist!![k].initial_amount!!).toDouble()


                                                        }



                                                        if (data[i].bookinglist!![k].discount_name!!.isEmpty()){
                                                            linr_invoice6.visibility=View.GONE
                                                            linr_invoicept.visibility=View.GONE

                                                            invoice_status_amt.text =  String.format(Locale("en", "US"),"%.2f",totalamt)+" SAR"
                                                            finaltotalinvoice.text =  String.format(Locale("en", "US"),"%.2f",totalamt)+" SAR"
                                                            nvoice_status_subtotal.text=  String.format(Locale("en", "US"),"%.2f",subtotal)+" SAR"
                                                            nvoice_status_vat.text = String.format(Locale("en", "US"),"%.2f",vat)+" SAR"

                                                            if (sharedHelper!!.initialamount.equals("0.0")){
                                                                linr_invoice5.visibility=View.GONE
                                                            }else{
                                                                linr_invoice5.visibility=View.VISIBLE

                                                            }
                                                            nvoice_status_bookingamt.text="- "+String.format(Locale("en", "US"),"%.2f",sharedHelper!!.initialamount)+" SAR"

                                                        }else{

                                                            linr_invoice6.visibility=View.VISIBLE
                                                            linr_invoicept.visibility=View.VISIBLE
                                                            // val subtotalll =subtotal-data[i].bookinglist!![k].discount_amount!!.toDouble()

                                                            // Log.d("sdfghjk",""+subtotalll)
                                                            Log.d("vvvv",""+subtotal+data[i].bookinglist!![k].discount_amount!!.toDouble())

                                                            // subtotal=subtotalll

                                                            val subt=subtotal-data[i].bookinglist!![k].discount_amount!!.toDouble()
                                                            nvoice_status_subtotal.text=  String.format(Locale("en", "US"),"%.2f",subt)+" SAR"
                                                            prodcuttotal.text=  String.format(Locale("en", "US"),"%.2f",subtotal)+" SAR"
                                                            val vattt = (subt / 100.0f) * sharedHelper!!.vat
                                                            vat=vattt
                                                            nvoice_status_vat.text = String.format(Locale("en", "US"),"%.2f",vat)+" SAR"
                                                            dicountmmt=  data[i].bookinglist!![k].discount_amount!!.toDouble()


                                                            val total=subt+vat- bookingamount!!
                                                            totalamt=total

                                                            invoice_status_amt.text =  String.format(Locale("en", "US"),"%.2f",total)+" SAR"
                                                            finaltotalinvoice.text =  String.format(Locale("en", "US"),"%.2f",total)+" SAR"
                                                            nvoice_status_bookingamt.text="- "+String.format(Locale("en", "US"),"%.2f",sharedHelper!!.initialamount)+" SAR"

                                                            discountamt.text=" - "+String.format(Locale("en", "US"),"%.2f",dicountmmt)+ " SAR"
                                                            //      prodcuttotal.text=String.format(Locale("en", "US"),"%.2f",dicountmmt + subtotal)+ " SAR"

                                                        }
                                                       // gettablestatus(bookingid,"closed",value,paymenttype)



                                                   /*     */


                                                    }else{
                                                        Log.d("nbnbnbnbnbnb",""+data[i].tablename)
                                                        invoice_status_date.text= data[i].tablename

                                                        closetable.visibility=View.GONE
                                                        linr_invoice6.visibility=View.GONE
                                                        linr_invoicept.visibility=View.GONE

                                                    }

                                                }
                                            }


                                        }


                                    }
                                }
                            }

                        }
                    }
                })
    }

    private fun reloadApp() {



        val intent = Intent(requireContext(), DashBoardActivity::class.java)

        intent.putExtra("langfrom", "Invoicefrag")
        intent.putExtra("tableid", value)
        intent.putExtra("bookingid", bookingid)
        intent.putExtra("resid", resid)
        intent.putExtra("subtotal", subtotal)
        intent.putExtra("vat", vat)
        intent.putExtra("total", totalamt)
        intent.putExtra("bookingamount", bookingamount)
        intent.putExtra("dicountmmt", dicountmmt)
        intent.putExtra("tablename", tablename)
        intent.putExtra("paymenttype", paymenttype)
        intent.putExtra("resimg", resimage)
        startActivity(intent)



    }


    public fun gettablestatus(bookingid: Int,txt: String,tableid: Int,paymenttype:String ) {
        DialogUtils.showLoader(requireContext())
        viewmodel?.getupdatetablestatus(requireContext(), bookingid,txt,tableid)
            ?.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                DialogUtils.dismissLoader()

                it?.let {
                    it.error?.let { error ->
                        if (error) {
                            it.message?.let { msg ->

                                UiUtils.showSnack(root_invoice, getString(R.string.tableisclosedbyadmin))


                                homeFragment = HomeFragment()
                                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                                fragmentTransaction.setCustomAnimations(R.anim.screen_in,R.anim.screen_out)
                                fragmentTransaction.replace(R.id.container, homeFragment!!)
                                fragmentTransaction.addToBackStack(null)
                                fragmentTransaction.commit()
                            }
                        } else {
                            it.message?.let { msg ->
                                UiUtils.showSnack(root_invoice, msg)
                               /* val bundle = Bundle()
                                bundle.putInt("bookingid",bookingid)
                                bundle.putInt("tableid",value)
                                bundle.putString("paymenttype",paymenttype)*/
                                successFragment = SuccessFragment()
                               // successFragment!!.arguments = bundle
                                fragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
                                fragmentTransaction.setCustomAnimations(R.anim.screen_in,R.anim.screen_out)
                                fragmentTransaction.replace(R.id.container, successFragment!!)
                                fragmentTransaction.addToBackStack(null)
                                fragmentTransaction.commit()


                            }
                        }

                    }
                }
            })

    }


}