package com.lia.yello.linedineresturant.Model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class MenuDetailResponse: Serializable {


    @SerializedName("error")
    var error: Boolean? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: MenuData? = null

    }

    class MenuData : Serializable {


    @SerializedName("restaurantdet")
    var restdetail: RestDetails? = null

   @SerializedName("workingHours")
    var data2: ArrayList<WorkingHours>? = null

    @SerializedName("menuDetails")
    var menudetail: ArrayList<MenuDetailss>? = null


}
class RestDetails : Serializable {

    @SerializedName("res_name")
    var res_name: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("banner")
    var bannerimg: String? = null

    @SerializedName("rating")
    var rating: String? = null

    @SerializedName("lat")
    var lat: Double? = null

    @SerializedName("lng")
    var lng: Double? = null

    @SerializedName("kids")
    var kids: String? = null

    @SerializedName("phone")
    var phonee: String? = null

    @SerializedName("res_status")
    var res_status: String? = null

}



class WorkingHours : Serializable {

    @SerializedName("openingTime")
    var openingTime: String? = null

    @SerializedName("closingTime")
    var closingTime: String? = null

    @SerializedName("open24")
    var open24: String? = null

    @SerializedName("closed")
    var closed: String? = null


}
class MenuDetailss : Serializable {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("menu_id")
    var menu_id: Int? = null

    @SerializedName("res_id")
    var res_id: Int? = null

    @SerializedName("item")
    var item: String? = null


    @SerializedName("calories")
    var calories: String? = null


    @SerializedName("description")
    var description: String? = null


    @SerializedName("price")
    var price: Double? = null


    @SerializedName("images")
    var images: String? = null

    @SerializedName("favourite")
    var favourite: String? = null


    @SerializedName("rating")
    var menurating: Double? = null


}

