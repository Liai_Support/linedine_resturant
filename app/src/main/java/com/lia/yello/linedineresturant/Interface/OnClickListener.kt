package com.lia.yello.linedineresturant.Interface

interface OnClickListener {
    fun onClickItem(position: Int)
}
