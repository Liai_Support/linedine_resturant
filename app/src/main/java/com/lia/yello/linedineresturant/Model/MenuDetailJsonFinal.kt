package com.lia.yello.linedineresturant.Model

import java.io.Serializable

data class MenuDetailJsonFinal(
    val menuid: Int,
    val quantity: Int,
    val cost: Double
)